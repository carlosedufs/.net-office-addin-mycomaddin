﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string dllMyCom = "C:\\temp\\addin_kanteiro\\MyCOMAddin.dll";
            string dllMyCom = @"C:\Users\Carlos Eduardo\Documents\Visual Studio 2010\Projects\MyCOMAddin\MyCOMAddin\bin\Debug\MyCOMAddin.dll";

            if (!File.Exists(dllMyCom)) return;

            string subkey = "CLSID\\{D8D6AFD5-4814-4635-8005-0C3C2346C788}";
            if ((Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(subkey)) != null)
            {
                Microsoft.Win32.Registry.ClassesRoot.DeleteSubKeyTree(subkey);
            }
            subkey = "MyCOMAddin.Connect";
            if ((Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(subkey)) != null)
            {
                Microsoft.Win32.Registry.ClassesRoot.DeleteSubKeyTree(subkey);
            }
            subkey = "SOFTWARE\\Microsoft\\Office\\Excel\\Addins\\MyCOMAddin.Connect";
            if ((Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subkey)) != null)
            {
                Microsoft.Win32.Registry.LocalMachine.DeleteSubKeyTree(subkey);
            }
            subkey = "SOFTWARE\\Microsoft\\Office\\Outlook\\Addins\\MyCOMAddin.Connect";
            if ((Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subkey)) != null)
            {
                Microsoft.Win32.Registry.LocalMachine.DeleteSubKeyTree(subkey);
            }

            #region CLSID\\{D8D6AFD5-4814-4635-8005-0C3C2346C788}
            RegistryKey CLSID;
            RegistryKey PROGID;
            RegistryKey TAWKAY = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default);

            subkey = "CLSID\\{D8D6AFD5-4814-4635-8005-0C3C2346C788}";

        try1:
            if ((CLSID = TAWKAY.OpenSubKey(subkey, true)) == null)   // Get values from Registry
            {
                CLSID = TAWKAY.CreateSubKey(subkey, RegistryKeyPermissionCheck.ReadWriteSubTree);
                CLSID.SetValue("", "MyCOMAddin.Connect"); //default value

                CLSID.CreateSubKey("Implemented Categories", RegistryKeyPermissionCheck.ReadWriteSubTree);
                CLSID.CreateSubKey("Implemented Categories\\{62C8FE65-4EBB-45E7-B440-6E39B2CDBF29}", RegistryKeyPermissionCheck.ReadWriteSubTree);

                var k1 = CLSID.CreateSubKey("InprocServer32", RegistryKeyPermissionCheck.ReadWriteSubTree);
                k1.SetValue("", "mscoree.dll"); //default value
                k1.SetValue("Assembly", "MyCOMAddin, Version=1.0.4969.5401, Culture=neutral, PublicKeyToken=null");
                k1.SetValue("CodeBase", dllMyCom);
                k1.SetValue("RuntimeVersion", "v2.0.50727");
                k1.SetValue("Class", "MyCOMAddin.Connect");
                k1.SetValue("ThreadingModel", "Both");
                k1.Close();

                k1 = CLSID.CreateSubKey("InprocServer32\\1.0.4969.5401", RegistryKeyPermissionCheck.ReadWriteSubTree);
                k1.SetValue("Assembly", "MyCOMAddin, Version=1.0.4969.5401, Culture=neutral, PublicKeyToken=null");
                k1.SetValue("CodeBase", dllMyCom);
                k1.SetValue("Class", "MyCOMAddin.Connect");
                k1.SetValue("RuntimeVersion", "v2.0.50727");
                k1.Close();

                PROGID = CLSID.CreateSubKey("ProgId", RegistryKeyPermissionCheck.ReadWriteSubTree);
                PROGID.SetValue("", "MyCOMAddin.Connect"); //default value

                goto try1;
            }
            else
            {
                if (CLSID != null)
                    CLSID.Close();
                Console.WriteLine("Cannot open registry");
            }

            //TAWKAY.DeleteSubKeyTree(subkey);
            #endregion

            #region ClassesRoot\\MyCOMAddin.Connect
            RegistryKey myCOM = null;
            TAWKAY = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default);
            subkey = "MyCOMAddin.Connect";
        try2:
            if ((myCOM = TAWKAY.OpenSubKey(subkey, true)) == null)   // Get values from Registry
            {
                myCOM = TAWKAY.CreateSubKey(subkey, RegistryKeyPermissionCheck.ReadWriteSubTree);
                myCOM.SetValue("", "MyCOMAddin.Connect");

                myCOM = myCOM.CreateSubKey("CLSID", RegistryKeyPermissionCheck.ReadWriteSubTree);
                myCOM.SetValue("", "{D8D6AFD5-4814-4635-8005-0C3C2346C788}");

                goto try2;
            }
            else
            {
                if (myCOM != null)
                    myCOM.Close();
                Console.WriteLine("Cannot open registry");
            }

            //TAWKAY.DeleteSubKeyTree(subkey);
            #endregion


            #region \SOFTWARE\Microsoft\Office\Excel\Addins\MyCOMAddin.Connect
            TAWKAY = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            subkey = "SOFTWARE\\Microsoft\\Office\\Excel\\Addins\\MyCOMAddin.Connect";
        try26:
            if ((CLSID = TAWKAY.OpenSubKey(subkey, true)) == null)   // Get values from Registry
            {
                CLSID = TAWKAY.CreateSubKey(subkey, RegistryKeyPermissionCheck.ReadWriteSubTree);
                CLSID.SetValue("Description", "MyCOMAddin Connect"); //default value
                CLSID.SetValue("FriendlyName", "MyCOMAddin Connect"); //default value
                CLSID.SetValue("LoadBehavior", 3, RegistryValueKind.DWord); //default value

                goto try26;
            }
            else
            {
                if (CLSID != null)
                    CLSID.Close();
                Console.WriteLine("Cannot open registry");
            }

            //TAWKAY.DeleteSubKeyTree(subkey);
            #endregion

            #region \SOFTWARE\Microsoft\Office\Outlook\Addins\MyCOMAddin.Connect
            TAWKAY = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            subkey = "SOFTWARE\\Microsoft\\Office\\Outlook\\Addins\\MyCOMAddin.Connect";
        try27:
            if ((CLSID = TAWKAY.OpenSubKey(subkey, true)) == null)   // Get values from Registry
            {
                CLSID = TAWKAY.CreateSubKey(subkey, RegistryKeyPermissionCheck.ReadWriteSubTree);
                CLSID.SetValue("Description", "MyCOMAddin Connect"); //default value
                CLSID.SetValue("FriendlyName", "MyCOMAddin Connect"); //default value
                CLSID.SetValue("LoadBehavior", 3, RegistryValueKind.DWord); //default value

                goto try27;
            }
            else
            {
                if (CLSID != null)
                    CLSID.Close();
                Console.WriteLine("Cannot open registry");
            }

            //TAWKAY.DeleteSubKeyTree(subkey);
            #endregion

            var x = Console.Read();
        }
    }
}
