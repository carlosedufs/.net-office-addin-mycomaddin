﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ManagedMAPI;

namespace TestMAPI33Wrapper
{
    class Program
    {
        [STAThread()] //se nao tiver esse atributo nao funciona o MAPI33
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando...");

            Console.WriteLine("PropTags'PR_DISPLAY_NAME':" + (uint)PropTags.PR_DISPLAY_NAME);
            Console.WriteLine("PropTags'PR_ENTRYID':" + (uint)PropTags.PR_ENTRYID);            

            var lst = MAPI33Wrapper.MAPI33Samples.ListFolders();
            if (lst != null && lst.Count > 0)
            {
                lst.ToList().ForEach(
                    f =>
                    {
                        Console.WriteLine("EntryID diretorio: " + f.ToString());
                    });
            }
            Console.WriteLine("Fim!");
            Console.Read();
        }
    }
}
