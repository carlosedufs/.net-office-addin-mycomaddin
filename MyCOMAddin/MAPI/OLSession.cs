﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ManagedMAPI;

using Outlook = Microsoft.Office.Interop.Outlook;
using ManagedMAPI.Common.Interfaces;
using System.Runtime.InteropServices;

namespace MyCOMAddin.MAPI
{
    public sealed class SessionSingleton
    {
        #region Singleton Implementation
        static readonly SessionSingleton instance = new SessionSingleton();

        MAPISession session_;
        static SessionSingleton()
        {
        }

        SessionSingleton()
        {
            bool is64 = IntPtr.Size == 8;

            //Initialisation code goes here
            session_ = new MAPISession();
            session_.OpenMessageStore("");
        }

        public static SessionSingleton Instance
        {
            get { return instance; }
        }

        public MAPISession Session
        {
            get { return session_; }
        }

        public void Test33()
        {
            MAPI33Wrapper.MAPI33Samples.ListFolders();
        }

        public void GetMail(Outlook.MailItem item)
        {
            //var folder = item.Parent as Outlook.Folder;
            //var mapif = (IMAPIFolder)folder.MAPIOBJECT;
            var mapim = (IMAPIMessage)item.MAPIOBJECT;
            
            MAPIMessage msg = new MAPIMessage(null, null, mapim);
            var result = msg.GetProperties();

            //System.Windows.Forms.MessageBox.Show(result[1] + "\n" + result[2]);

            msg.Dispose();

            //Marshal.ReleaseComObject(mapif);
            Marshal.ReleaseComObject(mapim);
            //Marshal.ReleaseComObject(folder);
        }

        #endregion
    }
}
