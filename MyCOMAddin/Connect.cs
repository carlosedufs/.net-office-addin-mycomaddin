namespace MyCOMAddin
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Windows.Forms;
    using Microsoft.Office.Core;
    using MyCOMAddin.Properties;
    using Excel = Microsoft.Office.Interop.Excel;
    using Outlook = Microsoft.Office.Interop.Outlook;
    using ManagedMAPI;
    using System.Collections.Generic;
    using VSTOContrib.Outlook;

    #region Read me for Add-in installation and setup information.
    // When run, the Add-in wizard prepared the registry for the Add-in.
    // At a later time, if the Add-in becomes unavailable for reasons such as:
    //   1) You moved this project to a computer other than which is was originally created on.
    //   2) You chose 'Yes' when presented with a message asking if you wish to remove the Add-in.
    //   3) Registry corruption.
    // you will need to re-register the Add-in by building the MyCOMAddinSetup project, 
    // right click the project in the Solution Explorer, then choose install.
    #endregion

    /// <summary>
    ///   The object for implementing an Add-in.
    /// </summary>
    /// <seealso class='IDTExtensibility2' />
    [GuidAttribute("D8D6AFD5-4814-4635-8005-0C3C2346C788"), ProgId("MyCOMAddin.Connect")]
    public class Connect : Object, Extensibility.IDTExtensibility2, IRibbonExtensibility
    {
        private CommandBarButton MyButton;
        private CommandBars oCommandBars;
        private CommandBar oStandardBar;

        private bool repurposing = false;
        private static object omissing = System.Reflection.Missing.Value;

        private OutlookFolderMonitor<Outlook.MailItem> folderMonitorMails;
        private Outlook.MAPIFolder folderOut;

        /// <summary>
        ///		Implements the constructor for the Add-in object.
        ///		Place your initialization code within this method.
        /// </summary>
        public Connect()
        {
            InitiateSSLTrust();
        }

        /// <summary>
        ///      Implements the OnConnection method of the IDTExtensibility2 interface.
        ///      Receives notification that the Add-in is being loaded.
        /// </summary>
        /// <param term='application'>
        ///      Root object of the host application.
        /// </param>
        /// <param term='connectMode'>
        ///      Describes how the Add-in is being loaded.
        /// </param>
        /// <param term='addInInst'>
        ///      Object representing this Add-in.
        /// </param>
        /// <seealso class='IDTExtensibility2' />
        public void OnConnection(object application, Extensibility.ext_ConnectMode connectMode, object addInInst, ref System.Array custom)
        {
            applicationObject = application;
            addInInstance = addInInst;

            if (connectMode != Extensibility.ext_ConnectMode.ext_cm_Startup)
            {
                OnStartupComplete(ref custom);
            }
        }

        /// <summary>
        ///     Implements the OnDisconnection method of the IDTExtensibility2 interface.
        ///     Receives notification that the Add-in is being unloaded.
        /// </summary>
        /// <param term='disconnectMode'>
        ///      Describes how the Add-in is being unloaded.
        /// </param>
        /// <param term='custom'>
        ///      Array of parameters that are host application specific.
        /// </param>
        /// <seealso class='IDTExtensibility2' />
        public void OnDisconnection(Extensibility.ext_DisconnectMode disconnectMode, ref System.Array custom)
        {
            if (disconnectMode != Extensibility.ext_DisconnectMode.ext_dm_HostShutdown)
            {
                OnBeginShutdown(ref custom);
            }
            applicationObject = null;
        }

        /// <summary>
        ///      Implements the OnAddInsUpdate method of the IDTExtensibility2 interface.
        ///      Receives notification that the collection of Add-ins has changed.
        /// </summary>
        /// <param term='custom'>
        ///      Array of parameters that are host application specific.
        /// </param>
        /// <seealso class='IDTExtensibility2' />
        public void OnAddInsUpdate(ref System.Array custom)
        {
        }

        /// <summary>
        ///      Implements the OnStartupComplete method of the IDTExtensibility2 interface.
        ///      Receives notification that the host application has completed loading.
        /// </summary>
        /// <param term='custom'>
        ///      Array of parameters that are host application specific.
        /// </param>
        /// <seealso class='IDTExtensibility2' />
        public void OnStartupComplete(ref System.Array custom)
        {
            try
            {
                oCommandBars = (CommandBars)applicationObject.GetType().InvokeMember("CommandBars", BindingFlags.GetProperty, null, applicationObject, null);
            }
            catch (Exception)
            {
                // Outlook has the CommandBars collection on the Explorer object.
                object oActiveExplorer;
                oActiveExplorer = applicationObject.GetType().InvokeMember("ActiveExplorer", BindingFlags.GetProperty, null, applicationObject, null);
                oCommandBars = (CommandBars)oActiveExplorer.GetType().InvokeMember("CommandBars", BindingFlags.GetProperty, null, oActiveExplorer, null);
            }

            #region Cria uma tab e um botao customizado
            if (1 != 1)
            {
                // Set up a custom button on the "Standard" commandbar.
                try
                {
                    oStandardBar = oCommandBars["Standard"];
                }
                catch (Exception)
                {
                    // Access names its main toolbar Database.
                    oStandardBar = oCommandBars["Database"];
                }

                // In case the button was not deleted, use the exiting one.
                try
                {
                    MyButton = (CommandBarButton)oStandardBar.Controls["My Custom Button"];
                }
                catch (Exception)
                {
                    object omissing = System.Reflection.Missing.Value;
                    MyButton = (CommandBarButton)oStandardBar.Controls.Add(1, omissing, omissing, omissing, omissing);
                    MyButton.Caption = "My Custom Button";
                    MyButton.Style = MsoButtonStyle.msoButtonCaption;
                }

                // The following items are optional, but recommended. 
                //The Tag property lets you quickly find the control 
                //and helps MSO keep track of it when more than
                //one application window is visible. The property is required
                //by some Office applications and should be provided.
                MyButton.Tag = "My Custom Button";

                // The OnAction property is optional but recommended. 
                //It should be set to the ProgID of the add-in, so that if
                //the add-in is not loaded when a user presses the button,
                //MSO loads the add-in automatically and then raises
                //the Click event for the add-in to handle. 
                MyButton.OnAction = "!<MyCOMAddin.Connect>";

                MyButton.Visible = true;
                MyButton.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.MyButton_Click);
            }
            #endregion

            object oName = applicationObject.GetType().InvokeMember("Name", BindingFlags.GetProperty, null, applicationObject, null);

            // Display a simple message to show which application you started in.
            //System.Windows.Forms.MessageBox.Show("This Addin is loaded by " + oName.ToString(), "MyCOMAddin");

            if (this.ExcelApp != null)
            {
                //Azedou
            }

            if (this.OutlookApp != null)
            {
                /*
                folderOut = this.OutlookApp.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderOutbox) as Outlook.MAPIFolder;

                Outlook.Explorer explorer = this.OutlookApp.ActiveExplorer() as Outlook.Explorer;
                try
                {
                    // Explorer Window 
                    if (explorer != null)
                    {
                        //explorer = context as Outlook.Explorer;
                        var name = explorer.CurrentFolder.Name;
                        folderMonitorMails = new OutlookFolderMonitor<Outlook.MailItem>(
                            this.OutlookApp.Session, explorer.CurrentFolder);
                        folderMonitorMails.ItemModified += new EventHandler<EventArgs<Outlook.MailItem>>(folderMonitorMails_ItemModified);
                    }
                }
                finally
                {
                    if (explorer != null) Marshal.ReleaseComObject(explorer);
                    //if (context != null) Marshal.ReleaseComObject(context);
                }
                */
            }
        }

        void folderMonitorMails_ItemModified(object sender, EventArgs<Outlook.MailItem> e)
        {
            var explorer = this.OutlookApp.ActiveExplorer() as Outlook.Explorer;

            if (explorer != null)
            {
                //explorer.CurrentFolder = this.folderOut;
                //if ( sender is Outlook.MAPIFolder )
                //    explorer.CurrentFolder = (Outlook.MAPIFolder)sender;

                if (explorer.IsItemSelectableInView(e.Value)) // ensure item is in current view
                {
                    explorer.ClearSelection();
                    explorer.AddToSelection(e.Value); // change explorer selection
                }
                else
                {
                    // TODO: change current view so that item is selectable
                }

                Marshal.ReleaseComObject(explorer);
            }
        }

        /// <summary>
        ///      Implements the OnBeginShutdown method of the IDTExtensibility2 interface.
        ///      Receives notification that the host application is being unloaded.
        /// </summary>
        /// <param term='custom'>
        ///      Array of parameters that are host application specific.
        /// </param>
        /// <seealso class='IDTExtensibility2' />
        public void OnBeginShutdown(ref System.Array custom)
        {
            if (localConfig != null)
            {
                //localConfig.User = "";
                //localConfig.Password = "";
                localConfig.UrlWCFService = Connect.uriRemoteQuery.ToString();
                localConfig.Save();
            }

            //System.Windows.Forms.MessageBox.Show("MyCOMAddin Add-in is unloading.");
            if (MyButton != null)
            {
                MyButton.Delete(omissing);
                MyButton = null;
            }

            if (oStandardBar != null)
            {
                Marshal.ReleaseComObject(oStandardBar);
                oStandardBar = null;
            }

            if (oCommandBars != null)
            {
                Marshal.ReleaseComObject(oCommandBars);
                oCommandBars = null;
            }

            if (this.ExcelApp != null)
            {
                Marshal.ReleaseComObject(this.ExcelApp);
            }

            if (this.OutlookApp != null)
            {
                if (folderMonitorMails != null)
                    folderMonitorMails.Dispose();
                folderMonitorMails = null;
            }
        }

        #region Ribbon

        #region Eventos do customID
        public void Ribbon_Load(IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        public Bitmap Ribbon_LoadImage(string imageName)
        {
            //Assembly assembly = Assembly.GetExecutingAssembly();
            //Stream stream = assembly.GetManifestResourceStream("RibbonDemo." + imageName);
            //return new Bitmap(stream);
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                var fileNoExt = Path.GetFileNameWithoutExtension(resourceNames[i]);
                if (fileNoExt.EndsWith(imageName, StringComparison.OrdinalIgnoreCase))
                {
                    Stream stream = asm.GetManifestResourceStream(resourceNames[i]);
                    if (stream != null)
                    {
                        return new Bitmap(stream);
                    }
                }
            }
            return null;
        }

        public string GetCustomUI(string RibbonID)
        {
            //System.Windows.Forms.MessageBox.Show("MyCOMAddin Add-in is coxa.");
            //return Ribbon1.GetResourceText();
            return Resources.MenuWCF;
        }

        #endregion

        #region Eventos menus do ribbon
        private void MyButton_Click(CommandBarButton cmdBarbutton, ref bool cancel)
        {
            try
            {
                System.Windows.Forms.MessageBox.Show("MyButton was Clicked", "MyCOMAddin");
                /*
                                if (this.ExcelApp != null)
                                {
                                    Excel.Worksheet sheet = this.ExcelApp.Application.ActiveSheet as Excel.Worksheet;

                                    string strRange = "A1";
                                    string andRange = "D5";

                                    System.Array arr = Array.CreateInstance(typeof(object), 5, 4);
                                    for (int i = 0; i < 4; i++)
                                    {
                                        for (int j = 0; j < 5; j++)
                                        {
                                            try
                                            {
                                                arr.SetValue(((10 * i) + (j+1*2)), i, j);
                                            }
                                            catch { }
                                        }

                                    }
                                    sheet.get_Range(strRange, andRange).Value2 = arr;

                                    var query = @"SELECT CustomerID, CompanyName, ContactName, ContactTitle, Address, 
                                City, Region, PostalCode, Country, Phone, Fax FROM Customers";

                                    WebClient client = new WebClient();
                                    byte[] data = client.DownloadData("http://localhost:8080/Service1.svc/GetDataByQuery/" + query); // Argument is XX
                                    using (Stream stream = new MemoryStream(data))
                                    {
                                        DataContractJsonSerializer obj = new DataContractJsonSerializer(typeof(DataExtractorTable));
                                        DataExtractorTable result = obj.ReadObject(stream) as DataExtractorTable;

                                        if (result != null && result.Rows.Length > 0)
                                        {
                                            //Preenche sheet
                                            for (int i = 0; i < result.Columns.Length; i++)
                                            {
                                                sheet.Cells[1, 1 + i] = result.Columns[i].Name;
                                            }
                                        }
                                    }
                                }
                 */
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Erro: " + ex.Message, "MyCOMAddin");
            }
        }

        public void OnDownloadData(IRibbonControl control)
        {
            //show form
            try
            {
                if (Connect.tokenRemoteQuery == Guid.Empty)
                {
                    if (!string.IsNullOrEmpty(localConfig.UrlWCFService))
                        Connect.uriRemoteQuery = new Uri(localConfig.UrlWCFService);

                    frmLogin uiLogin = new frmLogin(Connect.uriRemoteQuery.ToString());
                    var result = uiLogin.ShowDialog();
                    if (result != DialogResult.OK)
                    {
                        return;
                    }

                    Connect.uriRemoteQuery = new Uri(uiLogin.urlWCFService);
                }

                // ShowData is the form in which your data will be shown
                //UIRemoteQueryForm ui = new UIRemoteQueryForm(this.ExcelApp, Connect.uriRemoteQuery);
                //ui.Show();
                //ui.BringToFront();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Erro: " + ex.Message, "MyCOMAddin");
            }
        }

        public string GetPressed(IRibbonControl control)
        {
            return "Your description here.";
        }

        public void OnAction(IRibbonControl control)
        {
            if (this.ExcelApp == null) return;

            switch (control.Id)
            {
                case "button1": MessageBox.Show("You clicked the Happy Face button."); break;
                case "button2": this.ExcelApp.Range["A1"].Value = "You selected the Insert Text button."; break;
                case "button3": this.ExcelApp.Range["A1"].Value = "You selected the Insert More Text button."; break;
                default: MessageBox.Show("There was a problem."); break;
            }
        }

        public string GetContent(IRibbonControl control)
        {
            StringBuilder MyStringBuilder = new StringBuilder(@"<menu xmlns=""http://schemas.microsoft.com/office/2009/07/customui"" >");
            MyStringBuilder.Append(@"<button id=""button2"" label=""Insert Text"" onAction=""OnAction"" imageMso=""SignatureLineInsert"" />");
            MyStringBuilder.Append(@"<menuSeparator id=""menusep1"" getTitle=""GetTitle"" />");
            MyStringBuilder.Append(@"<button id=""button3"" label=""Insert More Text"" onAction=""OnAction"" imageMso=""FileDocumentInspect"" />");
            MyStringBuilder.Append(@"</menu>");
            return MyStringBuilder.ToString();
        }

        public string GetTitle(IRibbonControl control)
        {
            return "menu separator title";
        }

        public void OnAbout(IRibbonControl control)
        {
            try
            {
                AboutBox1 ui = new AboutBox1();
                ui.ShowDialog();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Erro: " + ex.Message, "MyCOMAddin");
            }
        }

        public void OpenNavPane(IRibbonControl control)
        {
            try
            {
                //Program2Chart.GenerateChart2(this.ExcelApp, true);

                switch (control.Id)
                {
                    case "MyImportExcel":
                        var o = this.ExcelApp.GetSaveAsFilename("Test", "Excel files (*.xl*),*.xl*", 1, "Custom Dialog Title");
                        if (o.GetType().IsPrimitive)
                        {
                            if (o is Boolean)
                            {

                            }
                            else if (o is string)
                            {

                            }
                        }
                        else
                            Marshal.ReleaseComObject(o);
                        break;
                    case "MyImportAccess":
                        var o2 = this.ExcelApp.GetOpenFilename("Excel Files (*.xls), *.xls", omissing, "Please select a file", omissing, omissing);
                        Marshal.ReleaseComObject(o2);
                        break;
                    case "MyImportOutlook":
                        //DoCmd.RunCommand acCmdImportAttachOutlook;
                        //RunCommandBar("FileSendAsAttachment");

                        if (this.OutlookApp != null)
                        {
                            MessageBox.Show("Est� no outlook");
                            var defFolders = MAPI.SessionSingleton.Instance.Session.CurrentStore.Folders.GetDefaultFoldersByType();

                            var folders = MAPI.SessionSingleton.Instance.Session
                                .CurrentStore.Folders.GetFolders();

                            if (folders != null && folders.Count > 0)
                            {
                                StringBuilder sb = new StringBuilder();

                                foreach (var folder in folders)
                                {
                                    sb.AppendLine("Folder: " + folder.ToString());
                                }

                                MessageBox.Show(
                                    string.Format("Caixa de email {0}:\r\n" + sb,
                                         MAPI.SessionSingleton.Instance.Session.CurrentStore.Name));
                            }

                            //Api MAPI33
                            if (IntPtr.Size == 4)
                            { //32bits
                                MAPI.SessionSingleton.Instance.Test33();
                            }
                        }

                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ContextMenuAction(IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "MyImportOutlook":
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void mySave(IRibbonControl control, bool cancelDefault)
        {
            if (repurposing)
            {
                MessageBox.Show("The Save button has been temporarily repurposed.");
                cancelDefault = false;
            }
            else
            {
                cancelDefault = false;
            }
        }

        public void changeRepurpose(IRibbonControl control, bool pressed)
        {
            repurposing = pressed;
            this.ribbon.Invalidate();
        }

        //================================================================
        //Lista de imagens
        //================================================================
        //FileSendAsAttachment
        //PivotTableChangeDataSource
        //DatabaseSqlServer
        //ImportXmlFile
        //DatabaseLinedTableManager
        //AttachItem
        //================================================================

        #endregion

        private void RunMacro(params object[] oRunArgs)
        {
            try
            {
                this.ExcelApp.GetType().InvokeMember("Run",
                    System.Reflection.BindingFlags.Default |
                    System.Reflection.BindingFlags.InvokeMethod,
                    null, this.ExcelApp, oRunArgs);
            }
            catch (Exception ex)
            {

            }
        }

        private void RunCommandBar(params string[] commands)
        {
            if (oCommandBars == null) return;

            //ex:
            //oCommandBars.ExecuteMso("FileSave");
            foreach (var cmd in commands)
            {
                oCommandBars.ExecuteMso(cmd);
            }
        }
        #endregion

        #region Context menu

        public string GetMyContent(IRibbonControl control)
        {
            var xmlString = @"<menu xmlns=""" +
      @"http://schemas.microsoft.com/office/2009/07/customui"">"
+ @"<button id=""btn1"" imageMso=""HappyFace"" " +
   @" label=""Click Me"" onAction=""btnAction"" />"
+ @"<menu id=""mnu"" label=""My Dynamic Menu"" > " +
   @"<button id=""btn2"" imageMso=""RecurrenceEdit"" /> " +
   @"<button id=""btn3"" imageMso=""CreateReportFromWizard"" /> " +
   @"</menu>" + "</menu>";
            return xmlString;
        }

        public void OnExplorerMailContextMenu(IRibbonControl control)
        {
            //show form
            try
            {
                var mailItem = GetMailItem(control);
                var explorer = this.OutlookApp.ActiveWindow() as Outlook.Explorer;
                if (explorer != null)
                {
                    int qq = 0;

                    explorer.ClearSelection();
                    GC.Collect();

                    var ei = mailItem.EntryID;
                vai_q_vai:
                    //==============================================================================
                    var mapiObject = mailItem.MAPIOBJECT;
                    string body = String.Empty;
                    IntPtr unk = IntPtr.Zero;
                    IntPtr unkObj = IntPtr.Zero;
                    bool bodyModified = false;
                    try
                    {
                        unkObj = Marshal.GetIUnknownForObject(mapiObject);
                        Guid iMapiProp = new Guid("00020303-0000-0000-C000-000000000046");
                        Marshal.QueryInterface(unkObj, ref iMapiProp, out unk);
                        if (unk != IntPtr.Zero)
                        {
                            IntPtr pPropValue = IntPtr.Zero;
                            try
                            {
                                var hr = ManagedMAPI.MAPINative.HrGetOneProp(unk, (uint)ManagedMAPI.ADXMapiTags.PR_BODY, out pPropValue);
                                ManagedMAPI.SPropValue propValue = (ManagedMAPI.SPropValue)Marshal.PtrToStructure(pPropValue, typeof(ManagedMAPI.SPropValue));
                                body = Marshal.PtrToStringAnsi(propValue.Value.lpszA);
                            }
                            catch (Exception ex)
                            {
                            }
                            if (pPropValue != IntPtr.Zero)
                                ManagedMAPI.MAPINative.MAPIFreeBuffer(pPropValue);
                            if (body == String.Empty)
                            {
                                pPropValue = IntPtr.Zero;
                                try
                                {
                                    var hr = ManagedMAPI.MAPINative.HrGetOneProp(unk, (uint)ManagedMAPI.ADXMapiTags.PR_BODY_HTML, out pPropValue);
                                    var propValue = (ManagedMAPI.SPropValue)Marshal.PtrToStructure(pPropValue, typeof(ManagedMAPI.SPropValue));
                                    body = Marshal.PtrToStringAnsi(propValue.Value.lpszA);
                                }
                                catch (Exception ex)
                                {
                                }
                                if (pPropValue != IntPtr.Zero)
                                    ManagedMAPI.MAPINative.MAPIFreeBuffer(pPropValue);
                            }
                            if (body == String.Empty)
                            {
                                pPropValue = IntPtr.Zero;
                                try
                                {
                                    var hr = ManagedMAPI.MAPINative.HrGetOneProp(unk, (uint)ManagedMAPI.ADXMapiTags.PR_HTML, out pPropValue);
                                    var propValue = (ManagedMAPI.SPropValue)Marshal.PtrToStructure(pPropValue, typeof(ManagedMAPI.SPropValue));
                                    body = Marshal.PtrToStringAnsi(propValue.Value.lpszA);
                                }
                                catch (Exception ex)
                                {
                                }
                                if (pPropValue != IntPtr.Zero)
                                    ManagedMAPI.MAPINative.MAPIFreeBuffer(pPropValue);
                            }

                            try
                            {
                                if (body == String.Empty)
                                {
                                    mailItem.Body = "usando Body do Interop, mudado em: " + DateTime.Now.ToString();
                                    bodyModified = true;
                                }
                                else
                                {

                                    var newbody = "Via MAPI, mudado em: " + DateTime.Now.ToString();
                                    using (MAPIMessage msg = new MAPIMessage((IMAPIProp)mapiObject))
                                    {
                                        msg.WriteNewBody(newbody);
                                    }

                                    #region test
                                    /*
                                    try
                                    {
                                        // Alloc memory for the text and create a pointer to it
                                        IntPtr ptrToValue = Marshal.StringToHGlobalAnsi(newbody);

                                        // Create our structure with data
                                        var propValue = new SPropValue();
                                        // Wich property should be set
                                        propValue.ulPropTag = (uint)ManagedMAPI.ADXMapiTags.PR_BODY;
                                        propValue.dwAlignPad = 0;
                                        propValue.Value.lpszA = ptrToValue;

                                        var ptrPropValue = Marshal.AllocHGlobal(Marshal.SizeOf(propValue));
                                        Marshal.StructureToPtr(propValue, ptrPropValue, false);

                                        // try to set the Property ( Property Tags can be found with Outlook Spy from Dmitry Streblechenko )
                                        var hr = MAPINative.HrSetOneProp(unk, ptrPropValue);
                                        if (hr != HRESULT.S_OK)
                                        {

                                        }

                                        Marshal.FreeHGlobal(ptrPropValue);
                                        Marshal.FreeHGlobal(ptrToValue);
                                    }
                                    finally
                                    {
                                    }
                                    */
                                    #endregion
                                }
                            }
                            catch (Exception ex)
                            {
                            }

                            if (pPropValue != IntPtr.Zero)
                                ManagedMAPI.MAPINative.MAPIFreeBuffer(pPropValue);
                        }
                    }
                    finally
                    {
                        Marshal.Release(unkObj);
                        if (unk != IntPtr.Zero) Marshal.Release(unk);
                    }
                    //==============================================================================

                    var x1 = Marshal.ReleaseComObject(mapiObject);

                    qq++;

                    if (qq <= 1)
                        //MAPI.SessionSingleton.Instance.GetMail(mailItem);
                        goto vai_q_vai;

                    //var old = mailItem.Subject;                    
                    //mailItem.Subject = ""+ old;
                    //var dtsent = mailItem.SentOn;
                    //if ( !mailItem.Saved )
                    //    mailItem.Save();
                    //mailItem.Subject = "Alterado em " + DateTime.Now.ToString();
                    mailItem.Close(Outlook.OlInspectorClose.olDiscard);

                    //ShowFolderInfo();

                    var x2 = Marshal.ReleaseComObject(mailItem);

                    //var mo = mailItem.MAPIOBJECT;

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    mailItem = null;

                    //explorer.RemoveFromSelection(mailItem);
                    //Application.DoEvents();

                    //this.OutlookApp.Session.RefreshRemoteHeaders(); 

                    //Outlook.Selection selectedItems = explorer.Selection;
                    //if (selectedItems.Count != 0) return;

                    //System.Threading.Thread.Sleep(1000);

                    //var panes = explorer.Panes;

                    //for (int i = 1; i <= panes.Count; i++)
                    //{
                    //    var pnl = panes[i];

                    //    GetTypeForComObject(pnl).ForEach(
                    //        (pt) =>
                    //        {
                    //            System.Diagnostics.Debug.WriteLine("Type found: " + pt.FullName);

                    //            foreach (var m in pt.GetMembers(BindingFlags.InvokeMethod | BindingFlags.NonPublic | 
                    //                BindingFlags.Public | BindingFlags.GetField | BindingFlags.Instance))
                    //            {
                    //                String name = m.Name.ToString();
                    //                System.Diagnostics.Debug.WriteLine("Member: " + name + ", type: " + m.MemberType.ToString());
                    //            }
                    //        });

                    //    if (pnl is Outlook._OutlookBarPane)
                    //    {
                    //        var pnlBarPane = pnl as Outlook._OutlookBarPane;
                    //        if (pnlBarPane.Contents.Groups.Count > 0)
                    //        {
                    //            System.Diagnostics.Debug.WriteLine("Groups======================");

                    //            for (int ii=1; ii<= pnlBarPane.Contents.Groups.Count ; ii++ ) {

                    //                System.Diagnostics.Debug.WriteLine("Group: " + pnlBarPane.Contents.Groups[ii].Name);

                    //                if ( pnlBarPane.Contents.Groups[ii].Name == "Links" ) {
                    //                    //objSCuts.Add("C:\RemoteLinks\mybacklogserver.lnk", "Backlog")
                    //                }
                    //            }                                
                    //        }
                    //    }

                    //foreach (var m in obj.GetMethods(BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetField | BindingFlags.Instance))
                    //{
                    //    String name = m.Name.ToString();
                    //    System.Diagnostics.Debug.WriteLine("Member: " + name + ", type: " + m.MemberType.ToString());

                    //    //if (m.MemberType == MemberTypes.Method)
                    //    //{
                    //    //    String name = m.Name.ToString();
                    //    //    System.Diagnostics.Debug.WriteLine("Member: " + name);
                    //    //}
                    //}

                    //foreach (var m in obj.GetFields(BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetField | BindingFlags.Instance))
                    //{
                    //    String name = m.Name.ToString();
                    //    System.Diagnostics.Debug.WriteLine("Fields: " + name + ", type: " + m.MemberType.ToString());

                    //    //if (m.MemberType == MemberTypes.Method)
                    //    //{
                    //    //    String name = m.Name.ToString();
                    //    //    System.Diagnostics.Debug.WriteLine("Member: " + name);
                    //    //}
                    //}

                    //foreach (var m in obj.GetProperties(BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetField | BindingFlags.Instance))
                    //{
                    //    String name = m.Name.ToString();
                    //    System.Diagnostics.Debug.WriteLine("Fields: " + name + ", type: " + m.MemberType.ToString());

                    //    //if (m.MemberType == MemberTypes.Method)
                    //    //{
                    //    //    String name = m.Name.ToString();
                    //    //    System.Diagnostics.Debug.WriteLine("Member: " + name);
                    //    //}
                    //}

                    //    Marshal.ReleaseComObject(pnl);
                    //}

                    //Marshal.ReleaseComObject(panes);

                    // return;

                    //if (explorer.IsPaneVisible(Outlook.OlPane.olPreview))
                    //    explorer.ShowPane(Outlook.OlPane.olPreview, false);

                    Outlook.NameSpace ns = this.OutlookApp.Session;
                    object item = ns.GetItemFromID(ei, Type.Missing); // retrieve item

                    if (explorer.IsItemSelectableInView(item)) // ensure item is in current view
                        explorer.AddToSelection(item); // change explorer selection
                    else
                    {
                        // TODO: change current view so that item is selectable
                    }

                    //Outlook.View view = explorer.CurrentView as Outlook.View;
                    //MessageBox.Show("The Current View is: " + view.Name);

                    //Marshal.ReleaseComObject(view);
                    Marshal.ReleaseComObject(item);
                    Marshal.ReleaseComObject(ns);
                    //Marshal.ReleaseComObject(explorer); 
                    ns = null;
                }

                //if (!explorer.IsPaneVisible(Outlook.OlPane.olPreview))
                //    explorer.ShowPane(Outlook.OlPane.olPreview, true);

                Marshal.ReleaseComObject(explorer);
                explorer = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
            }
        }

        #endregion

        #region Testes

        public static List<Type> GetTypeForComObject(object comObject)
        {
            List<Type> ltypes = new List<Type>();

            var iunkwn = Marshal.GetIUnknownForObject(comObject);

            try
            {
                Assembly outlookAssembly = Assembly.GetAssembly(typeof(Outlook.ApplicationClass));
                Type[] outlookTypes = outlookAssembly.GetTypes();

                foreach (Type currType in outlookTypes)
                {
                    Guid iid = currType.GUID;
                    if (!currType.IsInterface || iid == Guid.Empty)
                    {
                        continue;
                    }

                    var ipointer = IntPtr.Zero;
                    Marshal.QueryInterface(iunkwn, ref iid, out ipointer);

                    if (ipointer != IntPtr.Zero)
                    {
                        ltypes.Add(currType);
                    }
                }
            }
            finally
            {
                Marshal.Release(iunkwn);
            }

            return ltypes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var newEntryID = "";
            Outlook._Application OutlookApp = this.OutlookApp;
            Outlook._Explorer explorer = OutlookApp.ActiveExplorer();
            explorer.GetType().InvokeMember("ClearSelection", System.Reflection.BindingFlags.InvokeMethod, null, explorer, null);
            Outlook.NameSpace ns = OutlookApp.Session;
            object item1 = ns.GetItemFromID(newEntryID, Type.Missing);
            try
            {
                explorer.GetType().InvokeMember("AddToSelection", System.Reflection.BindingFlags.InvokeMethod, null, explorer, new object[] { item1 });
                Marshal.ReleaseComObject(item1);
                Marshal.ReleaseComObject(ns);
                Marshal.ReleaseComObject(explorer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvData_SelectionChanged(object sender, EventArgs e)
        {
            //get the current selected mail EntryID 
            string selectedID = ""; // dgvData.CurrentRow.Cells[6].Value.ToString();

            Outlook.NameSpace ns = this.OutlookApp.Session;
            Outlook.MailItem item = (Outlook.MailItem)ns.GetItemFromID(selectedID, Type.Missing);
            Outlook.MailItem newItem = (Outlook.MailItem)item.Copy();
            string newEntryID = newItem.EntryID;

            Outlook.MailItem newItem2 = (Outlook.MailItem)newItem.Move(ns.Folders[1].Folders["Searching..."]);

            newEntryID = newItem2.EntryID;
            Marshal.ReleaseComObject(newItem);
            Marshal.ReleaseComObject(newItem2);
            //Application.DoEvents(); 
            //button1.PerformClick();
        }

        private Outlook._MailItem GetMailItem(IRibbonControl e)
        {
            var active = this.OutlookApp.ActiveWindow();
            var context = this.OutlookApp.ActiveExplorer();
            Outlook.Inspector inspector = null;
            Outlook.Explorer explorer = null;
            Outlook.Selection selectedItems = null;
            Outlook._MailItem item = null;
            try
            {
                // Inspector Window
                if (context is Outlook.Inspector)
                {
                    inspector = context as Outlook.Inspector;
                    if (inspector == null) return null;

                    //Se vc fizer inspector.CurrentItem as Outlook._MailItem vc esta criando uma instancia do COM
                    // entao, colocarmos em um objeto depois perguntamos se eh do tipo
                    var mail = inspector.CurrentItem;

                    if (mail is Outlook._MailItem)
                        item = mail as Outlook._MailItem;
                }
                // Explorer Window 
                else if (context is Outlook.Explorer)
                {
                    explorer = context as Outlook.Explorer;
                    if (explorer == null) return null;
                    selectedItems = explorer.Selection;

                    if (selectedItems.Count != 1) return null;

                    //Se vc fizer selectedItems[1] as Outlook._MailItem vc esta criando uma instancia do COM
                    // entao, colocarmos em um objeto depois perguntamos se eh do tipo

                    var mail = selectedItems[1];
                    if (mail is Outlook._MailItem)
                        item = mail as Outlook._MailItem;
                }

                ///Para monitorar eventos de alteracao de um item
                /*
                if (item is Outlook.ItemEvents_10_Event)
                {
                    ((Outlook.ItemEvents_10_Event)item).Reply += new Outlook.ItemEvents_10_ReplyEventHandler(Connect_Reply);
                    ((Outlook.ItemEvents_10_Event)item).PropertyChange += new Outlook.ItemEvents_10_PropertyChangeEventHandler(Connect_PropertyChange);
                    ((Outlook.ItemEvents_10_Event)item).AfterWrite += new Outlook.ItemEvents_10_AfterWriteEventHandler(Connect_AfterWrite);
                    ((Outlook.ItemEvents_10_Event)item).Write += new Outlook.ItemEvents_10_WriteEventHandler(Connect_Write);
                }
                */
                return item;
            }
            finally
            {
                if (inspector != null) Marshal.ReleaseComObject(inspector);
                if (selectedItems != null) Marshal.ReleaseComObject(selectedItems);
                if (explorer != null) Marshal.ReleaseComObject(explorer);
                if (active != null) Marshal.ReleaseComObject(active);
                if (context != null) Marshal.ReleaseComObject(context);
                //if (item != null) Marshal.ReleaseComObject(item);
            }

            return null;
        }

        void Connect_Write(ref bool Cancel)
        {

        }

        void Connect_AfterWrite()
        {

        }

        void Connect_PropertyChange(string Name)
        {
            if (Name == "Body")
            {
            }
        }

        void Connect_Reply(object Response, ref bool Cancel)
        {

        }

        private void ShowFolderInfo()
        {
            Outlook.Folder folder =
                this.OutlookApp.Session.PickFolder()
                as Outlook.Folder;
            if (folder != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Folder EntryID:");
                sb.AppendLine(folder.EntryID);
                sb.AppendLine();
                sb.AppendLine("Folder StoreID:");
                sb.AppendLine(folder.StoreID);
                sb.AppendLine();
                sb.AppendLine("Unread Item Count: "
                    + folder.UnReadItemCount);
                sb.AppendLine("Default MessageClass: "
                    + folder.DefaultMessageClass);
                sb.AppendLine("Current View: "
                    + folder.CurrentView.Name);
                sb.AppendLine("Folder Path: "
                    + folder.FolderPath);
                MessageBox.Show(sb.ToString(),
                    "Folder Information",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                Outlook.Folder folderFromID =
                    this.OutlookApp.Session.GetFolderFromID(
                    folder.EntryID, folder.StoreID)
                    as Outlook.Folder;
                folderFromID.Display();
            }
        }


        /// <summary>
        /// Open the file path received in Excel. Then, open the workbook
        /// within the file. Send the workbook to the next function, the internal scan
        /// function. Will throw an exception if a file cannot be found or opened.
        /// </summary>
        public void ExcelOpenSpreadsheets(string thisFileName)
        {
            try
            {
                //
                // This mess of code opens an Excel workbook. I don't know what all
                // those arguments do, but they can be changed to influence behavior.
                //
                Excel.Workbook workBook = this.ExcelApp.Workbooks.Open(thisFileName,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing);

                //
                // Pass the workbook to a separate function. This new function
                // will iterate through the worksheets in the workbook.
                //
                ExcelScanIntenal(workBook);

                //
                // Clean up.
                //
                workBook.Close(false, thisFileName, null);
                Marshal.ReleaseComObject(workBook);
            }
            catch
            {
                //
                // Deal with exceptions.
                //
            }
        }

        /// <summary>
        /// Scan the selected Excel workbook and store the information in the cells
        /// for this workbook in an object[,] array. Then, call another method
        /// to process the data.
        /// </summary>
        private void ExcelScanIntenal(Excel.Workbook workBookIn)
        {
            //
            // Get sheet Count and store the number of sheets.
            //
            int numSheets = workBookIn.Sheets.Count;

            //
            // Iterate through the sheets. They are indexed starting at 1.
            //
            for (int sheetNum = 1; sheetNum < numSheets + 1; sheetNum++)
            {
                Excel.Worksheet sheet = (Excel.Worksheet)workBookIn.Sheets[sheetNum];

                //
                // Take the used range of the sheet. Finally, get an object array of all
                // of the cells in the sheet (their values). You can do things with those
                // values. See notes about compatibility.
                //
                Excel.Range excelRange = sheet.UsedRange;
                object[,] valueArray = (object[,])excelRange.get_Value(
                    Excel.XlRangeValueDataType.xlRangeValueDefault);

                //
                // Do something with the data in the array with a custom method.
                //
                //ProcessObjects(valueArray);
            }
        }

        public Excel.Application ExcelApp
        {
            get
            {
                return this.applicationObject as Excel.Application;
            }
        }

        public Outlook.Application OutlookApp
        {
            get
            {
                return this.applicationObject as Outlook.Application;
            }
        }


        #region Para nao validar os certificados de debug
        public static void InitiateSSLTrust()
        {
            try
            {
                //Change SSL checks so that all checks pass
                ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
            }
            catch (Exception ex)
            {

            }
        }

        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return true;
        }
        #endregion

        #endregion

        internal static Guid tokenRemoteQuery = Guid.Empty;
        //internal static Uri uriRemoteQuery = new Uri("https://carlosedufs.no-ip.biz:50626/WCFExcel/Service1.svc");
        internal static Uri uriRemoteQuery = new Uri("http://localhost:50625/WCFExcel/Service1.svc");
        internal static ConfigHelpers localConfig = ConfigHelpers.Load();

        internal object applicationObject;
        internal object addInInstance;
        private IRibbonUI ribbon;
    }
}