﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using MyCOMAddin;
using MyCOMAddin.DataContracts.FaultException;

namespace MyCOMAddin
{
    public partial class frmLogin : Form
    {
        #region "Properties"

        private bool _Authenticated = false;

        public bool Authenticated
        {
            get { return _Authenticated; }
            set { _Authenticated = value; }
        }
        private string _Username = "";

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        #endregion

        public frmLogin(string urlservice)
        {
            InitializeComponent();
            this.urlWCFService = urlservice;
            
            //Pega ultimas informacoes de login
            this.tbUsername.Text = Connect.localConfig.User;
            this.tbPassword.Text = Connect.localConfig.Password;
        }

        // exit the program if they do not want to login
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            //this.Close();
        }

        // check that username and password are not empty
        // lookup username in database and compare passwords - if ok continue
        // if not ok, retry 2 more times then exit
        private void btnLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Login()
        {

            if (tbPassword.Text.Length > 0 && tbUsername.Text.Length > 0)
            {
                if (UserAuthenticated(tbUsername.Text, tbPassword.Text))
                {
                    Authenticated = true;
                    //this.Close(); // close this form - do not exit the application
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    Authenticated = false;
                    //MessageBox.Show("Username or Password not recognised");

                }
            }
            else // password or username is empty
            {
                Authenticated = false;
                MessageBox.Show("You need to enter both a username and a password to continue",
                    "Login",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        // Does the user exist?
        // if so - is the password correct?
        private bool UserAuthenticated(string p, string p_2)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes("{\"user\":\""+ p + "\"," +
                    "\"password\":\"" + p_2 + "\"}");

                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json; charset=utf-8";

                try
                {
                    //data = client.DownloadData(urlWCFService + "/Login?u=" + p + "&p=" + p_2);
                    data = client.UploadData(this.urlWCFService + "/Login", "POST", data);

                    using (Stream stream = new MemoryStream(data))
                    {
                        DataContractJsonSerializer obj = new DataContractJsonSerializer(typeof(Guid));
                        var guid = (Guid)obj.ReadObject(stream);
                        Connect.tokenRemoteQuery = guid;

                        Connect.uriRemoteQuery = new Uri(this.urlWCFService);
                    }

                    //Atualiza ultimas informacoes de login
                    Connect.localConfig.User = p;
                    Connect.localConfig.Password = p_2;

                    Authenticated = true;
                    return true;
                }
                catch (WebException ex)
                {
                    if (ex.Response.ContentLength == 0)
                    {
                        MessageBox.Show("Problemas ao acessar serviço: " + ex.Message,
                            "Http code: " + ex.Status,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                    else
                    {
                        var buferro = Encoding.UTF8.GetBytes("Erro");// UIRemoteQueryForm.ReadFully(ex.Response.GetResponseStream());
                        var serro = Encoding.UTF8.GetString(buferro);
                        //serro = "{\"Message\":\"Unauthorized\",\"FaultType\":\"JsonFault\"}";
                        serro = serro.Replace("JsonFault:#ServiceApp.Handlers.FaultExceptions", "JsonFault:#MyCOMAddin.DataContracts.FaultException");
                        buferro = Encoding.UTF8.GetBytes(serro);
                        using (var ms = new MemoryStream(buferro))
                        {
                            try
                            {
                                var obj1 = new DataContractJsonSerializer(typeof(JsonFault), new[] { typeof(JsonFault), typeof(BaseFault) });
                                JsonFault jsonFault = obj1.ReadObject(ms) as JsonFault;

                                var msg = jsonFault.Detail == null ? jsonFault.Message : jsonFault.Detail.Message;

                                MessageBox.Show("Ocorreu um erro: " + msg,
                                    "Erro: " + jsonFault.FaultType,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                            }
                            catch (Exception ex1)
                            {
                                MessageBox.Show("Problemas ao acessar serviço: " + ex.Message,
                                           "Http code: " + ex.Status,
                                           MessageBoxButtons.OK,
                                           MessageBoxIcon.Error);
                            }
                        }
                    }
                }

            }
            catch (Exception) // FIXED: Added Exception catching  which defaults to Not Authenticated
            {
                Authenticated = false;
                return false;
            }
            Authenticated = false;
            return false;
        }

        // You do not need this
        private void usersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'applicationDataSet.Users' table. You can move, or remove it, as needed.
            //this.usersTableAdapter.Fill(this.applicationDataSet.Users); // you do not need this

        }

        // register the user - get Username, password x 2
        // Add user to database if passwords match
        private void llRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //frmRegister reg = new frmRegister();
            //reg.ShowDialog();
            //if ( reg.Registered )
            //    {
            //    Authenticated = true;
            //    this.Close();
            //    }
            //else
            //    {
            //    Authenticated = true;
            //    this.Close();
            //    }
            //}

            string url = this.urlWCFService;//Connect.uriRemoteQuery.ToString();

            if (InputBox.Show("Digite a URL do serviço WCF de consulta", "Endereço do serviço:", ref url) == DialogResult.OK)
            {
                //Testa a url baixa o wsdl
                try
                {
                    var wcfUri = new Uri(url + "?wsdl");

                    using (WebClient wc = new WebClient())
                    {
                        wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows; Windows NT 5.1; rv:1.9.2.4) Gecko/20100611 Firefox/3.6.4");
                        string html = wc.DownloadString(wcfUri);

                        var isWsdlOkay =  html.IndexOf("<?xml version=\"1.0\" encoding=\"utf-8\"?>") >=0 &&
                            html.IndexOf(@"<wsdl:message name=""IService1_GetData_InputMessage""><wsdl:part name=""parameters"" element=""tns:GetData""/></wsdl:message>") > 0;

                        if ( isWsdlOkay || ( html.IndexOf("You can do this using the svcutil.exe tool from the command line with the following syntax:") > 0
                            && html.IndexOf("/WCFExcel/Service1.svc?disco") > 0 ))
                        {
                            //Connect.uriRemoteQuery = new Uri(url);
                            this.urlWCFService = url;
                            
                            MessageBox.Show("Url do serviço configurada com sucesso!", "Registro de acesso ao serviço",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                            this.tbUsername.Focus();

                            return;
                        }
                    }

                    MessageBox.Show("Não foi possível ler o wsdl online do serviço", "Erro",
                       MessageBoxButtons.OK, MessageBoxIcon.Warning);                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro ao acessar a url do serviço: " + ex.Message, "Erro",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }                
            }
        }

        public string urlWCFService
        {
            get;
            private set;
        }
    }
}