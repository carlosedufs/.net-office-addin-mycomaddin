﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace MyCOMAddin.DataContracts.FaultException
{
    [DataContract]
    [KnownType(typeof(JsonFault))]
    public abstract class BaseFault
    {
        #region Properties
        ///
        /// The fault message
        ///
        [DataMember]
        public string Message
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class JsonFault : BaseFault
    {
        #region Properties
        ///
        /// The detail of the fault
        ///
        [DataMember]
        public BaseFault Detail
        {
            get;
            set;
        }

        ///
        /// The type of the fault
        ///
        [DataMember]
        public string FaultType
        {
            get;
            set;
        }
        #endregion
    }
}
