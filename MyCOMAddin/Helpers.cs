﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using MyCOMAddin.ServiceReference1;

using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;

namespace MyCOMAddin
{
    class ConfigHelpers
    {
        public string UrlWCFService
        {
            get;
            set;
        }

        public string User
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public static ConfigHelpers Load()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            try
            {
                try
                {
                    var user = config.AppSettings.Settings["User"].Value;
                    var pass = config.AppSettings.Settings["Password"].Value;
                    var urlService = config.AppSettings.Settings["UrlWCFService"].Value;

                    return new ConfigHelpers
                    {
                        User = user,
                        Password = pass,
                        UrlWCFService = urlService,
                    };
                }
                catch (Exception ex)
                {
                    return new ConfigHelpers
                    {
                        UrlWCFService = Connect.uriRemoteQuery.ToString(),
                    };
                }
            }
            finally
            {
                config = null;
            }
        }

        public void Save()
        {
            Save(this);
        }

        public static void Save(ConfigHelpers addinCfg)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (!config.AppSettings.Settings.AllKeys.Contains("User"))
                    config.AppSettings.Settings.Add("User", "");
                if (!config.AppSettings.Settings.AllKeys.Contains("Password"))
                    config.AppSettings.Settings.Add("Password", "");
                if (!config.AppSettings.Settings.AllKeys.Contains("UrlWCFService"))
                    config.AppSettings.Settings.Add("UrlWCFService", "");

                config.AppSettings.Settings["User"].Value = addinCfg.User;
                config.AppSettings.Settings["Password"].Value = addinCfg.Password;
                config.AppSettings.Settings["UrlWCFService"].Value = addinCfg.UrlWCFService;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {

            }
        }
    }

    class DataTableHelper
    {
        public static DataTable Parse(DataExtractorTable x)
        {
            DataTable dt = new DataTable();

            if (x.Columns != null && x.Columns.Length > 0)
            {
                foreach (var col in x.Columns)
                {
                    dt.Columns.Add(new DataColumn(col.Name, typeof(string)));
                }
                //dt.Columns.Add(new DataColumn("colBestBefore", typeof(DateTime)));
                //dt.Columns.Add(new DataColumn("colStatus", typeof(string)));
            }

            //dt.Columns["colStatus"].Expression = 
            //    String.Format("IIF(colBestBefore < #{0}#, 'Ok','Not ok')", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (x.Rows != null && x.Rows.Length > 0)
            {
                foreach (var rows in x.Rows.OrderBy( o => o.RowId ))
                {
                    List<string> rowCols = new List<string>();
                    foreach (var rowcol in rows.Rows)
                    {
                        rowCols.Add(rowcol.Value);
                    }
                    dt.Rows.Add(rowCols.ToArray());
                }
                //dt.Columns.Add(new DataColumn("colBestBefore", typeof(DateTime)));
                //dt.Columns.Add(new DataColumn("colStatus", typeof(string)));
            }

            return dt;
        }
    }

    /// <summary>
    /// http://www.dotnetperls.com/excel
    /// </summary>
    class ProgramChartHelper
    {
        const string fileName = "C:\\Book1.xlsx";
        const string topLeft = "A1";
        const string bottomRight = "A4";
        const string graphTitle = "Graph Title";
        const string xAxis = "Time";
        const string yAxis = "Value";

        public static void GenerateChart1(Excel.Application application)
        {
            // Open Excel and get first worksheet.
            //var application = new Application();
            var workbook = application.Workbooks.Open(fileName);
            var worksheet = workbook.Worksheets[1] as
                Microsoft.Office.Interop.Excel.Worksheet;

            // Add chart.
            var charts = worksheet.ChartObjects() as
                Microsoft.Office.Interop.Excel.ChartObjects;
            var chartObject = charts.Add(60, 10, 300, 300) as
                Microsoft.Office.Interop.Excel.ChartObject;
            var chart = chartObject.Chart;

            // Set chart range.
            var range = worksheet.get_Range(topLeft, bottomRight);
            chart.SetSourceData(range);

            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
            chart.ChartWizard(Source: range,
                Title: graphTitle,
                CategoryTitle: xAxis,
                ValueTitle: yAxis);

            // Save.
            workbook.Save();
        }

        public static void GenerateChart2(Excel.Application xlApp, bool inNewSheet)
        {
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            if (!inNewSheet)
            {
                xlWorkBook = xlApp.Application.ActiveWorkbook as Excel.Workbook;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Add(misValue);
                xlWorkSheet.Name = "Grafico " + (xlWorkBook.Worksheets.Count).ToString();
            }
            else
            {
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            }

            //add data 
            xlWorkSheet.Cells[1, 1] = "";
            xlWorkSheet.Cells[1, 2] = "Student1";
            xlWorkSheet.Cells[1, 3] = "Student2";
            xlWorkSheet.Cells[1, 4] = "Student3";

            xlWorkSheet.Cells[2, 1] = "Term1";
            xlWorkSheet.Cells[2, 2] = "80";
            xlWorkSheet.Cells[2, 3] = "65";
            xlWorkSheet.Cells[2, 4] = "45";

            xlWorkSheet.Cells[3, 1] = "Term2";
            xlWorkSheet.Cells[3, 2] = "78";
            xlWorkSheet.Cells[3, 3] = "72";
            xlWorkSheet.Cells[3, 4] = "60";

            xlWorkSheet.Cells[4, 1] = "Term3";
            xlWorkSheet.Cells[4, 2] = "82";
            xlWorkSheet.Cells[4, 3] = "80";
            xlWorkSheet.Cells[4, 4] = "65";

            xlWorkSheet.Cells[5, 1] = "Term4";
            xlWorkSheet.Cells[5, 2] = "75";
            xlWorkSheet.Cells[5, 3] = "82";
            xlWorkSheet.Cells[5, 4] = "68";

            Excel.Range chartRange;

            Excel.ChartObjects xlCharts = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
            Excel.ChartObject myChart = (Excel.ChartObject)xlCharts.Add(10, 80, 300, 250);
            Excel.Chart chartPage = myChart.Chart;

            chartRange = xlWorkSheet.get_Range("A1", "d5");
            chartPage.SetSourceData(chartRange, misValue);
            chartPage.ChartType = Excel.XlChartType.xlColumnClustered;

            try
            {
                //xlWorkBook.SaveAs("csharp.net-informations.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                //xlWorkBook.Close(true, misValue, misValue);
                //xlApp.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            //releaseObject(xlApp);

            //MessageBox.Show("Excel file created , you can find the file c:\\csharp.net-informations.xls");
        }

        public static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void FillSheet(Excel.Worksheet sheet, DataExtractorTable result)
        {
            if (sheet != null)
            {
                sheet.Cells.Select();
                if (sheet.UsedRange != null)
                    sheet.UsedRange.Clear();

                if (result != null && result.Rows.Length > 0)
                {
                    //Preenche as colunas
                    for (int i = 0; i < result.Columns.Length; i++)
                    {
                        sheet.Cells[1, 1 + i] = result.Columns[i].Name;
                    }

                    //preenche as linhas
                    for (int i = 0; i < result.Rows.Length; i++)
                    {
                        for (int c = 0; c < result.Rows[i].Rows.Length; c++)
                        {
                            sheet.Cells[2 + i, 1 + c] = result.Rows[i].Rows[c].Value;
                        }
                    }
                }

                if (sheet.UsedRange != null)
                    sheet.UsedRange.Columns.AutoFit();

                sheet.get_Range("a1").Select();
            }
        }
    }

}
