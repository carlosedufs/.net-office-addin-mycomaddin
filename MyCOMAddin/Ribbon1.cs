﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace MyCOMAddin
{
    [ComVisible(true)]
    [GuidAttribute("E850CFCF-7FB9-32D2-B6CE-AE66535E3B4C"), ProgId("MyCOMAddin.Ribbon1")]
    public class Ribbon1 : IRibbonExtensibility
    {
        private IRibbonUI ribbon;

        public Ribbon1()
        {
        }

        public Ribbon1(Connect addin)
        {
            this.AddIn = addin;
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("MyCOMAddin.Ribbon1.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, select the Ribbon XML item in Solution Explorer and then press F1

        public void Ribbon_Load(IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        public void OnTextButton(IRibbonControl control)
        {
            object missing = System.Type.Missing;
            System.Collections.Specialized.StringCollection columns = new System.Collections.Specialized.StringCollection();
            System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
            dlg.Filter = "Xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamWriter f = new StreamWriter(dlg.FileName);

                Excel.Worksheet sheet = this.ExcelApp.Application.ActiveSheet as Excel.Worksheet;
                Excel.Range r;

                //Extracting the list of columns
                for (int i = 65; i <= 90; i++)
                {
                    r = sheet.get_Range(((char)i).ToString() + "1", missing);
                    string value = (string)r.Value2;
                    if (value != null)
                    {
                        columns.Add(value);
                    }
                    else
                    {
                        break;
                    }
                }

                f.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                f.WriteLine("<" + sheet.Name + ">");

                //traversing through the rows
                int rowNo = 2;
                string[] rowValues = new string[columns.Count];
                do
                {

                    for (int i = 65; i < 65 + columns.Count; i++)
                    {
                        r = sheet.get_Range(((char)i).ToString() + rowNo.ToString(), missing);
                        rowValues[i - 65] = Convert.ToString(r.Value2);
                    }

                    if (IsRowNull(rowValues))
                    {
                        break;
                    }
                    else
                    {
                        f.WriteLine("<Row>");
                        for (int j = 0; j < rowValues.Length; j++)
                        {
                            f.Write("<" + columns[j] + ">");
                            f.Write(rowValues[j]);
                            f.Write("</" + columns[j] + ">");
                        }
                        f.WriteLine("</Row>");

                    }

                    rowNo++;
                } while (true);

                f.WriteLine("</" + sheet.Name + ">");
                f.Close();
            }
        }

        public void OnDownloadData(IRibbonControl control)
        {
           
        }

        private bool IsRowNull(string[] rowValues)
        {
            for (int i = 0; i < rowValues.Length; i++)
            {
                if (rowValues[i] != null) return false;
            }
            return true;
        }

        private System.Data.DataTable GenerateDatatable()
        {
            Range oRng = null;
            // It takes the current activesheet from the workbook. You can always pass any sheet as an argument

            Excel.Worksheet ws = this.ExcelApp.ActiveSheet as Excel.Worksheet;

            // set this value using your own function to read last used column, There are simple function to find last last used column
            int col = 4;
            // set this value using your own function to read last used row, There are simple function to find last last used rows
            int row = 5;

            string strRange = "A1";
            string andRange = "D5";

            System.Array arr = (System.Array)ws.get_Range(strRange, andRange).get_Value(Type.Missing);
            System.Data.DataTable dt = new System.Data.DataTable();
            for (int cnt = 1;
                cnt <= col; cnt++)
                dt.Columns.Add(new string ((char)cnt, 1), typeof(string));
            for (int i = 1; i <= row; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 1; j <= col; j++)
                {
                    if (arr.GetValue(i, j) != null)
                    {
                        dr[j - 1] = arr.GetValue(i, j).ToString();
                    }
                    else
                    {
                        dr[j - 1] = 0;
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        #endregion

        public Connect AddIn
        {
            get;
            set;
        }

        public Excel.Application ExcelApp
        {
            get
            {
                return this.AddIn.applicationObject as Excel.Application;
            }
         }

        #region Helpers

        internal static string GetResourceText()
        {
            return GetResourceText("MyCOMAddin.Ribbon1.xml");
        }

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
