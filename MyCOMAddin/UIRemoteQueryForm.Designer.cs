﻿namespace MyCOMAddin
{
    partial class UIRemoteQueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UIRemoteQueryForm));
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.btnExecutar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUrlServidor = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnPreencherSheet = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblErro = new System.Windows.Forms.RichTextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.verTabelasDisponíveisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarGráficoDeExemploToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesquisaExemplo1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesquisaExemplo2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlServidor = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.pnlServidor.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtQuery
            // 
            this.txtQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuery.Location = new System.Drawing.Point(6, 29);
            this.txtQuery.Multiline = true;
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQuery.Size = new System.Drawing.Size(468, 135);
            this.txtQuery.TabIndex = 0;
            // 
            // btnExecutar
            // 
            this.btnExecutar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecutar.Image = global::MyCOMAddin.Properties.Resources.check_13_20110903141405_00011;
            this.btnExecutar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExecutar.Location = new System.Drawing.Point(292, 173);
            this.btnExecutar.Name = "btnExecutar";
            this.btnExecutar.Size = new System.Drawing.Size(91, 23);
            this.btnExecutar.TabIndex = 1;
            this.btnExecutar.Text = "   Executar";
            this.btnExecutar.UseVisualStyleBackColor = true;
            this.btnExecutar.Click += new System.EventHandler(this.btnExecutar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Image = global::MyCOMAddin.Properties.Resources.cross_01_20110903141926_00002;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(389, 173);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 23);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "   Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Url do servidor:";
            // 
            // txtUrlServidor
            // 
            this.txtUrlServidor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrlServidor.Location = new System.Drawing.Point(88, 2);
            this.txtUrlServidor.Name = "txtUrlServidor";
            this.txtUrlServidor.ReadOnly = true;
            this.txtUrlServidor.Size = new System.Drawing.Size(408, 20);
            this.txtUrlServidor.TabIndex = 5;
            this.txtUrlServidor.Text = "http://carlosedufs.no-ip.biz:50625/WCFExcel/Service1.svc";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(3, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(495, 229);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtQuery);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnExecutar);
            this.tabPage1.Controls.Add(this.btnCancelar);
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(487, 202);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Executar consulta";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Digite a pesquisa a ser executa:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnPreencherSheet);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(487, 202);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Preview";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnPreencherSheet
            // 
            this.btnPreencherSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreencherSheet.Image = global::MyCOMAddin.Properties.Resources.excel;
            this.btnPreencherSheet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreencherSheet.Location = new System.Drawing.Point(361, 6);
            this.btnPreencherSheet.Name = "btnPreencherSheet";
            this.btnPreencherSheet.Size = new System.Drawing.Size(119, 23);
            this.btnPreencherSheet.TabIndex = 2;
            this.btnPreencherSheet.Text = "     Preencher sheet";
            this.btnPreencherSheet.UseVisualStyleBackColor = true;
            this.btnPreencherSheet.Click += new System.EventHandler(this.btnPreencherSheet_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Resultado da consulta:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 35);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(475, 159);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblErro);
            this.tabPage3.ImageIndex = 3;
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(10);
            this.tabPage3.Size = new System.Drawing.Size(487, 202);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Erros";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblErro
            // 
            this.lblErro.BackColor = System.Drawing.SystemColors.Control;
            this.lblErro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblErro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblErro.Location = new System.Drawing.Point(10, 10);
            this.lblErro.Name = "lblErro";
            this.lblErro.Size = new System.Drawing.Size(467, 165);
            this.lblErro.TabIndex = 0;
            this.lblErro.Text = "";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "insert_table.png");
            this.imageList1.Images.SetKeyName(1, "table.png");
            this.imageList1.Images.SetKeyName(2, "warning.png");
            this.imageList1.Images.SetKeyName(3, "error2.png");
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSplitButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(499, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton1.Text = "Sair";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verTabelasDisponíveisToolStripMenuItem,
            this.gerarGráficoDeExemploToolStripMenuItem,
            this.pesquisaExemplo1ToolStripMenuItem,
            this.pesquisaExemplo2ToolStripMenuItem,
            this.gerarPreviewToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(59, 22);
            this.toolStripSplitButton1.Text = "Opções";
            // 
            // verTabelasDisponíveisToolStripMenuItem
            // 
            this.verTabelasDisponíveisToolStripMenuItem.Name = "verTabelasDisponíveisToolStripMenuItem";
            this.verTabelasDisponíveisToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.verTabelasDisponíveisToolStripMenuItem.Text = "Ver tabelas disponíveis";
            this.verTabelasDisponíveisToolStripMenuItem.Click += new System.EventHandler(this.verTabelasDisponíveisToolStripMenuItem_Click);
            // 
            // gerarGráficoDeExemploToolStripMenuItem
            // 
            this.gerarGráficoDeExemploToolStripMenuItem.Name = "gerarGráficoDeExemploToolStripMenuItem";
            this.gerarGráficoDeExemploToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.gerarGráficoDeExemploToolStripMenuItem.Text = "Gerar gráfico de exemplo";
            this.gerarGráficoDeExemploToolStripMenuItem.Click += new System.EventHandler(this.gerarGráficoDeExemploToolStripMenuItem_Click);
            // 
            // pesquisaExemplo1ToolStripMenuItem
            // 
            this.pesquisaExemplo1ToolStripMenuItem.Name = "pesquisaExemplo1ToolStripMenuItem";
            this.pesquisaExemplo1ToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.pesquisaExemplo1ToolStripMenuItem.Text = "Pesquisa exemplo 1";
            this.pesquisaExemplo1ToolStripMenuItem.Click += new System.EventHandler(this.pesquisaExemplo1ToolStripMenuItem_Click);
            // 
            // pesquisaExemplo2ToolStripMenuItem
            // 
            this.pesquisaExemplo2ToolStripMenuItem.Name = "pesquisaExemplo2ToolStripMenuItem";
            this.pesquisaExemplo2ToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.pesquisaExemplo2ToolStripMenuItem.Text = "Pesquisa exemplo 2";
            this.pesquisaExemplo2ToolStripMenuItem.Click += new System.EventHandler(this.pesquisaExemplo2ToolStripMenuItem_Click);
            // 
            // gerarPreviewToolStripMenuItem
            // 
            this.gerarPreviewToolStripMenuItem.Checked = true;
            this.gerarPreviewToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gerarPreviewToolStripMenuItem.Name = "gerarPreviewToolStripMenuItem";
            this.gerarPreviewToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.gerarPreviewToolStripMenuItem.Text = "Gerar preview antes";
            this.gerarPreviewToolStripMenuItem.Click += new System.EventHandler(this.gerarPreviewToolStripMenuItem_Click);
            // 
            // pnlServidor
            // 
            this.pnlServidor.Controls.Add(this.label1);
            this.pnlServidor.Controls.Add(this.txtUrlServidor);
            this.pnlServidor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServidor.Location = new System.Drawing.Point(0, 25);
            this.pnlServidor.Name = "pnlServidor";
            this.pnlServidor.Size = new System.Drawing.Size(499, 25);
            this.pnlServidor.TabIndex = 8;
            // 
            // UIRemoteQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 285);
            this.Controls.Add(this.pnlServidor);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "UIRemoteQueryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remote Query Data";
            this.Load += new System.EventHandler(this.UIRemoteQueryForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pnlServidor.ResumeLayout(false);
            this.pnlServidor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtQuery;
        private System.Windows.Forms.Button btnExecutar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUrlServidor;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox lblErro;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel pnlServidor;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem verTabelasDisponíveisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarGráficoDeExemploToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesquisaExemplo1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesquisaExemplo2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarPreviewToolStripMenuItem;
        private System.Windows.Forms.Button btnPreencherSheet;
    }
}