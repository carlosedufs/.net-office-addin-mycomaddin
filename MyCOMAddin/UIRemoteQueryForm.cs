﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using MyCOMAddin.ServiceReference1;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using MyCOMAddin.DataContracts.FaultException;

namespace MyCOMAddin
{
    public partial class UIRemoteQueryForm : Form
    {
        public UIRemoteQueryForm(Excel.Application excel, Uri urlService)
        {
            InitializeComponent();
            this.Excel = excel;
            this.txtUrlServidor.Text = urlService.ToString();
        }

        private void UIRemoteQueryForm_Load(object sender, EventArgs e)
        {
            this.txtQuery.Text = @"SELECT CustomerID, CompanyName, ContactName, ContactTitle, Address, 
                City, Region, PostalCode, Country, Phone, Fax FROM Customers";
        }

        private void btnExecutar_Click(object sender, EventArgs e)
        {
            bool noproblem = false;
            DataExtractorTable result = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Application.DoEvents();
                try
                {
                    #region faz chamada e gera sheet de dados
                    btnPreencherSheet.Tag = null;
                    lblErro.Text = "";
                    this.dataGridView1.DataSource = null;
                    if (this.Excel != null)
                    {
                        Excel.Worksheet sheet = this.Excel.Application.ActiveSheet as Excel.Worksheet;

                        if (sheet != null)
                        {
                            sheet.Cells.Select();
                            if (sheet.UsedRange != null)
                                sheet.UsedRange.Clear();

                            WebClient client = new WebClient();
                            //byte[] data = client.DownloadData("https://localhost:50626/Service1.svc/GetDataByQuery/" + this.txtQuery.Text); // Argument is XX
                            //byte[] data = client.DownloadData(txtUrlServidor.Text + "/GetDataByQuery/" + this.txtQuery.Text); // Argument is XX
                            client.Headers["Content-type"] = "application/json";
                            client.Headers["token"] = Connect.tokenRemoteQuery.ToString();

                            byte[] paramb = null;

                            using (MemoryStream stream = new MemoryStream())
                            {
                                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(String));
                                serializer.WriteObject(stream, this.txtQuery.Text);
                                paramb = stream.ToArray();
                            }

                            byte[] data = null;

                            try
                            {
                                data = client.UploadData(txtUrlServidor.Text + "/GetDataByQuery", "POST", paramb);

                                using (Stream stream = new MemoryStream(data))
                                {
                                    DataContractJsonSerializer obj = new DataContractJsonSerializer(typeof(DataExtractorTable));
                                    btnPreencherSheet.Tag = result = obj.ReadObject(stream) as DataExtractorTable;
                                }

                                if (!gerarPreviewToolStripMenuItem.Checked)
                                {
                                    if (result != null && result.Rows.Length > 0)
                                    {
                                        //Preenche as colunas
                                        for (int i = 0; i < result.Columns.Length; i++)
                                        {
                                            sheet.Cells[1, 1 + i] = result.Columns[i].Name;
                                        }

                                        //preenche as linhas
                                        for (int i = 0; i < result.Rows.Length; i++)
                                        {
                                            for (int c = 0; c < result.Rows[i].Rows.Length; c++)
                                            {
                                                sheet.Cells[2 + i, 1 + c] = result.Rows[i].Rows[c].Value;
                                            }
                                        }
                                    }

                                    if (sheet.UsedRange != null)
                                        sheet.UsedRange.Columns.AutoFit();
                                }

                                sheet.get_Range("a1").Select();

                                Marshal.ReleaseComObject(sheet);

                                noproblem = true;

                                this.btnPreencherSheet.Enabled = gerarPreviewToolStripMenuItem.Checked;
                            }
                            catch (WebException ex)
                            {
                                this.lblErro.Text = ex.ToString();
                                if (ex.Response.ContentLength == 0)
                                {                                    
                                    MessageBox.Show("Problemas ao acessar serviço: " + ex.Message,
                                               "Http code: " + ex.Status,
                                               MessageBoxButtons.OK,
                                               MessageBoxIcon.Error);
                                }
                                else
                                {
                                    var buferro = ReadFully(ex.Response.GetResponseStream());
                                    var serro = Encoding.UTF8.GetString(buferro);
                                    //serro = "{\"Message\":\"Unauthorized\",\"FaultType\":\"JsonFault\"}";
                                    serro = serro.Replace("JsonFault:#ServiceApp.Handlers.FaultExceptions", "JsonFault:#MyCOMAddin.DataContracts.FaultException");
                                    buferro = Encoding.UTF8.GetBytes(serro);
                                    using (var ms = new MemoryStream(buferro))
                                    {
                                        try
                                        {
                                            var obj1 = new DataContractJsonSerializer(typeof(JsonFault), new[] { typeof(JsonFault), typeof(BaseFault) });
                                            JsonFault jsonFault = obj1.ReadObject(ms) as JsonFault;

                                            var msg = jsonFault.Detail == null ? jsonFault.Message : jsonFault.Detail.Message;

                                            MessageBox.Show("Ocorreu um erro: " + msg,
                                                "Erro: " + jsonFault.FaultType,
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Error);
                                        }
                                        catch (Exception ex1)
                                        {
                                            this.lblErro.Text = ex.ToString();
                                            MessageBox.Show("Problemas ao acessar serviço: " + ex.Message,
                                               "Http code: " + ex.Status,
                                               MessageBoxButtons.OK,
                                               MessageBoxIcon.Error);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    //Erro
                    this.lblErro.Text = ex.ToString();
                }
            }
            finally
            {
                if (noproblem)
                {
                    if (gerarPreviewToolStripMenuItem.Checked && result != null)
                    {
                        this.dataGridView1.DataSource = DataTableHelper.Parse(result);
                        this.tabControl1.SelectedTab = this.tabPage2;
                    }
                    else
                    {
                        this.Close();
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }

#if !NET40
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
#else
        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
#endif
        public Excel.Application Excel
        {
            get;
            private set;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pesquisaExemplo2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.txtQuery.Text = @"SELECT  EmployeeID, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, Notes, 
                      ReportsTo FROM         Employees";
            this.tabControl1.SelectedTab = this.tabPage1;
        }

        private void pesquisaExemplo1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.txtQuery.Text = @"SELECT CustomerID, CompanyName, ContactName, ContactTitle, Address, 
                City, Region, PostalCode, Country, Phone, Fax FROM Customers";
            this.tabControl1.SelectedTab = this.tabPage1;
        }

        private void gerarGráficoDeExemploToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramChartHelper.GenerateChart2(this.Excel, false);
            }
            catch (Exception ex)
            {
                this.lblErro.Text = ex.ToString();
            }
        }

        private void verTabelasDisponíveisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.txtQuery.Text = @"SELECT * FROM MSysObjects WHERE Type=1 AND Flags=0";
            this.btnExecutar_Click(sender, e);
            this.tabControl1.SelectedTab = this.tabPage2;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gerarPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gerarPreviewToolStripMenuItem.Checked = !gerarPreviewToolStripMenuItem.Checked;
        }

        private void btnPreencherSheet_Click(object sender, EventArgs e)
        {
            if (btnPreencherSheet.Tag != null && btnPreencherSheet.Tag is DataExtractorTable)
            {
                 lblErro.Text = "";
                 try
                 {
                     if (this.Excel != null)
                     {
                         Excel.Worksheet sheet = this.Excel.Application.ActiveSheet as Excel.Worksheet;

                         if (sheet != null)
                         {
                             ProgramChartHelper.FillSheet(sheet, (DataExtractorTable)btnPreencherSheet.Tag);
                         }

                         Marshal.ReleaseComObject(sheet);
                     }
                 }
                 catch (Exception ex)
                 {
                     lblErro.Text = ex.ToString();
                 }
            }
        }
    }
}
