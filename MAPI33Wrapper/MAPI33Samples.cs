﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using MAPI33;
using MAPI33.MapiTypes;
using System.Windows.Forms;
using System.Collections;

namespace MAPI33Wrapper
{
    public class MAPI33Samples
    {
        public static List<object> ListFolders()
        {
            List<object> ret = new List<object>();
            try
            {
                MapiWrapper wrapper = new MapiWrapper(null);
                //wrapper.MapiConnected = true;
                if (!wrapper.MapiConnected)
                {
                    Console.WriteLine("mapi não conectado");
                    return null;
                }

                var stores = wrapper.GetMessageStores();

                foreach (var store in stores)
                {
                    var iStore = wrapper.OpenMsgStore(store);
                    if (iStore != null)
                    {
                        Console.WriteLine(
                            ("Store: " + new MapiBinary(MAPI33.Tags.PR_ENTRYID, store.ab).ToString()) + "\r\n");

                        var entryFolder = wrapper.GetRootFolder(iStore);

                        var lstOfSf = wrapper.GetSubFolders(iStore, entryFolder);

                        ret.AddRange(
                                lstOfSf.Select(s => s.ToString()).ToArray()
                            );

                        iStore.Dispose();
                    }
                }

                wrapper.MapiConnected = false;
            }
            catch (COMException ex)
            {
                Console.WriteLine("Erro COMException: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro Exception: " + ex.Message);
            }

            return ret;
        }
    }

    class MapiWrapper
    {
        private const MAPI.FLAGS MAPI_FLAGS_USEDEFAULT = (MAPI.FLAGS)0x40; // Flag does not (yet) exist in Mapi33
        private IMAPISession session = null;
        private bool CanQuery = false;
        private Form Parent = null;

        public MapiWrapper(Form _Parent)
        {
            Parent = _Parent;
            MAPI_Init();
        }

        private bool MAPI_Init()
        {
            if (!MapiConnected)
            {
                //Status("Initialising MAPI session...");
                MapiConnected = true;

                // Status((mapi.MapiConnected ? "OK" : "Error") + "\r\n");
            }

            // if connection has not been esatblished, return false
            return MapiConnected;
        }


        /// <summary>
        /// Initialises/De-initialises the MAPI session, returns session state
        /// </summary>
        public bool MapiConnected
        {
            get
            {
                return (CanQuery && (session != null));
            }

            set
            {
                Error hr;

                if (value)
                {
                    if (session == null)
                    {
                        try
                        {
                            // initialise MAPI driver
                            hr = MAPI.Initialize(null);
                            // log on to a new MAPI session
                            // MAPI.FLAGS.Extended .. use extended mapi
                            // MAPI.FLAGS.LogonUI ... show dialog if logon details required
                            // MAPI.FLAGS.NoMail .. no mail support required, no check for new mail will be performed etc.
                            // MAPI_FLAGS_USEDEFAULT .. try to use default profile
                            hr = MAPI.LogonEx(IntPtr.Zero, null, null, MAPI.FLAGS.LogonUI | MAPI.FLAGS.Extended | MAPI.FLAGS.NoMail | MAPI_FLAGS_USEDEFAULT, out session);

                            CanQuery = (hr == Error.Success);
                        }
                        catch (Exception ex)
                        {
                            CanQuery = false;
                            if (Parent != null)
                                MessageBox.Show(Parent, "Error opening the MAPI connection: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                                throw ex;
                        }
                    }
                }
                else
                {
                    if (session != null)
                    {
                        try
                        {
                            // session logoff 
                            session.Logoff(IntPtr.Zero, 0);
                            // imortant! - all components must be disposed explicitly!
                            session.Dispose();
                            // close MAPI driver
                            MAPI.Uninitialize();
                            session = null;
                        }
                        catch
                        {
                            // session logoff failed - anyway...
                        }
                    }
                    CanQuery = false;
                }
            }
        }

        public IMAPISession Session
        {
            get
            {
                return session;
            }
        }

        /// <summary>
        /// Resolves a part of a recipient's name or an email-address to the fully qualified
        /// outlook recipient
        /// </summary>
        /// <param name="_Name">Part of recipient to resolve</param>
        /// <param name="_Handle">Handle of calling window (for showing mapi-dialogs modally)</param>
        /// <returns>Array with recipient's name and email-address</returns>
        public string[] ResolveName(string _Name, IntPtr _Handle)
        {
            Error hr;
            IAddrBook ab = null;
            string[][] txtRecipients = new string[1][];
            txtRecipients[0] = new string[0];

            if (!MapiConnected)
                return null;

            try
            {
                ADRENTRY[] abe;

                // try to open standard address book
                hr = session.OpenAddressBook(IntPtr.Zero, Guid.Empty, 0, out ab);

                if (hr != Error.Success)
                {
                    // error opening the address book
                    ab.Dispose();
                    return null;
                }

                abe = new ADRENTRY[1];
                abe[0] = new ADRENTRY(2);
                abe[0].PropVals[0] = new MapiString(Tags.PR_DISPLAY_NAME, _Name);
                abe[0].PropVals[1] = new MapiString(Tags.PR_EMAIL_ADDRESS, "");

                // first call WITHOUT showing any dialog... returns ether ambiguous, no_match or success...
                hr = ab.ResolveName(_Handle, 0, null, ref abe);

                if (hr == Error.AmbiguousRecip)
                {
                    abe = new ADRENTRY[1];
                    abe[0] = new ADRENTRY(1);
                    abe[0].PropVals[0] = new MapiString(Tags.PR_DISPLAY_NAME, _Name);

                    // once againg, this time a dialog for picking the desired recipient will be shown...
                    hr = ab.ResolveName(_Handle, IAddrBook.FLAGS.Dialog, null, ref abe);
                }

                if (hr == Error.Success)
                {
                    // now - get a suitable string representation of the selected recipient
                    // look at the used methods to see more
                    txtRecipients = NormalizeAddressEnties(GetEntryIdsFromABEntries(abe));
                }

                ab.Dispose();

                // empty if no matches
                return txtRecipients[0];
            }
            catch (Exception e)
            {
                ab.Dispose();
                return null;
            }
        }

        /// <summary>
        /// Shows the standard MAPI dialog for selecting one or multiple recipients
        /// </summary>
        /// <param name="_Handle">Handle of parent window (dialog will be shown modally)</param>
        /// <returns>Array of recipient names and email addresses</returns>
        public string[][] GetMapiRecipients(IntPtr _Handle)
        {
            Error hr;
            IAddrBook ab = null;
            string[][] txtRecipients = null;

            if (!MapiConnected)
                return new string[0][];

            try
            {
                // open address book
                hr = session.OpenAddressBook(IntPtr.Zero, Guid.Empty, 0, out ab);

                ADRENTRY[] abe = new ADRENTRY[0];
                ADRPARM abp = new ADRPARM();
                abp.Caption = "Choose your recipient(s)..."; // caption of the dialog
                abp.Flags = 0x21;
                abp.DestFields = 1; // how many categories (to/cc/bcc) do we have?
                abp.DestWellsTitle = "Add following recipients:";
                abp.DestComps = new ADRPARM.DESTCOMPS[] { ADRPARM.DESTCOMPS.Orig };
                abp.DestTitles = new String[] { "TO:" }; // title for our single category
                // show dialog
                hr = ab.Address(_Handle, ref abp, ref abe);

                // now "normalize" all selected recipients
                txtRecipients = NormalizeAddressEnties(GetEntryIdsFromABEntries(abe));

                // important - don't forget!
                ab.Dispose();

                return txtRecipients;

            }
            catch
            {
                ab.Dispose();
                return null;
            }
        }

        /// <summary>
        /// Gets the Entry IDs of Address Book Entries
        /// </summary>
        /// <param name="abe">Array of Address Book Entries to look up</param>
        /// <returns>Array of corresponding Entry IDs</returns>
        private ENTRYID[] GetEntryIdsFromABEntries(ADRENTRY[] abe)
        {
            ENTRYID[] entries = new ENTRYID[abe.Length];

            for (int i = 0; i < abe.Length; i++)
            {
                ADRENTRY aktEntry = abe[i];
                byte[] eidBytes = null;

                // parse aktEntry's propVals, look for ID of recipient object...
                for (int j = 0; j < aktEntry.PropVals.Length; j++)
                {
                    if (aktEntry.PropVals[j].PropTag == Tags.PR_MEMBER_ENTRYID)
                    {
                        eidBytes = (byte[])((MapiBinary)aktEntry.PropVals[j]).Value;
                        break;
                    }
                }

                // yes, we found an ENTRYID
                if (eidBytes != null)
                {
                    entries[i] = new ENTRYID(eidBytes);
                }
                else
                {
                    entries[i] = null;
                }
            }

            return entries;
        }

        /// <summary>
        /// Returns all recipient's name and email address as string
        /// </summary>
        /// <param name="abe">Array of recipients to lookup</param>
        /// <returns>Array of name/email string arrays</returns>
        private string[][] NormalizeAddressEnties(ENTRYID[] eids)
        {
            Error hr;
            IAddrBook ab = null;
            IUnknown unknown = null;
            IMailUser mu = null;
            List<string[]> resolvedRcps = new List<string[]>();

            if (!MapiConnected)
                return null;

            try
            {
                // open address book
                hr = session.OpenAddressBook(IntPtr.Zero, Guid.Empty, 0, out ab);

                if (hr != Error.Success)
                {
                    ab.Dispose();
                    return new string[0][];
                }

                for (int i = 0; i < eids.Length; i++)
                {

                    if (eids[i] != null)
                    {
                        ENTRYID ei = eids[i];
                        string[] entry = new string[2];
                        MAPI.TYPE retType;

                        // now we can open the recipient object by it's ENTRYID
                        hr = ab.OpenEntry(ei, Guid.Empty, IAddrBook.FLAGS.BestAccess, out retType, out unknown);

                        if (hr == Error.Success)
                        {
                            if (unknown != null)
                            {
                                // is out object an individual recipient (no group etc.)?
                                if (retType == MAPI.TYPE.IndividualRecipient)
                                {
                                    mu = (IMailUser)unknown;

                                    // now we query the following properties:
                                    Tags[] itags = new Tags[] { Tags.PR_GIVEN_NAME, Tags.PR_SURNAME, Tags.PR_SMTP_ADDRESS, Tags.PR_EMAIL_ADDRESS };

                                    MAPI33.MapiTypes.Value[] ivals = new MAPI33.MapiTypes.Value[0];
                                    // perform query
                                    hr = mu.GetProps(itags, IMAPIProp.FLAGS.Default, out ivals);

                                    // read attributes
                                    if (ivals.Length > 0)
                                    {
                                        for (int k = 0; k < ivals.Length; k++)
                                        {
                                            Tags attrTag = Tags.PR_DEBUG;
                                            string attrVal = "";

                                            if (ivals[k].GetType() == typeof(MapiString))
                                            {
                                                attrTag = ((MapiString)ivals[k]).PropTag;
                                                attrVal = ((MapiString)ivals[k]).Value;
                                            }
                                            if (ivals[k].GetType() == typeof(MapiUnicode))
                                            {
                                                attrTag = ((MapiUnicode)ivals[k]).PropTag;
                                                attrVal = ((MapiUnicode)ivals[k]).Value;
                                            }

                                            if (attrTag != Tags.PR_DEBUG && attrVal.Length > 0)
                                            {
                                                switch (attrTag)
                                                {
                                                    case Tags.PR_SURNAME_A:
                                                    case Tags.PR_SURNAME_W:
                                                        entry[0] = entry[0] + attrVal;
                                                        break;

                                                    case Tags.PR_GIVEN_NAME_A:
                                                    case Tags.PR_GIVEN_NAME_W:
                                                        entry[0] = attrVal + " " + entry[0];
                                                        break;

                                                    case Tags.PR_SMTP_ADDRESS_A:
                                                    case Tags.PR_SMTP_ADDRESS_W:
                                                    case Tags.PR_EMAIL_ADDRESS_A:
                                                    case Tags.PR_EMAIL_ADDRESS_W:
                                                        if (attrVal.IndexOf("@") != -1 && entry[1] == null)
                                                            entry[1] = attrVal;
                                                        break;
                                                }
                                            }
                                        }
                                    }

                                    mu.Dispose();
                                    mu = null;
                                }
                            }
                        }

                        // do we have an email address for the recipient?
                        if (entry[1] != null && entry[1].Length > 0)
                            resolvedRcps.Add(entry);

                        unknown.Dispose();
                        unknown = null;
                    }
                }
            }
            catch
            {
                // well, no success...
            }

            if (ab != null)
                ab.Dispose();

            if (unknown != null)
                unknown.Dispose();

            if (mu != null)
                mu.Dispose();

            return //(string[][])resolvedRcps.ToArray();
                resolvedRcps.ToArray();
        }

        /// <summary>
        /// This method "opens" a outlook PST file and reads all messages in the Inbox folder.
        /// It does that - AFAIK the only way - by attaching the PST message store to a 
        /// temporary profile.
        /// </summary>
        /// <param name="_Filename"></param>
        public string OpenMessageStoreFromFile(IntPtr _Handle, string _Filename)
        {
            if (!MapiConnected)
                throw new Exception("Error: MAPI not connected!!!!!");

            Error hr;
            string tmpProfileName = "TemporaryProfileToAccessPstFile";
            IProfAdmin adm;

            // we get the profile administration object
            hr = MAPI.AdminProfiles((MAPI.FLAGS)0, out adm);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // and create a new profile
            hr = adm.CreateProfile(tmpProfileName, null, _Handle, IProfAdmin.FLAGS.Default);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // now we get the administration object for our new profile
            IMsgServiceAdmin msa;
            hr = adm.AdminServices(tmpProfileName, null, _Handle, IProfAdmin.FLAGS.Default, out msa);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // and create a new message service of type "MS PST" (a pst file) in our profile
            hr = msa.CreateMsgService("MSPST MS", tmpProfileName, _Handle, IMsgServiceAdmin.FLAGS.ServiceUIAllowed);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // now we get the message service table
            IMAPITable tblMessageService;
            hr = msa.GetMsgServiceTable(IMsgServiceAdmin.FLAGS.Default, out tblMessageService);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // and retrieve the corresponding UID 
            Tags[] itags = new Tags[] { Tags.PR_SERVICE_UID, Tags.PR_DISPLAY_NAME };
            tblMessageService.SetColumns(itags, IMAPITable.FLAGS.Default);

            MAPI33.MapiTypes.Value[,] rows;
            hr = tblMessageService.QueryRows(1, IMAPITable.FLAGS.Default, out rows);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            tblMessageService.Dispose(); // don't need it any longer

            Guid msgSvcGuid = new Guid((byte[])((MAPI33.MapiTypes.MapiBinary)rows[0, 0]).Value);

            // here we configure our message service to use OUR pst file
            MAPI33.MapiTypes.Value[] ivals = new MAPI33.MapiTypes.Value[1];
            ivals[0] = new MapiString(Tags.PR_PST_PATH, _Filename);
            msa.ConfigureMsgService(msgSvcGuid, _Handle, IMsgServiceAdmin.FLAGS.ServiceUIAllowed, ivals);

            // never forget to dispose all the no longer used MAPI stuff
            // NOTE: we won't dispose IProfAdmin for now as we will need it later on to delete the temp profile
            msa.Dispose();

            // -------------------------------------------------------------------
            // ok. we attached the pst to our temp profile and got its name
            // now, let's see what we can do with it...

            // first we get a list of all message stores in our profile
            IMAPITable tblMsgStores;
            hr = session.GetMsgStoresTable(0, out tblMsgStores);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // no we define the cols we want to read
            itags = new Tags[] { Tags.PR_ENTRYID, Tags.PR_DISPLAY_NAME };
            tblMsgStores.SetColumns(itags, IMAPITable.FLAGS.Default);

            // then we simply take the first row (for we have only one message store, our pst)...
            hr = tblMsgStores.QueryRows(1, 0, out rows);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            tblMsgStores.Dispose(); // don't need it any longer

            // ... and read its ENTRYID and DISPLAY_NAME
            ENTRYID folderEid = null;
            // Guid folderGuid = Guid.Empty;

            if (rows[0, 0].GetType() == typeof(MapiBinary))
            {
                folderEid = new ENTRYID((byte[])((MapiBinary)rows[0, 0]).Value);
                // folderGuid = new Guid((byte[])((MapiBinary)rows[0,0]).Value);
            }

            string name = "";
            if (rows[0, 1].GetType() == typeof(MapiString))
                name = ((MapiString)rows[0, 1]).Value;

            if (name.Length == 0 || folderEid == null) throw new Exception("Error getting PST props!");

            // now we can open our message store
            IMsgStore store;
            hr = session.OpenMsgStore(_Handle, folderEid, Guid.Empty, IMAPISession.FLAGS.Default, out store);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // we will query the EID of the root folder now
            itags = new Tags[] { Tags.PR_IPM_SUBTREE_ENTRYID };
            ivals = new MAPI33.MapiTypes.Value[] { new MapiBinary(Tags.PR_IPM_SUBTREE_ENTRYID, new byte[0]) };

            hr = store.GetProps(itags, 0, out ivals);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            ENTRYID rootFldEid = new ENTRYID((byte[])((MapiBinary)ivals[0]).Value);

            // now we open the root folder...
            MAPI.TYPE type;
            MAPI33.IUnknown unkRootFolder = null;
            hr = store.OpenEntry(rootFldEid, Guid.Empty, 0, out type, out unkRootFolder);

            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());
            if (type != MAPI.TYPE.Folder) throw new Exception("Error: Wrong type");
            if (unkRootFolder == null) throw new Exception("Error: Expected folder is NULL");

            IMAPIFolder fldRootFolder = (IMAPIFolder)unkRootFolder;

            // ... and get a list of all contained objects
            MAPI33.IUnknown unkRootFolderObjects = null;
            hr = fldRootFolder.OpenProperty(Tags.PR_CONTAINER_HIERARCHY, IMAPITable.IID, 0, 0, out unkRootFolderObjects);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());
            if (unkRootFolderObjects == null) throw new Exception("Error: Expected table is NULL");

            IMAPITable tblRootFolderObjects = (IMAPITable)unkRootFolderObjects;

            // again, we define the cols we want to query
            itags = new Tags[] { Tags.PR_ENTRYID, Tags.PR_DISPLAY_NAME, Tags.PR_SUBFOLDERS };
            hr = tblRootFolderObjects.SetColumns(itags, 0);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // and perform the query
            hr = tblRootFolderObjects.QueryRows(50, 0, out rows);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // not disposing MAPI objects will cause an exception!
            unkRootFolder.Dispose();
            fldRootFolder.Dispose();
            unkRootFolderObjects.Dispose();
            tblRootFolderObjects.Dispose();

            // now we parse all containes objects to find the folder named "posteingang",
            // then we save the names of all contained items into a string and return it.
            // there are several other ways to fetch default folders (like inbox, contacts etc.),
            // but for this example, identification by name is way sufficient.
            ENTRYID fldInboxEid = null;
            for (int i = 0; i <= rows.GetUpperBound(0); i++)
            {
                string aktName = ((MapiString)rows[i, 1]).Value;
                if (aktName.ToLower().Equals("posteingang"))
                {
                    fldInboxEid = new ENTRYID((byte[])((MapiBinary)rows[i, 0]).Value);
                    break;
                }
            }

            if (fldInboxEid == null) throw new Exception("Error: Couldn't find inbox folder!");

            // here we open our inbox folder
            IUnknown unkInboxFolder;
            hr = store.OpenEntry(fldInboxEid, Guid.Empty, 0, out type, out unkInboxFolder);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());
            if (type != MAPI.TYPE.Folder) throw new Exception("Error: Wrong type");
            if (unkInboxFolder == null) throw new Exception("Error: Expected folder is NULL");

            IMAPIFolder fldInboxFolder = (IMAPIFolder)unkInboxFolder;

            // and get the content table
            IMAPITable tblInboxFolderObjects;
            hr = fldInboxFolder.GetContentsTable(0, out tblInboxFolderObjects);
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // we just want to retrieve the subject
            itags = new Tags[] { Tags.PR_SENDER_NAME, Tags.PR_SUBJECT };
            hr = tblInboxFolderObjects.SetColumns(itags, 0);

            // perform query
            hr = tblInboxFolderObjects.QueryRows(50, 0, out rows); // max. of 50 items
            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // not disposing MAPI objects will cause an exception!
            unkInboxFolder.Dispose();
            fldInboxFolder.Dispose();
            tblInboxFolderObjects.Dispose();
            store.Dispose();

            // now we can iterate through our results
            string result = "";
            for (int i = 0; i <= rows.GetUpperBound(0); i++)
            {
                result += string.Format("Msg from \"{0}\": {1}\r\n", ((MapiString)rows[i, 0]).Value, ((MapiString)rows[i, 1]).Value);
            }

            // ok, now we have what we wanted - let's remove the temporary profile and finish
            adm.DeleteProfile(tmpProfileName, 0);
            adm.Dispose(); // don't need it any longer			

            return result;
        }

        /// <summary>
        /// Gets all available MessageStores for the given session
        /// </summary>
        /// <param name="session"></param>
        /// <returns>Collection with the ENTRYIDs of the stores</returns>
        public List<ENTRYID> GetMessageStores()
        {
            List<ENTRYID> result = new List<ENTRYID>();

            Error err;
            Tags[] itags;

            // In Mapi33, the C++ data type SRowSet has been implemented as rectangular array
            MAPI33.MapiTypes.Value[,] rows;

            IMAPITable tblMsgStores;
            err = session.GetMsgStoresTable(MAPI33.IMAPISession.FLAGS.Default, out tblMsgStores);
            if (err != Error.Success)
            {
                throw new Exception("Error Getting Message stores: " + err.ToString());
            }

            //Define cols to read
            itags = new Tags[] { Tags.PR_ENTRYID };
            err = tblMsgStores.SetColumns(itags, IMAPITable.FLAGS.Default);
            if (err != Error.Success)
            {
                throw new Exception("Error setting Colums for Query: " + err.ToString());
            }


            // In Mapi33, the C++ data type SRowSet has been implemented as rectangular array
            //MAPI33.MapiTypes.Value[,] rows;
            err = tblMsgStores.QueryRows(int.MaxValue, IMAPITable.FLAGS.Default, out rows);
            if (err != Error.Success)
            {
                throw new Exception("Error while querying MsgStore: " + err.ToString());
            }

            tblMsgStores.Dispose();

            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i, 0].GetType() == typeof(MapiBinary))
                {
                    //convert the entry and add it to the result-collection
                    result.Add(new ENTRYID((byte[])((MapiBinary)rows[i, 0]).Value));
                }
            }

            return result;
        }

        /// <summary>
        /// Trys to open the Message store with BestAccess Permission
        /// Throws an Exception if something fails
        /// </summary>
        /// <param name="session">IMAPISession to work with</param>
        /// <param name="storeId">ENTRYID of the Message store to open</param>
        /// <returns>IMsgStore</returns>
        public IMsgStore OpenMsgStore(ENTRYID storeId)
        {
            IMsgStore result;
            //the first 3 params are very important
            //For Guid parameter you must pass Guid.Empty value. [1]
            //For parameter named as WindowHandle you must pass IntPtr.Zero value.[1]
            //you have to pass MAPI33.IMAPISession.FLAGS.BestAccess to manipulate items
            Error err = session.OpenMsgStore(IntPtr.Zero, storeId, Guid.Empty, MAPI33.IMAPISession.FLAGS.BestAccess, out result);

            if (err != Error.Success)
            {
                throw new Exception("Error opening MsgStore: " + err.ToString());
            }

            return result;
        }

        /// <summary>
        /// reads the properties from a IMAPIProp object and returns the Value array
        /// </summary>
        /// <param name="mapipropertyobject"></param>
        /// <returns></returns>
        public Value[] GetMAPIProperties(IMAPIProp mapipropertyobject)
        {
            Error err;
            Tags[] itags;
            Value[] ivals;

            //Get all available properties for the mapipropertyobject
            err = mapipropertyobject.GetPropList(MAPI33.IMAPIProp.FLAGS.Default, out itags);
            if (err != Error.Success)
            {
                throw new Exception("Error getting Propertylist: " + err.ToString());
            }

            //Read the properties of the object
            itags = new Tags[] { Tags.PR_SUBJECT, Tags.PR_SENDER_NAME }; //to read only Subject and SenderName property
            err = mapipropertyobject.GetProps(itags, MAPI33.IMAPIProp.FLAGS.Default, out ivals);
            if (err != Error.Success)
            {
                throw new Exception("Error getting Properties: " + err.ToString());
            }

            //Value contains 3 important things:
            //PropertyTag -> which MAPIProperty? PR_SENDER, PR_ENTRY_ID...
            //Type -> PT_STRING
            //Value -> the value of the property 
            return ivals; //In C# Arrays are "compatible" to the ICollection interface
        }

        public ENTRYID GetRootFolder(IMsgStore store)
        {
            Error err;
            //The PR_IPM_SUBTREE_ENTRYID property contains the entry
            //identifier of the root of the interpersonal message (IPM)
            //folder subtree in the message store's folder tree.
            Tags[] itags = new Tags[] { Tags.PR_IPM_SUBTREE_ENTRYID };
            Value[] ivals;// = new Value[] {new MapiBinary(Tags.PR_IPM_SUBTREE_ENTRYID, new byte[0])};

            err = store.GetProps(itags, IMAPIProp.FLAGS.Default, out ivals);
            if (err != Error.Success)
            {
                throw new Exception("Error getting Properties of Store: " + err.ToString());
            }

            //Create a new ENTRYID with the value we've read
            //Creation of ENTRYID changes sometimes!!
            ENTRYID rootFldEID = null;//new ENTRYID();
            //we wanted one Tag => we got one Value
            //it contains the ENTRYID of the RootFolder
            rootFldEID = new ENTRYID((byte[])((MapiBinary)ivals[0]).Value);

            return rootFldEID;
        }

        string inboxFolderName = "Posteingang";
        public List<ENTRYID> GetSubFolders(IMsgStore store, ENTRYID ei)
        {
            Error hr;

            List<ENTRYID> eiSubFolders = new List<ENTRYID>();

            //The PR_IPM_SUBTREE_ENTRYID property contains the entry
            //identifier of the root of the interpersonal message (IPM)
            //folder subtree in the message store's folder tree.
            Tags[] itags = new Tags[] { Tags.PR_IPM_SUBTREE_ENTRYID };
            Value[] ivals;// = new Value[] {new MapiBinary(Tags.PR_IPM_SUBTREE_ENTRYID, new byte[0])};
            MAPI33.MapiTypes.Value[,] rows;

            MAPI.TYPE type = MAPI.TYPE.Folder;
            MAPI33.IUnknown unkRootFolder = null;

            hr = store.OpenEntry(ei, Guid.Empty, __MAPI33__INTERNALS__.MsgStore.FLAGS.BestAccess, out type, out unkRootFolder);
            if (hr != Error.Success)
            {
                throw new Exception("Error getting Properties of Store: " + hr.ToString());
            }

            if (type != MAPI.TYPE.Folder) throw new Exception("Error: Wrong type");
            if (unkRootFolder == null) throw new Exception("Error: Expected folder is NULL");

            IMAPIFolder fldRootFolder = (IMAPIFolder)unkRootFolder;

            MAPI33.IUnknown unkRootFolderObjects = null;
            hr = fldRootFolder.OpenProperty(Tags.PR_CONTAINER_HIERARCHY, IMAPITable.IID, 0, 0,
                                                                  out unkRootFolderObjects);

            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());
            if (unkRootFolderObjects == null) throw new Exception("Error: Table is NULL");

            IMAPITable tblRootFolderObjects = (IMAPITable)unkRootFolderObjects;

            // Again, we define the cols we want to query
            itags = new Tags[] { Tags.PR_ENTRYID, Tags.PR_DISPLAY_NAME, Tags.PR_SUBFOLDERS };
            hr = tblRootFolderObjects.SetColumns(itags, 0);

            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // Perform the query
            hr = tblRootFolderObjects.QueryRows(50, 0, out rows);

            if (hr != Error.Success) throw new Exception("Error: " + hr.ToString());

            // Not disposing MAPI objects will cause an exception!
            unkRootFolder.Dispose();
            fldRootFolder.Dispose();
            unkRootFolderObjects.Dispose();
            tblRootFolderObjects.Dispose();

            // Now we iterate through all containes objects to find the inbox folder,
            // then we save the names of all contained items into a string and return it.
            // There are several other ways to fetch default folders (like inbox, contacts
            // etc.), but for this example, identification by name is way sufficient.

            ENTRYID fldInboxEid = null;
            for (int i = 0; i <= rows.GetUpperBound(0); i++)
            {
                eiSubFolders.Add(new ENTRYID((byte[])((MapiBinary)rows[i, 0]).Value));

                string aktName = ((MapiString)rows[i, 1]).Value;
                if (aktName.ToLower().Equals(inboxFolderName.ToLower()))
                {
                    // We found our folder :)
                    fldInboxEid = new ENTRYID((byte[])((MapiBinary)rows[i, 0]).Value);
                    break;
                }
            }

            //if (fldInboxEid == null) throw new Exception("Error: Couldn't find inbox!");

            return eiSubFolders;
        }
    }
}
