﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ManagedMAPI.Common.Interfaces;

namespace ManagedMAPI
{
    /// <summary>
    /// 
    /// </summary>
    public class MAPIFolder : IDisposable
    {
        public static readonly Guid IID_IMAPIFolder = new Guid(MAPIInterface.IID_IMAPIFolder);

        private IMAPIFolder _folder;
        private string _display;
        private EntryID _entryID;

        public MAPIFolder(string display, byte[] entryID, IMAPIFolder folder)
        {
            _folder = folder;
            _display = display;
            _entryID = new EntryID(entryID);
        }

        #region Public properties
        public IMAPIFolder IMAPIFolder
        {
            get
            {
                return _folder;
            }
        }

        public string DisplayName
        {
            get
            {
                return _display;
            }
        }

        public EntryID EntryID
        {
            get
            {
                return _entryID;
            }
        }

        public string sEntryID
        {
            get
            {
                return _entryID.ToString();
            }
        }

        public TypeFolderFlag FolderType
        {
            get;
            set;
        }

        public override string ToString()
        {
            string fName = this.DisplayName;

            switch (this.FolderType)
            {
                case TypeFolderFlag.Inbox:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case TypeFolderFlag.DeleteBox:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case TypeFolderFlag.Calendar:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case TypeFolderFlag.OutBox:
                    break;
                default:
                    fName += "(" + base.ToString() + ")";
                    break;
            }

            return fName;
        }
        #endregion

        #region Static

        /// <summary>
        /// Defines the particular properties and order of properties to appear as columns in the table.
        /// </summary>
        /// <param name="tags">An array of property tags identifying properties to be included as columns in the table</param>
        /// <returns></returns>
        public static uint[] GetPropsTagsArray(PropTags[] tags)
        {
            uint[] t = tags.Cast<uint>().ToArray();
            return GetPropsTagsArray(t);
        }

        public static uint[] GetPropsTagsArray(Common.MAPI33Imports.Tags[] tags)
        {
            uint[] t = tags.Cast<uint>().ToArray();
            return GetPropsTagsArray(t);
        }

        public static uint[] GetPropsTagsArray(uint[] tags)
        {
            uint[] t = new uint[tags.Length + 1];
            t[0] = (uint)tags.Length;
            for (int i = 0; i < tags.Length; i++)
                t[i + 1] = (uint)tags[i];
            return t;
        }
        #endregion

        #region IDisposable Interface
        /// <summary>
        /// Dispose MAPITable object.
        /// </summary>
        public void Dispose()
        {
            if (_folder != null)
            {
                Marshal.ReleaseComObject(_folder);
                _folder = null;
            }
        }

        #endregion

        #region enums
        public enum TypeFolderFlag
        {
            Nenhum,
            Inbox,
            Note,
            Calendar,
            OutBox,
            DeleteBox,
            SentMail,
            Contato,
            Jornal,
            Tarefa,
            Anotacoes
        }
        #endregion
    }
}
