﻿#region File Info
//
// File       : define.cs
// Description: MAPI Definitions
// Package    : ManagedMAPI
//
// Authors    : Fred Song
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ManagedMAPI
{
    #region MAPI Interface ID'S
    public struct MAPIInterface
    {
        // The Interface ID's are used to retrieve the specific MAPI Interfaces from the IUnknown Object
        public const string IID_IMAPISession = "00020300-0000-0000-C000-000000000046";
        public const string IID_IMAPIProp = "00020303-0000-0000-C000-000000000046";
        public const string IID_IMAPITable = "00020301-0000-0000-C000-000000000046";
        public const string IID_IMAPIMsgStore = "00020306-0000-0000-C000-000000000046";
        public const string IID_IMAPIFolder = "0002030C-0000-0000-C000-000000000046";
        public const string IID_IMAPISpoolerService = "0002031E-0000-0000-C000-000000000046";
        public const string IID_IMAPIStatus = "0002031E-0000-0000-C000-000000000046";
        public const string IID_IMessage = "00020307-0000-0000-C000-000000000046";
        public const string IID_IAddrBook = "00020309-0000-0000-C000-000000000046";
        public const string IID_IProfSect = "00020304-0000-0000-C000-000000000046";
        public const string IID_IMAPIContainer = "0002030B-0000-0000-C000-000000000046";
        public const string IID_IABContainer = "0002030D-0000-0000-C000-000000000046";
        public const string IID_IMsgServiceAdmin = "0002031D-0000-0000-C000-000000000046";
        public const string IID_IProfAdmin = "0002031C-0000-0000-C000-000000000046";
        public const string IID_IMailUser = "0002030A-0000-0000-C000-000000000046";
        public const string IID_IDistList = "0002030E-0000-0000-C000-000000000046";
        public const string IID_IAttachment = "00020308-0000-0000-C000-000000000046";
        public const string IID_IMAPIControl = "0002031B-0000-0000-C000-000000000046";
        public const string IID_IMAPILogonRemote = "00020346-0000-0000-C000-000000000046";
        public const string IID_IMAPIForm = "00020327-0000-0000-C000-000000000046";
    }
    #endregion

    #region public MAPI definitions

    /// <summary>
    /// A mask of values that indicate the types of notification events that the caller is interested in and should be included in the registration. There is a corresponding NOTIFICATION structure associated with each type of event that holds information about the event.
    /// </summary>
    public enum EEventMask : uint
    {
        /// <summary>
        /// Registers for notifications about severe errors, such as insufficient memory.
        /// </summary>
        fnevCriticalError = 0x00000001,
        /// <summary>
        /// Registers for notifications about the arrival of new messages.
        /// </summary>
        fnevNewMail = 0x00000002,
        /// <summary>
        /// Registers for notifications about the creation of a new folder or message.
        /// </summary>
        fnevObjectCreated = 0x00000004,
        /// <summary>
        /// Registers for notifications about a folder or message being deleted.
        /// </summary>
        fnevObjectDeleted = 0x00000008,
        /// <summary>
        /// Registers for notifications about a folder or message being modified.
        /// </summary>
        fnevObjectModified = 0x00000010,
        /// <summary>
        /// Registers for notifications about a folder or message being moved.
        /// </summary>
        fnevObjectMoved = 0x00000020,
        /// <summary>
        /// Registers for notifications about a folder or message being copied.
        /// </summary>
        fnevObjectCopied = 0x00000040,
        /// <summary>
        /// Registers for notifications about the completion of a search operation.
        /// </summary>
        fnevSearchComplete = 0x00000080,
        /// <summary>
        /// Registers for notifications about a table being modified.
        /// </summary>
        fnevTableModified = 0x00000100,
        /// <summary>
        /// Registers for notifications about a status object being modified.
        /// </summary>
        fnevStatusObjectModified = 0x00000200,
        /// <summary>
        /// Reserved
        /// </summary>
        fnevReservedForMapi = 0x40000000,
        /// <summary>
        /// Registers for notifications about events specific to the particular message store provider.
        /// </summary>
        fnevExtended = 0x80000000,
    }
    /// <summary>
    /// Describes a row from a table containing selected properties for a specific object.
    /// </summary>
    public struct SRow
    {
        /// <summary>
        /// An array of SPropValue structures that describe the property values for the columns in the row. 
        /// </summary>
        public IPropValue[] propVals;
    }

    /// <summary>
    /// Bitmask of flags that controls the mapi character set.
    /// </summary>
    public enum CharacterSet : uint
    {
        /// <summary>
        /// Ansi
        /// </summary>
        ANSI = 0,
        /// <summary>
        /// Unicode
        /// </summary>
        UNICODE = 0x80000000
    }

    /// <summary>
    /// Generic MAPI Bitmask
    /// </summary>
    public enum MAPIFlag : uint
    {
        LOGON_UI = 0x00000001,
        /// <summary>
        /// An attempt should be made to create a new MAPI session instead of acquiring the shared session. 
        /// </summary>
        NEW_SESSION = 0x00000002,
        /// <summary>
        /// An attempt should be made to download all of the user's messages before returning. If it is not set, messages can be downloaded in the background after the call to MAPILogonEx returns.
        /// </summary>
        FORCE_DOWNLOAD = 0x00001000,
        /// <summary>
        /// A dialog box should be displayed to prompt the user for logon information if required.
        /// </summary>
        MAPI_LOGON_UI = 0x00000001,
        /// <summary>
        /// The shared session should be returned, which allows later clients to obtain the session without providing any user credentials.
        /// </summary>
        ALLOW_OTHERS = 0x00000008,
        /// <summary>
        /// The default profile should not be used and the user should be required to supply a profile.
        /// </summary>
        EXPLICIT_PROFILE = 0x00000010,
        /// <summary>
        /// Log on with extended capabilities. This flag should always be set.
        /// </summary>
        EXTENDED = 0x00000020,
        /// <summary>
        /// MAPILogonEx should display a configuration dialog box for each message service in the profile. 
        /// </summary>
        SERVICE_UI_ALWAYS = 0x00002000,
        /// <summary>
        /// MAPI should not inform the MAPI spooler of the session's existence.
        /// </summary>
        NO_MAIL = 0x00008000,
        /// <summary>
        /// The messaging subsystem should substitute the profile name of the default profile for the Profile Name parameter. T
        /// </summary>
        USE_DEFAULT = 0x00000040,
        /// <summary>
        /// Requests that the object be opened by using the maximum network permissions allowed for the user and the maximum client application access. 
        /// </summary>
        BEST_ACCESS = 0x00000010,
        /// <summary>
        /// MAPI spooler is a process that is responsible for sending messages to and receiving messages from a messaging system.
        /// </summary>
        SPOOLER = 37,
        /// <summary>
        /// Suppresses the display of dialog boxes. If the AB_NO_DIALOG flag is not set, the address book providers that contribute to the integrated address book can prompt the user for any necessary information.
        /// </summary>
        AB_NO_DIALOG = 0x00000001,
        /// <summary>
        /// The ulUIParam parameter is ignored unless the MAPI_DIALOG flag is set.
        /// </summary>
        MAPI_DIALOG = 0x00000008,
    }

    /// <summary>
    /// Bitmask of flags that controls the flush operation.
    /// </summary>
    public enum FlushFlag
    {
        /// <summary>
        /// The outbound message queues should be flushed.
        /// </summary>
        UPLOAD = 2,
        /// <summary>
        /// The inbound message queues should be flushed.
        /// </summary>
        DOWNLOAD = 4,
        /// <summary>
        /// The flush operation should occur regardless, in spite of the possibility of performance degradation. This flag must be set when targeting an asynchronous transport provider.
        /// </summary>
        FORCE = 8,
        /// <summary>
        /// The status object should not display a progress indicator. This flag is used only by the MAPI spooler; providers ignore this flag.
        /// </summary>
        NO_UI = 16,
        /// <summary>
        /// The flush operation can occur asynchronously. This flag only applies to the MAPI spooler's status object. 
        /// </summary>
        ASYNC_OK = 32
    }

    /// <summary>
    /// Type of MAPI object
    /// </summary>
    public enum ObjectType : uint
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,
        /// <summary>
        /// Message Store
        /// </summary>
        MAPI_STORE = 0x00000001,
        /// <summary>
        /// Address Book
        /// </summary>
        MAPI_ADDRBOOK = 0x00000002,
        /// <summary>
        /// Folder
        /// </summary>
        MAPI_FOLDER = 0x00000003,
        /// <summary>
        /// Address Book Container
        /// </summary>
        MAPI_ABCONT = 0x00000004,
        /// <summary>
        /// Message
        /// </summary>
        MAPI_MESSAGE = 0x00000005,
        /// <summary>
        /// Individual Recipient
        /// </summary>
        MAPI_MAILUSER = 0x00000006,
        /// <summary>
        /// Attachment
        /// </summary>
        MAPI_ATTACH = 0x00000007,
        /// <summary>
        /// Distribution List Recipient
        /// </summary>
        MAPI_DISTLIST = 0x00000008,
        /// <summary>
        /// Profile Section
        /// </summary>
        MAPI_PROFSECT = 0x00000009,
        /// <summary>
        ///  Status Object
        /// </summary>
        MAPI_STATUS = 0x0000000A,
        /// <summary>
        /// Session
        /// </summary>
        MAPI_SESSION = 0x0000000B,
        /// <summary>
        /// Form Information
        /// </summary>
        MAPI_FORMINFO = 0x0000000C,
    }

    #endregion

    #region internal definitions
    [StructLayout(LayoutKind.Explicit)]
    public struct _PV
    {
        [FieldOffset(0)]
        public Int16 i;
        [FieldOffset(0)]
        public int l;
        [FieldOffset(0)]
        public uint ul;
        [FieldOffset(0)]
        public float flt;
        [FieldOffset(0)]
        public double dbl;
        [FieldOffset(0)]
        public UInt16 b;
        [FieldOffset(0)]
        public double at;
        [FieldOffset(0)]
        public IntPtr lpszA;
        [FieldOffset(0)]
        public IntPtr lpszW;
        [FieldOffset(0)]
        public IntPtr lpguid;
        /*[FieldOffset(0)]
        public IntPtr bin;*/
        [FieldOffset(0)]
        public UInt64 li;
        [FieldOffset(0)]
        public SBinary bin;
        [FieldOffset(0)]
        public SBinaryArray MVbin;
    }

    //    typedef struct _MAPIERROR
    //{
    //  ULONG ulVersion;
    //  LPSTR lpszError;
    //  LPSTR lpszComponent;
    //  ULONG ulLowLevelError;
    //  ULONG ulContext;
    //} MAPIERROR, FAR * LPMAPIERROR;
    [StructLayout(LayoutKind.Sequential)]
    internal struct MAPIError
    {
        public UInt32 ulVersion;
        [MarshalAs(UnmanagedType.LPStr)]
        public StringBuilder lpszError;
        [MarshalAs(UnmanagedType.LPStr)]
        public StringBuilder lpszComponent;
        public UInt32 ulLowLevelError;
        public UInt32 ulContext;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SPropValue
    {
        public UInt32 ulPropTag;
        public UInt32 dwAlignPad;
        public _PV Value;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SMultivalueStructure
    {
        public ushort PropType; // Type of returned value
        public ushort Command; //Command we entered (PR_EMS_PROXY...)
        public uint dwAlignPad; // Reserved - usually 4 bytes of 0
        public uint stringArrayLength; // SWStringArray length
        public uint pStringArrayMemoryAddress; //SWStringArray pointer to string array
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ENTRYID
    {
        /// <summary>
        /// Count of bytes in the entry identifier pointed to by the lpEntryID member.
        /// </summary>
        public uint cbEntryID;

        /// <summary>
        /// Pointer to the entry identifier of the newly arrived message.
        /// </summary>
        public IntPtr pEntryID;
    }

    /// <summary>
    ///  The bookmark identifying the starting position for the seek operation.
    /// </summary>
    public enum BookMark
    {
        /// <summary>
        /// Starts the seek operation from the beginning of the table.
        /// </summary>
        BEGINNING = 0,
        /// <summary>
        /// Starts the seek operation from the row in the table where the cursor is located.
        /// </summary>
        CURRENT = 1,
        /// <summary>
        /// Starts the seek operation from the end of the table.
        /// </summary>
        END = 2
    }

    // Nested Types
    [Flags]
    public enum OpenEntryFlags
    {
        BestAccess = 16,
        Default = 0,
        DeferredErrors = 8,
        LogoffAbort = 8,
        LogoffInBound = 131072,
        LogoffNoWait = 1,
        LogoffOrderly = 2,
        LogoffOutBound = 262144,
        LogoffOutBoundQueue = 524288,
        LogoffPurge = 4,
        LogoffQuiet = 16,
        Modify = 1,
        NoCache = 512,
        Unicode = -2147483648
    }

    /// <summary>
    /// Storage instantiation modes.
    /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/wceappservices5/html/wce50lrfSTGM127.asp
    /// </summary>
    [Flags]
    public enum STGM : uint
    {
        // Access STGM_READ 0x00000000L 
        //   STGM_WRITE 0x00000001L 
        //   STGM_READWRITE 0x00000002L 
        // Sharing STGM_SHARE_DENY_NONE 0x00000040L 
        //   STGM_SHARE_DENY_READ 0x00000030L 
        //   STGM_SHARE_DENY_WRITE 0x00000020L 
        //   STGM_SHARE_EXCLUSIVE 0x00000010L 
        //   STGM_PRIORITY 0x00040000L 
        // Creation STGM_CREATE 0x00001000L 
        //   STGM_CONVERT 0x00020000L 
        //   STGM_FAILIFTHERE 0x00000000L 
        // Transactioning STGM_DIRECT 0x00000000L 
        //   STGM_TRANSACTED 0x00010000L 
        // Transactioning Performance STGM_NOSCRATCH 0x00100000L 
        //   STGM_NOSNAPSHOT 0x00200000L 
        // Direct SWMR and Simple STGM_SIMPLE 0x08000000L 
        //   STGM_DIRECT_SWMR 0x00400000L 
        // Delete On Release STGM_DELETEONRELEASE 0x04000000L 


        /// <summary>
        /// In direct mode, each change to a storage element is written as it occurs. This is the default. 
        /// </summary>
        STGM_DIRECT = 0x00000000,


        /// <summary>
        /// In transacted mode, changes are buffered and are written only if an explicit commit operationis called. 
        /// To ignore the changes, call the Revert method in the IStream, IStorage, or IPropertyStorage
        /// interfaces. 
        /// The COM compound file and NSS implementations of IStorage do not support transacted streams,
        /// which means that streams can be opened only in direct mode, and you cannot revert changes to them. 
        /// Transacted storages are, however, supported. 
        /// The compound file, NSS standalone, and NTFS implementations of IPropertySetStorage similarly
        /// do not support transacted, simple mode property sets as these property sets are stored in streams. 
        /// However, non-simple property sets, which can be created by specifying the
        /// PROPSETFLAG_NONSIMPLE flag in the grfFlags parameter of IPropertySetStorage::Create, are supported. 
        /// </summary>
        STGM_TRANSACTED = 0x00010000,


        /// <summary>
        /// STGM_SIMPLE is a mode that provides a much faster implementation of a compound file in a limited,
        /// but frequently used case. It is described in detail in the following Remarks section. 
        /// </summary>
        STGM_SIMPLE = 0x08000000,


        /// <summary>
        /// The STGM_DIRECT_SWMR supports direct mode for single-writer, multi-reader file operations. 
        /// </summary>
        STGM_DIRECT_SWMR = 0x00400000,


        /// <summary>
        /// For stream objects, STGM_READ allows you to call the ISequentialStream::Read method. For storage
        /// objects, you can enumerate the storage elements and open them for reading. 
        /// </summary>
        STGM_READ = 0x00000000,


        /// <summary>
        /// STGM_WRITE lets you save changes to the object. 
        /// </summary>
        STGM_WRITE = 0x00000001,


        /// <summary>
        /// STGM_READWRITE is the combination of STGM_READ and STGM_WRITE. 
        /// </summary>
        STGM_READWRITE = 0x00000002,


        /// <summary>
        /// Specifies that subsequent openings of the object are not denied read or write access. 
        /// </summary>
        STGM_SHARE_DENY_NONE = 0x00000040,


        /// <summary>
        /// Prevents others from subsequently opening the object in STGM_READ mode. It is typically used on
        /// a root storage object. 
        /// </summary>
        STGM_SHARE_DENY_READ = 0x00000030,


        /// <summary>
        /// Prevents others from subsequently opening the object in STGM_WRITE mode. This value is typically
        /// used to prevent unnecessary copies made of an object opened by multiple users. 
        /// If this value is not specified, a snapshot is made, independent of whether there are subsequent
        /// opens or not. Thus, you can improve performance by specifying this value. 
        /// </summary>
        STGM_SHARE_DENY_WRITE = 0x00000020,


        /// <summary>
        /// The combination of STGM_SHARE_DENY_READ and STGM_SHARE_DENY_WRITE. 
        /// </summary>
        STGM_SHARE_EXCLUSIVE = 0x00000010,


        /// <summary>
        /// Opens the storage object with exclusive access to the most recently committed version. Thus,
        /// other users cannot commit changes to the object while you have it open in priority mode. 
        /// You gain performance benefits for copy operations, but you prevent others from committing
        /// changes. So, you should limit the time you keep objects open in priority mode. You must specify
        /// STGM_DIRECT and STGM_READ with priority mode. 
        /// </summary>
        STGM_PRIORITY = 0x00040000,


        /// <summary>
        /// Indicates that the underlying file is to be automatically destroyed when the root storage
        /// object is released. This capability is most useful for creating temporary files. 
        /// </summary>
        STGM_DELETEONRELEASE = 0x04000000,


        /// <summary>
        /// Indicates that an existing storage object or stream should be removed before the new one replaces
        /// it. A new object is created when this flag is specified, only if the existing object has been
        /// successfully removed. 
        /// This flag is used in the following situations: 
        ///   When you are trying to create a storage object on disk but a file of that name already exists. 
        ///   When you are trying to create a stream inside a storage object but a stream with the specified
        ///   name already exists. 
        ///   When you are creating a byte array object but one with the specified name already exists. 
        /// </summary>
        STGM_CREATE = 0x00001000,


        /// <summary>
        /// Creates the new object while preserving existing data in a stream named CONTENTS. In the case of
        /// a storage object or a byte array, the old data is flattened to a stream regardless of whether the
        /// existing file or byte array currently contains a layered storage object. 
        /// </summary>
        STGM_CONVERT = 0x00020000,


        /// <summary>
        /// Causes the create operation to fail if an existing object with the specified name exists. In this
        /// case, STG_E_FILEALREADYEXISTS is returned. STGM_FAILIFTHERE applies to both storage objects and
        /// streams. 
        /// </summary>
        STGM_FAILIFTHERE = 0x00000000,


        /// <summary>
        /// In transacted mode, a scratch file is usually used to save until the commit operation. Specifying
        /// STGM_NOSCRATCH permits the unused portion of the original file to be used as scratch space. This
        /// does not affect the data in the original file, and is a much more efficient use of memory. 
        /// </summary>
        STGM_NOSCRATCH = 0x00100000,


        /// <summary>
        /// This flag is used when opening a storage with STGM_TRANSACTED and without STGM_SHARE_EXCLUSIVE
        /// or STGM_SHARE_DENY_WRITE. 
        /// In this case, specifying STGM_NOSNAPSHOT prevents the system-provided implementation from
        /// creating a snapshot copy of the file. 
        /// Instead, changes to the file are written to the end of the file. Unused space is not reclaimed
        /// unless consolidation is done during the commit, and there is only one current writer on the file. 
        /// When the file is opened in no snapshot mode, another open cannot be done without specifying
        /// STGM_NOSNAPSHOT — in other words, you cannot combine no-snapshot with other modes. 
        /// </summary>
        STGM_NOSNAPSHOT = 0x00200000
    } // End enum STGM

    /// <summary>
    /// The STGC enumeration constants specify the conditions for
    /// performing the Commit operation in the IStorage::Commit
    /// and IStream::Commit methods.
    /// Remarks
    /// You can specify STGC_DEFAULT or some Combination of
    /// STGC_OVERWRITE, STGC_ONLYIFCURRENT, and
    /// STGC_DANGEROUSLYComMITMERELYTODISKCACHE for normal Commit
    /// operations. You can specify STGC_CONSOLIDATE with any other
    /// STGC flags.
    /// Typically, use STGC_ONLYIFCURRENT to protect the storage
    /// object in cases where more than one user can edit the object
    /// simultaneously.
    /// 
    /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/stg/stg/stgc.asp
    /// </summary>
    [Flags]
    public enum STGC : uint
    {
        /// <summary>
        /// You can specify this condition with STGC_CONSOLIDATE, or some Combination of the other three flags in this list of elements. Use this value to increase the readability of code.
        /// </summary>
        STGC_DEFAULT = 0,


        /// <summary>
        /// The Commit operation can overwrite existing data to reduce
        /// overall space requirements. This value is not reCommended for
        /// typical usage because it is not as robust as the default value.
        /// In this case, it is possible for the Commit operation to fail
        /// after the old data is overwritten, but before the new data is
        /// Completely Committed. Then, neither the old version nor the
        /// new version of the storage object will be intact.
        /// You can use this value in the following cases:
        /// * The user is willing to risk losing the data.
        /// * The low-memory save sequence will be used to safely save the
        ///   storage object to a smaller file.
        /// * A previous Commit returned STG_E_MEDIUMFULL, but overwriting
        ///   the existing data would provide enough space to Commit changes
        ///   to the storage object.
        /// Be aware that the Commit operation verifies that adequate space
        /// exists before any overwriting occurs. Thus, even with this value
        /// specified, if the Commit operation fails due to space requirements,
        /// the old data is safe. It is possible, however, for data loss to
        /// occur with the STGC_OVERWRITE value specified if the Commit
        /// operation fails for any reason other than lack of disk space.
        /// </summary>
        STGC_OVERWRITE = 1,


        /// <summary>
        /// Prevents multiple users of a storage object from overwriting each
        /// other's changes. The Commit operation occurs only if there have
        /// been no changes to the saved storage object because the user most
        /// recently opened it. Thus, the saved version of the storage object
        /// is the same version that the user has been editing. If other users
        /// have changed the storage object, the Commit operation fails and
        /// returns the STG_E_NOTCURRENT value. To override this behavior, call
        /// the IStorage::Commit or IStream::Commit method again using the
        /// STGC_DEFAULT value.
        /// </summary>
        STGC_ONLYIFCURRENT = 2,


        /// <summary>
        /// Commits the changes to a write-behind disk cache, but does not save
        /// the cache to the disk. In a write-behind disk cache, the operation
        /// that writes to disk actually writes to a disk cache, thus increasing
        /// performance. The cache is eventually written to the disk, but usually
        /// not until after the write operation has already returned. The 
        /// performance increase Comes at the expense of an increased risk of
        /// losing data if a problem occurs before the cache is saved and the
        /// data in the cache is lost.
        /// If you do not specify this value, then Committing changes to root-level
        /// storage objects is robust even if a disk cache is used. The two-phase
        /// Commit process ensures that data is stored on the disk and not just to
        /// the disk cache.
        /// </summary>
        STGC_DANGEROUSLYCOMMITMERELYTODISKCACHE = 4,


        /// <summary>
        /// Windows 2000 and Windows XP: Indicates that a storage should be
        /// consolidated after it is Committed, resulting in a smaller file on disk.
        /// This flag is valid only on the outermost storage object that has been
        /// opened in transacted mode. It is not valid for streams. The
        /// STGC_CONSOLIDATE flag can be Combined with any other STGC flags.
        /// </summary>
        STGC_CONSOLIDATE = 8
    } // End enum STGC

    delegate void OnAdviseCallbackHandler(IntPtr pContext, uint cNotification, IntPtr lpNotifications);

    #endregion
}
