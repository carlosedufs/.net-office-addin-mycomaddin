﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using ManagedMAPI.Common.Interfaces;
using iop = System.Runtime.InteropServices;

namespace ManagedMAPI
{
    public class MAPIMessage : IDisposable
    {
        public static readonly Guid IID_IMessage = new Guid(MAPIInterface.IID_IMessage);
        public static readonly Guid IID_IStream = new Guid("0000000c-0000-0000-C000-000000000046");
        public const uint MAPI_MODIFY = 0x00000001;
        public const uint STORE_UNCOMPRESSED_RTF = 0x00008000;

        private IMAPIProp _message;
        private string _display;
        private EntryID _entryID;
        private IMsgStore _msgStore;
        private IMAPIFolder _folder;

        public MAPIMessage(IMsgStore msgStore, IMAPIFolder _folder, IMAPIMessage message)
        {
            this._message = message;
            this._msgStore = msgStore;
            this._folder = _folder;
        }

        public MAPIMessage(IMAPIProp message)
        {
            this._message = message;
        }

        public string[] GetProperties()
        {
            IntPtr unk = MAPINative.NULL;
            IntPtr lpStream = MAPINative.NULL;

            try
            {
                var hr = this._message.OpenProperty(
                        (uint)Common.MAPI33Imports.Tags.PR_BODY,
                        IID_IStream, 0, 0, out unk);

                if (hr != HRESULT.S_OK) return null;

                Guid x = IID_IStream;
                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(unk, ref x, out lpStream) != (int)HRESULT.S_OK) return null;

                System.Runtime.InteropServices.ComTypes.IStream pStream =
                        Marshal.GetObjectForIUnknown(lpStream) as System.Runtime.InteropServices.ComTypes.IStream;

                string content2 = null;
                string contentHtml = null;

                using ( MemoryStream  ms = new MemoryStream() ) {
                    byte[] buffer = new byte[1024];
                    int offset = 0;
                    int read = 0;
                    ComStreamWrapper csw = new ComStreamWrapper(pStream);
                    do
                    {
                        read = csw.Read(buffer, 0, buffer.Length);
                        ms.Write(buffer, 0, read);
                    } while (read > 0);

                    content2 = System.Text.Encoding.Unicode.GetString(ms.ToArray());
                }
                Marshal.ReleaseComObject(pStream);

                MAPINative.MAPIFreeBuffer(lpStream);
                MAPINative.MAPIFreeBuffer(unk);

                hr = this._message.OpenProperty(
                       (uint)Common.MAPI33Imports.Tags.PR_HTML,
                       IID_IStream, 0, 0, out unk);
                if (hr != HRESULT.S_OK) return null;

                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(unk, ref x, out lpStream) != (int)HRESULT.S_OK) return null;

                pStream = Marshal.GetObjectForIUnknown(lpStream) as System.Runtime.InteropServices.ComTypes.IStream;

                using (MemoryStream ms = new MemoryStream())
                {
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    ComStreamWrapper csw = new ComStreamWrapper(pStream);
                    do
                    {
                        read = csw.Read(buffer, 0, buffer.Length);
                        ms.Write(buffer, 0, read);
                    } while (read > 0);

                    contentHtml = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                }

                Marshal.ReleaseComObject(pStream);

                MAPINative.MAPIFreeBuffer(lpStream);
                MAPINative.MAPIFreeBuffer(unk);

                //troca mensagem
                var newMsg = System.Text.Encoding.Unicode.GetBytes("Trocado na data de " + DateTime.Now.ToString());

                hr = this._message.OpenProperty(
                      (uint)Common.MAPI33Imports.Tags.PR_BODY,
                      IID_IStream, 0, MAPI_MODIFY, out unk);
                if (hr != HRESULT.S_OK) return null;

                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(unk, ref x, out lpStream) != (int)HRESULT.S_OK) return null;

                pStream = Marshal.GetObjectForIUnknown(lpStream) as System.Runtime.InteropServices.ComTypes.IStream;

                ComStreamWrapper csw2 = new ComStreamWrapper(pStream);
                csw2.Write(newMsg, 0, newMsg.Length);
                csw2.SetLength(newMsg.Length);

                if (this._message.SaveChanges((uint)MessageSaveOptions.FORCE_SAVE | (uint)MessageSaveOptions.KEEP_OPEN_READWRITE) != HRESULT.S_OK) return null;

                Marshal.ReleaseComObject(pStream);

                MAPINative.MAPIFreeBuffer(lpStream);
                MAPINative.MAPIFreeBuffer(unk);

                return new[] { contentHtml , content2, System.Text.Encoding.Unicode.GetString(newMsg)};

            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public string GetBody()
        {
            IntPtr unk = MAPINative.NULL;
            IntPtr lpStream = MAPINative.NULL;

            try
            {
                var hr = this._message.OpenProperty(
                        (uint)Common.MAPI33Imports.Tags.PR_BODY,
                        IID_IStream, 0, 0, out unk);

                if (hr != HRESULT.S_OK) return null;

                Guid x = IID_IStream;
                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(unk, ref x, out lpStream) != (int)HRESULT.S_OK) return null;

                System.Runtime.InteropServices.ComTypes.IStream pStream =
                        Marshal.GetObjectForIUnknown(lpStream) as System.Runtime.InteropServices.ComTypes.IStream;

                string content2 = null;
                string contentHtml = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    byte[] buffer = new byte[1024];
                    int offset = 0;
                    int read = 0;
                    ComStreamWrapper csw = new ComStreamWrapper(pStream);
                    do
                    {
                        read = csw.Read(buffer, 0, buffer.Length);
                        ms.Write(buffer, 0, read);
                    } while (read > 0);

                    content2 = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                }
                Marshal.ReleaseComObject(pStream);

                MAPINative.MAPIFreeBuffer(lpStream);
                MAPINative.MAPIFreeBuffer(unk);

                return content2;

            }
            catch (Exception ex)
            {
                throw ex;
            }  
        }

        public void WriteNewBody(string body)
        {
            IntPtr unk = MAPINative.NULL;
            IntPtr lpStream = MAPINative.NULL;

            try
            {
                var hr = this._message.OpenProperty(
                        (uint)Common.MAPI33Imports.Tags.PR_BODY,
                        IID_IStream, (uint)STGM.STGM_READWRITE, MAPI_MODIFY, out unk);
                if (hr != HRESULT.S_OK) return;

                Guid x = IID_IStream;

                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(unk, ref x, out lpStream) != (int)HRESULT.S_OK) return;

                var pStream = Marshal.GetObjectForIUnknown(lpStream) as System.Runtime.InteropServices.ComTypes.IStream;

                var newBody = System.Text.Encoding.Unicode.GetBytes(body);

                ComStreamWrapper csw2 = new ComStreamWrapper(pStream);
                csw2.Write(newBody, 0, newBody.Length);
                csw2.SetLength(newBody.Length);
                csw2.Commit();

                var lpMessagePtr = Marshal.GetIUnknownForObject(this._message);

                bool lpMsgUpdate = false;
                if (MAPINative.RTFSync(lpMessagePtr, (uint)MapiRTFFlags.RTF_SYNC_RTF_CHANGED, out lpMsgUpdate) == HRESULT.S_OK)
                {
                    //opa, pode salver
                    if (this._message.SaveChanges(
                        (uint)MessageSaveOptions.FORCE_SAVE |
                        (uint)MessageSaveOptions.KEEP_OPEN_READWRITE) != HRESULT.S_OK) 
                        return;
                }               

                Marshal.Release(lpMessagePtr);
                var x1 = Marshal.ReleaseComObject(pStream);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ( lpStream != MAPINative.NULL )
                    MAPINative.MAPIFreeBuffer(lpStream);
                if (unk != MAPINative.NULL)
                    MAPINative.MAPIFreeBuffer(unk);
            }

            //var xbody = GetBody();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IMAPIMessage GetMessageFromEntry(object container, EntryID id)
        {
            uint tpEntry;
            IntPtr iEntry = MAPINative.NULL;
            IntPtr pEntryID = MAPINative.NULL;

            try
            {
                pEntryID = Marshal.AllocHGlobal(id.AsByteArray.Length * IntPtr.Size);

                Marshal.Copy(id.AsByteArray, 0, pEntryID, id.AsByteArray.Length);

                var hresult =
                    container is IMsgStore ? ((IMsgStore)container).OpenEntry((uint)id.AsByteArray.Length, pEntryID, IntPtr.Zero, (uint)OpenEntryFlags.BestAccess, out tpEntry, out iEntry) :
                        ((IMAPIContainer)container).OpenEntry((uint)id.AsByteArray.Length, pEntryID, IntPtr.Zero, (uint)OpenEntryFlags.BestAccess, out tpEntry, out iEntry);

                if (hresult != ManagedMAPI.HRESULT.S_OK) return null;

                if ((ObjectType)tpEntry == ObjectType.MAPI_FOLDER)
                {
                    return Marshal.GetObjectForIUnknown(iEntry) as IMAPIMessage;
                }
            }
            finally
            {
                if (pEntryID != MAPINative.NULL) Marshal.FreeHGlobal(pEntryID);
            }

            return null;
        }

        #region IDisposable Interface
        /// <summary>
        /// Dispose MAPITable object.
        /// </summary>
        public void Dispose()
        {
            if (_message != null)
            {
                //var x1 = Marshal.ReleaseComObject(_message);
                //_message = null;
            }
        }

        #endregion

        enum MessageSaveOptions : uint
        {
            KEEP_OPEN_READONLY = 0x00000001,
            KEEP_OPEN_READWRITE = 0x00000002,
            FORCE_SAVE = 0x00000004,
        }

        public class ComStreamWrapper : System.IO.Stream
        {
            private System.Runtime.InteropServices.ComTypes.IStream mSource;
            private IntPtr mInt64;

            public ComStreamWrapper(System.Runtime.InteropServices.ComTypes.IStream source)
            {
                mSource = source;
                mInt64 = iop.Marshal.AllocCoTaskMem(8);
            }

            ~ComStreamWrapper()
            {
                iop.Marshal.FreeCoTaskMem(mInt64);
            }

            public override bool CanRead { get { return true; } }
            public override bool CanSeek { get { return true; } }
            public override bool CanWrite { get { return true; } }

            public override void Flush()
            {
                mSource.Commit(0);
            }

            public override long Length
            {
                get
                {
                    System.Runtime.InteropServices.ComTypes.STATSTG stat;
                    mSource.Stat(out stat, 1);
                    return stat.cbSize;
                }
            }

            public override long Position
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                if (offset != 0) throw new NotImplementedException();
                mSource.Read(buffer, count, mInt64);
                return iop.Marshal.ReadInt32(mInt64);
            }

            public override long Seek(long offset, System.IO.SeekOrigin origin)
            {
                mSource.Seek(offset, (int)origin, mInt64);
                return iop.Marshal.ReadInt64(mInt64);
            }

            public override void SetLength(long value)
            {
                mSource.SetSize(value);
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                if (offset != 0) throw new NotImplementedException();
                mSource.Write(buffer, count, IntPtr.Zero);
            }

            public void Commit() {
                mSource.Commit((int)STGC.STGC_OVERWRITE);
            }
        }
    }
}
