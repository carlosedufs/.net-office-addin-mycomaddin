﻿#region File Info
//
// File       : sbinary.cs
// Description: SBinary struct
// Package    : ManagedMAPI
//
// Authors    : Fred Song
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ManagedMAPI
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SBinary
    {
        public uint cb;
        public IntPtr lpb;

        public byte[] AsBytes
        {
            get
            {
                byte[] b = new byte[cb];
                for (int i = 0; i < cb; i++)
                    b[i] = Marshal.ReadByte(lpb, i);
                return b;
            }
        }

        public static SBinary SBinaryCreate(byte[] data)
        {
            SBinary b;
            b.cb = (uint)data.Length;
            b.lpb = Marshal.AllocHGlobal((int)b.cb);
            for (int i = 0; i < b.cb; i++)
                Marshal.WriteByte(b.lpb, i, data[i]);
            return b;
        }

        public static void SBinaryRelease(ref SBinary b)
        {
            if (b.lpb != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(b.lpb);
                b.lpb = IntPtr.Zero;
                b.cb = 0;
            }
        }

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SBinaryArray
    {
        public uint cValues;
        public IntPtr lpbin;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SBinaryWrapper
    {
        //public uint cb;
        public IntPtr lpb;

        public SBinaryWrapper(IntPtr lpb)
        {
            this.lpb = lpb;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SBinaryArrayWrapper
    {
        public IntPtr lpbin;
        public SBinary[] lpBinary;

        public SBinaryArrayWrapper(SBinaryArray item)
        {
            this.lpbin = item.lpbin;

            int pIntSize = IntPtr.Size, intSize = Marshal.SizeOf(typeof(Int32));
            int sizeOfSRow = 2 * intSize + pIntSize;
            IntPtr rows = new IntPtr((uint)this.lpbin + intSize);

            if (item.cValues > 0)
            //for (int i = 0; i < item.cValues; i++)
            {
                //IntPtr pRowOffset = new IntPtr((uint)rows + (i * sizeOfSRow));
                //uint cValues = (uint)Marshal.ReadInt32(new IntPtr((uint)pRowOffset + pIntSize));
                IntPtr pProps = item.lpbin; //Marshal.ReadIntPtr(new IntPtr((uint)pRowOffset + pIntSize + intSize));

                //IPropValue[] lpProps = new IPropValue[item.cValues];
                //for (int j = 0; j < item.cValues; j++) // each column
                //{
                //    IntPtr pointer = new IntPtr((uint)pProps + (j * Marshal.SizeOf(typeof(SPropValue))));

                //    SPropValue lpProp = (SPropValue)Marshal.PtrToStructure(pointer, typeof(SPropValue));
                //    lpProps[j] = new MAPIProp(lpProp);
                //}                
                //sRows[i].propVals = lpProps;
                lpBinary = new SBinary[item.cValues];
                for (int j = 0; j < item.cValues; j++) // each column
                {
                    IntPtr pointer = new IntPtr((uint)pProps + (j * Marshal.SizeOf(typeof(SBinary))));
                    lpBinary[j] = (SBinary)Marshal.PtrToStructure(pointer, typeof(SBinary));
                }
            }
            else
            {
                lpBinary = null;
            }
        }
    }
}
