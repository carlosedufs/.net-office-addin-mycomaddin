﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ManagedMAPI.Common.MAPI33Imports
{
    /// <summary>
    /// retirada da blibioteca MAPI33
    /// </summary>
    public abstract class Unknown : IDisposable
    {
        // Fields
        internal unsafe void* inside;
        internal string trace;
        internal static TraceSwitch ts = new TraceSwitch("MAPI33", "");

        // Methods
        internal unsafe Unknown(void* pUnknown)
        {
            if (ts.TraceVerbose)
            {
                this.trace = this.StackTrace();
            }
            this.inside = pUnknown;
        }

        ~Unknown()
        {
            GC.SuppressFinalize(this);
            this.xFinalize();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private unsafe void Dispose([MarshalAs(UnmanagedType.U1)] bool __unnamed000)
        {
            if (this.inside == null)
            {
                string str;
                if (ts.TraceVerbose)
                {
                    str = "\nThe object has been disposed at:\n" + this.trace;
                }
                else
                {
                    str = "";
                }
                throw new ObjectDisposedException(base.GetType().ToString(), str);
            }
            if (ts.TraceVerbose)
            {
                this.trace = this.StackTrace();
            }
            //**(((int*) this.inside))[8](this.inside);
            this.inside = null;
        }

        protected void xFinalize()
        {
            string str;
            if (ts.TraceVerbose)
            {
                str = "\nThe object has been created at:\n" + this.trace;
            }
            else
            {
                str = "";
            }
            throw new NotSupportedException("MAPI33: The object " + base.GetType().ToString() + " has not been disposed." + str);
        }

        private string StackTrace()
        {
            StackTrace trace = new StackTrace(1, true);
            StringBuilder builder = new StringBuilder();
            int index = 0;
            if (0 < trace.FrameCount)
            {
                do
                {
                    StackFrame frame = trace.GetFrame(index);
                    if (frame.GetMethod().DeclaringType.Assembly != base.GetType().Assembly)
                    {
                        /*
                        string str;
                        string str2;
                        string[] strArray = new string[$ConstGCArrayBound$0x33a81a75$1$];
                        strArray[0] = frame.GetMethod().DeclaringType.ToString() + "." + frame.GetMethod().ToString().Substring(frame.GetMethod().ToString().IndexOf(" ") + 1);
                        if (frame.GetFileLineNumber() > 0)
                        {
                            str2 = " in " + frame.GetFileName();
                        }
                        else
                        {
                            str2 = "";
                        }
                        strArray[1] = str2;
                        if (frame.GetFileLineNumber() > 0)
                        {
                            str = ":line " + frame.GetFileLineNumber().ToString();
                        }
                        else
                        {
                            str = "";
                        }
                        strArray[2] = str;
                        builder.AppendFormat("   {0}{1}{2}\n", (object[]) strArray);
                        */
                    }
                    index++;
                }
                while (index < trace.FrameCount);
            }
            return builder.ToString();
        }

        // Properties
        internal IntPtr INSIDE
        {
            get
            {
                //string str;
                //if (this.inside != null)
                //{
                //    return this.inside;
                //}
                //if (ts.TraceVerbose)
                //{
                //    str = "\nThe object has been disposed at:\n" + this.trace;
                //}
                //else
                //{
                //    str = "";
                //}
                throw new ObjectDisposedException(base.GetType().ToString(), "\nThe object has been disposed at:\n");
            }
        }

        public IntPtr NATIVEOBJECT
        {
            get
            {
                //if (this.inside == null)
                //{
                //    string str;
                //    if (ts.TraceVerbose)
                //    {
                //        str = "\nThe object has been disposed at:\n" + this.trace;
                //    }
                //    else
                //    {
                //        str = "";
                //    }
                //    throw new ObjectDisposedException(base.GetType().ToString(), str);
                //}
                //**(((int*) this.inside))[4](this.inside);
                IntPtr ptr = new IntPtr();
                return ptr; // new IntPtr(this.inside);
            }
        }

    }

    public class _IUnknown : Unknown
    {
        // Fields
        public static Guid IID = new Guid("00000000-0000-0000-C000-000000000046");

        // Methods
        internal unsafe _IUnknown(void* p) : base(p)
        {
        }
    }

}
