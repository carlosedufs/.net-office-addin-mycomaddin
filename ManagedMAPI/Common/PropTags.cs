﻿#region File Info
//
// File       : proptag.cs
// Description: PropTags enum
// Package    : ManagedMAPI
//
// Authors    : Fred Song
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManagedMAPI
{
    public enum PropTags : uint
    {
        /// <summary>
        /// Contains the display name of the message store.
        /// <br></br>Data Type: PT_TSTRING
        /// </summary>
        PR_DISPLAY_NAME = PT.PT_TSTRING | 0x3001 << 16,

        /// <summary>
        /// Contains TRUE if a message store is the default message store in the message store table.
        /// <br></br>Data Type: PT_BOOLEAN
        /// </summary>
        PR_DEFAULT_STORE = PT.PT_BOOLEAN | 0x3400 << 16,

        /// <summary>
        ///  Contains a MAPI entry identifier used to open and edit properties of a particular MAPI object.
        ///  <br></br>Data Type: PT_BINARY
        /// </summary>
        PR_ENTRYID = PT.PT_BINARY | 0x0FFF << 16,

        /// <summary>
        /// Contains a value indicating the service provider type.
        /// <br></br>Data Type: PT_LONG
        /// </summary>
        PR_RESOURCE_TYPE = PT.PT_LONG | 0x3E03 << 16,

               /// <summary>
        /// Used to get the Emailheaders
        /// </summary>
        PR_TRANSPORT_MESSAGE_HEADERS = 0x007D001E,

        /// <summary>
        /// Used to read the Body of an Email
        /// </summary>
        PR_BODY = 0x1000001E,

        /// <summary>
        /// Used to read the HTML Body of the Email
        /// </summary>
        PR_BODY_HTML = 0x1013001E,

        /// <summary>
        /// Used to read the HTML Body of the Email
        /// </summary>
        PR_HTML = 0x10130102,

        /// <summary>
        /// Used to read the smtp / exchange sender address of an Email
        /// </summary>
        PR_SENDER_EMAIL_ADDRESS = 0x0C1F001E,

        /// <summary>
        /// 
        /// </summary>
        //PR_ENTRYID = 0x0FFF0102,

        // attachment handling constants
        // added PB/NextUs
        PR_ATTACH_ADDITIONAL_INFO = 0x370F0102,
        PR_ATTACH_DATA_BIN = 0x37010102,
        PR_ATTACH_DATA_OBJ = 0x3701000D,
        PR_ATTACH_ENCODING = 0x37020102,
        PR_ATTACH_EXTENSION = 0x3703001E,
        PR_ATTACH_FILENAME = 0x3704001E,
        PR_ATTACH_LONG_FILENAME = 0x3707001E,
        PR_ATTACH_LONG_PATHNAME = 0x370D001E,
        PR_ATTACH_METHOD = 0x37050003,
        PR_ATTACH_MIME_TAG = 0x370E001E,
        PR_ATTACH_NUM = 0x0E210003,
        PR_ATTACH_PATHNAME = 0x3708001E,
        PR_ATTACH_RENDERING = 0x37090102,
        PR_ATTACH_SIZE = 0x0E200003,
        PR_ATTACH_TAG = 0x370A0102,
        PR_ATTACH_TRANSPORT_NAME = 0x370C001E,
        // end attachment

        PT_NULL = 1,	/* NULL property value */
        PT_I2 = 2,	/* Signed 16-bit value */
        PT_LONG = 3,	/* Signed 32-bit value */
        PT_R4 = 4,	/* 4-byte floating point */
        PT_DOUBLE = 5,	/* Floating point double */
        PT_CURRENCY = 6,	/* Signed 64-bit int (decimal w/	4 digits right of decimal pt) */
        PT_APPTIME = 7,	/* Application time */
        PT_ERROR = 10,	/* 32-bit error value */
        PT_BOOLEAN = 11,	/* 16-bit boolean (non-zero true) */
        PT_OBJECT = 13,	/* Embedded object in a property */
        PT_I8 = 20,	/* 8-byte signed integer */
        PT_STRING8 = 30,	/* Null terminated 8-bit character string */
        PT_UNICODE = 31,	/* Null terminated Unicode string */
        PT_SYSTIME = 64,	/* FILETIME 64-bit int w/ number of 100ns periods since Jan 1,1601 */
        PT_CLSID = 72,	/* OLE GUID */
        PT_BINARY = 258,	/* Uninterpreted (counted byte array) */

        PT_MV_TSTRING = (MV_FLAG | PT_STRING8),
        PT_MV_TBINARY = (MV_FLAG | PT_BINARY),

        MV_FLAG = 0x1000,

    }
  
    /// <summary>
    /// Property Type
    /// </summary>
    public enum PT : uint
    {
        /// <summary>
        /// (Reserved for interface use) type doesn't matter to caller
        /// </summary>
        PT_UNSPECIFIED = 0,
        /// <summary>
        /// NULL property value
        /// </summary>
        PT_NULL = 1,
        /// <summary>
        /// Signed 16-bit value
        /// </summary>
        PT_I2 = 2,
        /// <summary>
        /// Signed 32-bit value
        /// </summary>
        PT_LONG = 3,
        /// <summary>
        /// 4-byte floating point
        /// </summary>
        PT_R4 = 4,
        /// <summary>
        /// Floating point double
        /// </summary>
        PT_DOUBLE = 5,
        /// <summary>
        /// Signed 64-bit int (decimal w/ 4 digits right of decimal pt)
        /// </summary>
        PT_CURRENCY = 6,
        /// <summary>
        /// Application time
        /// </summary>
        PT_APPTIME = 7,
        /// <summary>
        /// 32-bit error value
        /// </summary>
        PT_ERROR = 10,
        /// <summary>
        /// 16-bit boolean (non-zero true,zero false)
        /// </summary>
        PT_BOOLEAN = 11,
        /// <summary>
        /// 16-bit boolean (non-zero true)
        /// </summary>
        PT_BOOLEAN_DESKTOP = 11,
        /// <summary>
        /// Embedded object in a property
        /// </summary>
        PT_OBJECT = 13,
        /// <summary>
        /// 8-byte signed integer
        /// </summary>
        PT_I8 = 20,
        /// <summary>
        /// Null terminated 8-bit character string
        /// </summary>
        PT_STRING8 = 30,
        /// <summary>
        /// Null terminated Unicode string
        /// </summary>
        PT_UNICODE = 31,
        /// <summary>
        /// FILETIME 64-bit int w/ number of 100ns periods since Jan 1,1601
        /// </summary>
        PT_SYSTIME = 64,
        /// <summary>
        /// OLE GUID
        /// </summary>
        PT_CLSID = 72,
        /// <summary>
        /// Uninterpreted (counted byte array)
        /// </summary>
        PT_BINARY = 258,
        /// <summary>
        /// IF MAPI Unicode, PT_TString is Unicode string; otheriwse 8-bit character string
        /// </summary>
        PT_TSTRING = PT_UNICODE
    }
}
