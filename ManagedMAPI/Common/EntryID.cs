﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ManagedMAPI
{
    /// <summary>
    /// Represents an entry identifier for a MAPI object. 
    /// </summary>
    public class EntryID
    {
        const int DefaultBufferSize = 256;
        private byte[] id_;
        
        /// <summary>
        /// Initializes a new instance of the EntryID from a byte array.
        /// </summary>
        /// <param name="id"></param>
        public EntryID(byte[] id) 
        {
            id_ = id;
        }
        /// <summary>
        /// Get byte array
        /// </summary>
        public byte[] AsByteArray { get { return this.id_; } }
        /// <summary>
        /// Build EntryID from a unmanaged memory block.
        /// </summary>
        /// <param name="cb">the size of block</param>
        /// <param name="lpb">the pointer of the unmanaged memory block</param>
        /// <returns>EntryID object</returns>
        public static EntryID BuildFromPtr(uint cb, IntPtr lpb)
        {
            byte[] b = new byte[cb];
            for (int i = 0; i < cb; i++)
                b[i] = Marshal.ReadByte(lpb, i);
            return new EntryID(b);
        }

        /// <summary>
        /// Build EntryID from string
        /// </summary>
        /// <param name="entryID">a string of entryID</param>
        /// <returns>EntryID object</returns>
        public static EntryID GetEntryID(string entryID)
        {
            if (string.IsNullOrEmpty(entryID))
                return null;
            int count = entryID.Length / 2;
            StringBuilder s = new StringBuilder(entryID);
            byte[] bytes = new byte[count];
            for (int i = 0; i < count; i++)
            {
                if ((2 * i + 2) > s.Length)
                    return null;
                string s1 = s.ToString(2 * i, 2);
                if (!Byte.TryParse(s1, System.Globalization.NumberStyles.HexNumber, null as IFormatProvider, out bytes[i]))
                    return null;
            }
            return new EntryID(bytes);
        }

        // Methods
        public static string EntryIdToString(byte[] binaryData)
        {
            StringBuilder builder = new StringBuilder(binaryData.Length);
            int index = 0;
            if (0 < binaryData.Length)
            {
                do
                {
                    if (binaryData[index] > 15)
                    {
                        builder.Append(binaryData[index].ToString("X"));
                    }
                    else
                    {
                        builder.Append("0" + binaryData[index].ToString("X"));
                    }
                    index++;
                }
                while (index < binaryData.Length);
            }
            return builder.ToString();
        }

        public static byte[] GetArrayFromEntryIDString(string entryID)
        {
            byte[] buffer = new byte[entryID.Length / 2];
            int index = 0;
            int num = 0;
            if (0 < (entryID.Length - 1))
            {
                do
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append(entryID[num]);
                    builder.Append(entryID[num + 1]);
                    buffer[index] = Convert.ToByte(builder.ToString(), 16);
                    index++;
                    num += 2;
                }
                while (num < (entryID.Length - 1));
            }
            return buffer;
        }

        internal static int[] GetTagArray(IntPtr ptr)
        {
            if (!(ptr != IntPtr.Zero))
            {
                return null;
            }
            int num2 = Marshal.ReadInt32(ptr);
            if (num2 <= 0)
            {
                return null;
            }
            int[] numArray = new int[num2];
            int num4 = ptr.ToInt32() + 4;
            int index = 0;
            if (0 < num2)
            {
                int num3 = num4;
                do
                {
                    IntPtr ptr2 = new IntPtr(num3);
                    numArray[index] = Marshal.ReadInt32(ptr2);
                    index++;
                    num3 += 4;
                }
                while (index < num2);
            }
            return numArray;
        }

        /// <summary>
        /// Convert EntryID to string
        /// </summary>
        /// <returns>id string</returns>
        public override string ToString()
        {
            StringBuilder s = new StringBuilder(DefaultBufferSize);
            foreach (Byte b in id_)
            {
                s.Append(b.ToString("X2"));
            }
            return s.ToString();
        }
     }
}
