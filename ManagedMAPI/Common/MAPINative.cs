﻿#region File Info
//
// File       : mapinative.cs
// Description: MAPI32 P/Invoke definitions
// Package    : ManagedMAPI
//
// Authors    : Fred Song
//
#endregion
using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.Win32;

namespace ManagedMAPI
{
    public class MAPINative
    {
        public static IntPtr NULL = IntPtr.Zero;
        public static Guid GUIDNULL = Guid.Empty;

        [DllImport("MAPI32.dll")]
        internal static extern HRESULT MAPIInitialize(IntPtr lpMapiInit);

        [DllImport("MAPI32.dll")]
        internal static extern void MAPIUninitialize();

        [DllImport("MAPI32.dll")]
        internal static extern int MAPILogonEx(uint ulUIParam, [MarshalAs(UnmanagedType.LPWStr)] string lpszProfileName,
                [MarshalAs(UnmanagedType.LPWStr)] string lpszPassword, uint flFlags, out IntPtr lppSession);

        /// <summary>
        /// The MAPIFreeBuffer function frees a memory buffer allocated with a call to the MAPIAllocateBuffer function or the MAPIAllocateMore function.
        /// </summary>
        /// <param name="lpBuffer">[in] Pointer to a previously allocated memory buffer. If NULL is passed in the lpBuffer parameter, MAPIFreeBuffer does nothing.</param>
        [DllImport("MAPI32.dll")]
        public static extern HRESULT MAPIFreeBuffer(IntPtr lpBuffer);
        //[DllImport("MAPI32.DLL", CharSet = CharSet.Ansi, EntryPoint = "MAPIFreeBuffer@4")]
        //internal static extern void MAPIFreeBuffer(IntPtr lpBuffer);

        /// <summary>
        /// The HrGetOneProp function retrieves the value of a single property from a property interface, that is, an interface derived from IMAPIProp.
        /// </summary>
        /// <param name="pmp">[in] Pointer to the IMAPIProp interface from which the property value is to be retrieved.</param>
        /// <param name="ulPropTag">[in] Property tag of the property to be retrieved.</param>
        /// <param name="ppprop">[out] Pointer to a pointer to the returned SPropValue structure defining the retrieved property value.</param>
        /// <remarks>
        /// Unlike the IMAPIProp::GetProps method, the HrGetOneProp function never returns any warning.
        /// Because it retrieves only one property, it simply either succeeds or fails. For retrieving multiple properties,
        /// GetProps is faster.
        ///
        /// You can set or change a single property with the HrSetOneProp function.
        /// </remarks>
        [DllImport("MAPI32.DLL")]
        public static extern HRESULT HrGetOneProp(IntPtr pmp, uint ulPropTag, out IntPtr ppprop);

        /// <summary>
        /// The HrSetOneProp function sets or changes the value of a single property on a property interface, that is, an interface derived from IMAPIProp.
        /// </summary>
        /// <param name="pmp">[in] Pointer to an IMAPIProp interface on which the property value is to be set or changed.</param>
        /// <param name="pprop">[in] Pointer to the SPropValue structure defining the property to be set or changed.</param>
        /// <remarks>
        /// Unlike the IMAPIProp::SetProps method, the HrSetOneProp function never returns any warning.
        /// Because it sets only one property, it simply either succeeds or fails.
        /// For setting or changing multiple properties, SetProps is faster.
        ///
        /// You can retrieve a single property with the HrGetOneProp function.
        /// </remarks>
        [DllImport("MAPI32.DLL")]
        public static extern HRESULT HrSetOneProp(IntPtr pmp, IntPtr pprop);

        [DllImport("MAPI32.DLL")]
        public static extern HRESULT RTFSync(IntPtr lpMessage, uint ulFlags, [MarshalAs(UnmanagedType.Bool)] out bool lpfMessageUpdated);
    }
}
