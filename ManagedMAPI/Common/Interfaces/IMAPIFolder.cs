﻿using System;
using System.Runtime.InteropServices;
using ManagedMAPI.Common.MAPI33Imports;

namespace ManagedMAPI.Common.Interfaces
{
    [ComVisible(false)]
    [ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("0002030C-0000-0000-C000-000000000046")]
    public interface IMAPIFolder : IMAPIContainer
    {
        // Methods
        int CopyFolder(ENTRYID EntryID, Guid Interface, IMAPIFolder Destination, string NewFolderName, FOLDERFLAGS Flags);
        int CopyMessages(ENTRYID[] MsgList, Guid Interface, IUnknown DestFolder, FOLDERFLAGS Flags);
        int CreateFolder(FOLDERTYPE Type, string FolderName, string FolderComment, Guid Interface, FOLDERFLAGS Flags, out IMAPIFolder Folder);
        int CreateMessage(Guid Interface, FOLDERFLAGS Flags, out IMAPIMessage Message);
        int DeleteFolder(ENTRYID EntryID, FOLDERFLAGS Flags);
        int DeleteMessages(ENTRYID[] MsgList, FOLDERFLAGS Flags);
        int EmptyFolder(FOLDERFLAGS Flags);
        int GetMessageStatus(ENTRYID EntryID, out MSGSTATUS MessageStatus);
        int SaveContentsSort(SortOrderSet SortCriteria, FOLDERFLAGS Flags);
        int SetMessageStatus(ENTRYID EntryID, MSGSTATUS NewStatus, MSGSTATUS NewStatusMask, out MSGSTATUS MessageStatus);
        int SetReadFlags(ENTRYID[] MsgList, FOLDERFLAGS Flags);
    }
}
