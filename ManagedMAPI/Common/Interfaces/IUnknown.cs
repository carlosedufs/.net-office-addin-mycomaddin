﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ManagedMAPI.Common.Interfaces
{
    [ComVisible(false)]
    [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), 
    Guid("00000000-0000-0000-C000-000000000046")]
    public interface IUnknown
    {
    }
}
