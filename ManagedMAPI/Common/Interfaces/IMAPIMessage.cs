﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ManagedMAPI.Common.MAPI33Imports;

namespace ManagedMAPI.Common.Interfaces
{
    [ComVisible(false)]
    [ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid(MAPIInterface.IID_IMessage)]
    public interface IMAPIMessage : IMAPIProp
    {   
        HRESULT CreateAttach(Guid Interface, msgFLAGS Flags, out int AttachmentNum, out IAttachment Attach);
        HRESULT DeleteAttach(uint AttachmentNum);
        HRESULT GetAttachmentTable(msgFLAGS Flags, out IMAPITable Table);
        HRESULT GetRecipientTable(msgFLAGS Flags, out IMAPITable Table);
        HRESULT ModifyRecipients(msgFLAGS Flags, ADRENTRY[] Mods);
        HRESULT OpenAttach(uint AttachmentNum, Guid Interface, msgFLAGS Flags, out IAttachment Attach);
        HRESULT SetReadFlag(msgFLAGS Flags);
        HRESULT SubmitMessage(msgFLAGS Flags);
    }

}
