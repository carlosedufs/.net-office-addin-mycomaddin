﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ManagedMAPI.Common.Interfaces
{
    [ComVisible(false)]
    [ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid(MAPIInterface.IID_IAttachment)]
    public interface IAttachment : IMAPIProp
    {
    }
}
