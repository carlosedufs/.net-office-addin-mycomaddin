﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ManagedMAPI
{
    public class FolderInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        MAPISession session_;

        public FolderInfo(MessageStore store)
        {
            session_ = store.Session;
            Name = store.Name;
            EntryId = store.StoreID;
        }

        public FolderInfo(MAPISession session, string name, EntryID storeId)
        {
            session_ = session;
            Name = name;
            EntryId = storeId;
        }

        public FolderInfo(MAPISession session, string name, EntryID storeId, MAPIFolder.TypeFolderFlag folderType)
        {
            session_ = session;
            Name = name;
            EntryId = storeId;
            FolderType = folderType;
        }

        public string Name { get; private set; }
        public EntryID EntryId { get; private set; }
        public bool hasSubFolders { get; private set; }
        public MAPIFolder.TypeFolderFlag FolderType { get; private set; }

        public bool IsDefault
        {
            get
            {
                if (session_ == null)
                    return false;
                return Equals(session_.DefaultStore);
            }
        }

        public bool IsOpened
        {
            get
            {
                if (session_ == null)
                    return false;
                return Equals(new StoreInfo(session_.CurrentStore));
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is StoreInfo && session_ != null)
            {
                FolderInfo st = obj as FolderInfo;
                if (session_ != st.session_)
                    return false;
                return session_.CompareEntryIDs(EntryId, st.EntryId);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            string fName = this.Name;

            switch (this.FolderType)
            {
                case MAPIFolder.TypeFolderFlag.Inbox:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case MAPIFolder.TypeFolderFlag.DeleteBox:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case MAPIFolder.TypeFolderFlag.Calendar:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
                case MAPIFolder.TypeFolderFlag.OutBox:
                    break;
                default:
                    fName += "(" + this.FolderType.ToString() + ")";
                    break;
            }

            return fName;
        }

        public void NotifyPropertiesChanged()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("IsDefault"));
                PropertyChanged(this, new PropertyChangedEventArgs("IsOpened"));
            }
        }
    }
}
