﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using ManagedMAPI.Common.Interfaces;

namespace ManagedMAPI
{
    public class FolderTree : IDisposable
    {
        private static Guid gMAPITable = new Guid(MAPIInterface.IID_IMAPITable);
        private static Guid gMapiFolder = new Guid(MAPIInterface.IID_IMAPIFolder);
        private static Guid gMapiContainer = new Guid(MAPIInterface.IID_IMAPIContainer);

        IMsgStore mapiObj_;

        /// <summary>
        /// EntryID of the object
        /// </summary>
        protected EntryID Id_;
        uint ulConnection_ = 0;

        /// <summary>
        /// Initializes a new instance of the MsgStore class. 
        /// </summary>
        /// <param name="msgStore">IMsgStore object</param>
        /// <param name="entryID">Entry identification of IMsgStore object</param>
        public FolderTree(MAPISession session, IMsgStore msgStore)
        {
            ulConnection_ = 0;
            Session = session;
            mapiObj_ = msgStore;
        }

        #region Public Properties

        public MAPISession Session { get; private set; }

        public List<FolderInfo> GetFolders()
        {
            return ListFolders();

        }

        public Dictionary<MAPIFolder.TypeFolderFlag, EntryID> GetDefaultFoldersByType()
        {
            return GetDefaultFoldersByType(this.mapiObj_);
        }

        public IMAPIFolder Inbox
        {
            get
            {
                IntPtr pFInbox = MAPINative.NULL;

                try
                {
                    //Pega o diretorio de recebimento dos emails
                    uint sizeFolderInbox = 0;
                    var hr = mapiObj_.GetReceiveFolder("IPM.Note", 0, out sizeFolderInbox, out pFInbox, (StringBuilder)null);

                    EntryID ENTRY_INBOX = null;

                    if (hr == HRESULT.S_OK)
                    {
                        byte[] b = new byte[sizeFolderInbox];
                        Marshal.Copy(pFInbox, b, 0, (int)sizeFolderInbox);

                        ENTRY_INBOX = new EntryID(b);
                        System.Diagnostics.Debug.WriteLine("entry id inbox: " + ENTRY_INBOX.ToString());

                        var fInbox = GetFolderFromEntry(ENTRY_INBOX);
                        if (fInbox != null)
                        {
                            System.Diagnostics.Debug.WriteLine("entry id inbox 2: " + ENTRY_INBOX.ToString());
                            return fInbox;
                        }
                    }

                }
                finally
                {
                    if (pFInbox != MAPINative.NULL) MAPINative.MAPIFreeBuffer(pFInbox);
                }

                return null;
            }
        }
        #endregion

        IMsgStore MAPIStore
        {
            get { return mapiObj_ as IMsgStore; }
        }

        public static uint FOLDER_IPM_OUTBOX_VALID = 0x00000004;
        public static uint FOLDER_IPM_SENTMAIL_VALID = 0x00000010;
        public static uint FOLDER_IPM_WASTEBASKET_VALID = 0x00000008;

        private Dictionary<MAPIFolder.TypeFolderFlag, EntryID> GetDefaultFoldersByType(IMsgStore store)
        {
            //           // 2. Other special folders. The message-store has properties for these.
            //SizedSPropTagArray(4, cols) = { 4, {PR_VALID_FOLDER_MASK, PR_IPM_OUTBOX_ENTRYID, PR_IPM_SENTMAIL_ENTRYID, PR_IPM_WASTEBASKET_ENTRYID} };
            //ULONG pcount; SPropValue *props;
            //hr = mapi_msgstore->GetProps((SPropTagArray*)&cols,0,&pcount,&props);
            //if (hr==S_OK || hr==MAPI_W_ERRORS_RETURNED)
            //{ LONG mask; if (props[0].ulPropTag==PR_VALID_FOLDER_MASK) mask=props[0].Value.ul; else mask=0;
            //  if ((mask&FOLDER_IPM_OUTBOX_VALID) && props[1].ulPropTag==PR_IPM_OUTBOX_ENTRYID) eid_outbox.set(props[1].Value.bin.cb, (ENTRYID*)props[1].Value.bin.lpb);
            //  if ((mask&FOLDER_IPM_SENTMAIL_VALID) && props[2].ulPropTag==PR_IPM_SENTMAIL_ENTRYID) eid_sent.set(props[2].Value.bin.cb, (ENTRYID*)props[2].Value.bin.lpb);
            //  if ((mask&FOLDER_IPM_WASTEBASKET_VALID) && props[3].ulPropTag==PR_IPM_WASTEBASKET_ENTRYID) eid_deleted.set(props[3].Value.bin.cb, (ENTRYID*)props[3].Value.bin.lpb);
            //  pMAPIFreeBuffer(props);
            //}
            var propsToGet = MAPIFolder.GetPropsTagsArray(
                new[] {Common.MAPI33Imports.Tags.PR_VALID_FOLDER_MASK, 
                    Common.MAPI33Imports.Tags.PR_IPM_OUTBOX_ENTRYID, 
                    Common.MAPI33Imports.Tags.PR_IPM_SENTMAIL_ENTRYID, 
                    Common.MAPI33Imports.Tags.PR_IPM_WASTEBASKET_ENTRYID});

            uint cItems = 0;
            IntPtr propArray = MAPINative.NULL;
            Dictionary<MAPIFolder.TypeFolderFlag, EntryID> dicFolderByType = new Dictionary<MAPIFolder.TypeFolderFlag, EntryID>();
            try
            {
                HRESULT hr = HRESULT.S_OK;
                if ((hr = store.GetProps(propsToGet, 0, out cItems, ref propArray)) == HRESULT.S_OK || hr == HRESULT.MAPI_W_ERRORS_RETURNED)
                {
                    if (cItems > 0)
                    {
                        MAPIProp[] lpProps = new MAPIProp[cItems];
                        for (int j = 0; j < cItems; j++) // each column
                        {
                            SPropValue lpProp = (SPropValue)Marshal.PtrToStructure(
                                propArray.AddMultiply(j, Marshal.SizeOf(typeof(SPropValue))), //+ j * Marshal.SizeOf(typeof(SPropValue)
                                typeof(SPropValue));
                            lpProps[j] = new MAPIProp(lpProp);
                        }

                        uint mask = 0;
                        if ((uint)lpProps[0].Tag == (uint)Common.MAPI33Imports.Tags.PR_VALID_FOLDER_MASK)
                            mask = lpProps[0].ul;
                        else
                            mask = 0;

                        if ((mask & FOLDER_IPM_OUTBOX_VALID) != 0 && (uint)lpProps[1].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_OUTBOX_ENTRYID)
                            dicFolderByType.Add(MAPIFolder.TypeFolderFlag.OutBox, new EntryID(lpProps[1].AsBinary));

                        if ((mask & FOLDER_IPM_SENTMAIL_VALID) != 0 && (uint)lpProps[2].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_SENTMAIL_ENTRYID)
                            dicFolderByType.Add(MAPIFolder.TypeFolderFlag.SentMail, new EntryID(lpProps[2].AsBinary));

                        if ((mask & FOLDER_IPM_WASTEBASKET_VALID) != 0 && (uint)lpProps[3].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_WASTEBASKET_ENTRYID)
                            dicFolderByType.Add(MAPIFolder.TypeFolderFlag.DeleteBox, new EntryID(lpProps[3].AsBinary));


                        if (propArray != MAPINative.NULL) MAPINative.MAPIFreeBuffer(propArray);
                        propArray = MAPINative.NULL;

                        var inboxFolder = this.Inbox;

                        //                      SizedSPropTagArray(5, spec) = {5, {0x36D00102, 0x36D10102, 0x36D20102, 0x36D30102, 0x36D40102}};
                        //hr = infolder->GetProps((SPropTagArray*)&spec,0,&pcount,&props);
                        //if (hr==S_OK || hr==MAPI_W_ERRORS_RETURNED)
                        //{ if (props[0].ulPropTag==0x36D00102) eid_calendar.set(props[0].Value.bin.cb, (ENTRYID*)props[0].Value.bin.lpb);
                        //  if (props[1].ulPropTag==0x36D10102) eid_contacts.set(props[1].Value.bin.cb, (ENTRYID*)props[1].Value.bin.lpb);
                        //  if (props[2].ulPropTag==0x36D20102) eid_journal.set(props[2].Value.bin.cb, (ENTRYID*)props[2].Value.bin.lpb);
                        //  if (props[3].ulPropTag==0x36D30102) eid_notes.set(props[3].Value.bin.cb, (ENTRYID*)props[3].Value.bin.lpb);
                        //  if (props[4].ulPropTag==0x36D40102) eid_tasks.set(props[4].Value.bin.cb, (ENTRYID*)props[4].Value.bin.lpb);
                        //  pMAPIFreeBuffer(props);

                        if (inboxFolder != null)
                        {
                            propsToGet = MAPIFolder.GetPropsTagsArray(
                               new uint[] {
                                   (uint)ADXMapiTags.PR_IPM_APPOINTMENT_ENTRYID, 
                                   (uint)ADXMapiTags.PR_IPM_CONTACT_ENTRYID, 
                                    (uint)ADXMapiTags.PR_IPM_JOURNAL_ENTRYID, 
                                    (uint)ADXMapiTags.PR_IPM_NOTE_ENTRYID, 
                                    (uint)ADXMapiTags.PR_IPM_TASK_ENTRYID});

                            propArray = MAPINative.NULL;
                            hr = HRESULT.S_FALSE;

                            if ((hr = inboxFolder.GetProps(propsToGet, 0, out cItems, ref propArray)) == HRESULT.S_OK || hr == HRESULT.MAPI_W_ERRORS_RETURNED)
                            {
                                if (cItems > 0)
                                {
                                    var lpPropsInbox = new MAPIProp[cItems];
                                    for (int j = 0; j < cItems; j++) // each column
                                    {
                                        SPropValue lpProp = (SPropValue)Marshal.PtrToStructure(
                                            propArray.AddMultiply(j, Marshal.SizeOf(typeof(SPropValue))), //+ j * Marshal.SizeOf(typeof(SPropValue)
                                            typeof(SPropValue));
                                        lpPropsInbox[j] = new MAPIProp(lpProp);
                                    }

                                    if ((uint)lpPropsInbox[0].Tag == (uint)ADXMapiTags.PR_IPM_APPOINTMENT_ENTRYID)
                                        dicFolderByType.Add(MAPIFolder.TypeFolderFlag.Calendar, new EntryID(lpPropsInbox[0].AsBinary));
                                    if ((uint)lpPropsInbox[1].Tag == (uint)ADXMapiTags.PR_IPM_CONTACT_ENTRYID)
                                        dicFolderByType.Add(MAPIFolder.TypeFolderFlag.Contato, new EntryID(lpPropsInbox[1].AsBinary));
                                    if ((uint)lpPropsInbox[2].Tag == (uint)ADXMapiTags.PR_IPM_JOURNAL_ENTRYID)
                                        dicFolderByType.Add(MAPIFolder.TypeFolderFlag.Jornal, new EntryID(lpPropsInbox[2].AsBinary));
                                    if ((uint)lpPropsInbox[3].Tag == (uint)ADXMapiTags.PR_IPM_NOTE_ENTRYID)
                                        dicFolderByType.Add(MAPIFolder.TypeFolderFlag.Anotacoes, new EntryID(lpPropsInbox[3].AsBinary));
                                    if ((uint)lpPropsInbox[4].Tag == (uint)ADXMapiTags.PR_IPM_TASK_ENTRYID)
                                        dicFolderByType.Add(MAPIFolder.TypeFolderFlag.Tarefa, new EntryID(lpPropsInbox[4].AsBinary));
                                }
                            }
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (propArray != MAPINative.NULL) MAPINative.MAPIFreeBuffer(propArray);
            }

            return dicFolderByType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<FolderInfo> ListFolders() //object mapiObject
        {
            List<FolderInfo> retdef = new List<FolderInfo>();

            // create a Guid that we pass to retreive the IMAPIProp Interface.
            Guid gStore = new Guid(MAPIInterface.IID_IMAPIMsgStore);
            Guid gFolder = new Guid(MAPIInterface.IID_IMAPIFolder);

            // Pointer to IUnknown Interface
            IntPtr IUnknown = MAPINative.NULL;

            // Pointer to IMessage Interface
            IntPtr IMessage = MAPINative.NULL;

            // Pointer to IMAPIProp Interface
            IntPtr IMAPIProp = MAPINative.NULL;

            // Pointer to IMAPIProp Interface
            IntPtr IMAPIFolder = MAPINative.NULL;

            // Structure that will hold the Property Value
            ManagedMAPI.SPropValue pPropValue;

            // A pointer that points to the SPropValue structure
            IntPtr ptrPropValue = MAPINative.NULL;

            // if we have no MAPIObject everything is senseless...
            if (mapiObj_ == null) return retdef;

            try
            {
                //ja tem sessao, nao precisa do codigo abaixo
                #region
                if (1 == 2)
                {
                    // We can pass NULL here as parameter, so we do it.
                    MAPINative.MAPIInitialize(MAPINative.NULL);

                    // retrive the IUnknon Interface from our MAPIObject comming from Outlook.
                    IUnknown = Marshal.GetIUnknownForObject(mapiObj_);

                    // since HrGetOneProp needs a IMessage Interface, we must query our IUnknown interface for the IMessage interface.
                    // create a Guid that we pass to retreive the IMessage Interface.
                    Guid guidIMessage = new Guid(MAPIInterface.IID_IMAPIMsgStore);

                    // try to retrieve the IMessage interface, if we don't get it, everything else is sensless.
                    if (Marshal.QueryInterface(IUnknown, ref guidIMessage, out IMessage) != (int)HRESULT.S_OK) return retdef;

                    //pega o objeto
                    ManagedMAPI.IMsgStore iStore = null;
                    var oIMAPIMsgStore = Marshal.GetObjectForIUnknown(IMessage);
                    if (oIMAPIMsgStore != null)
                    {
                        iStore = oIMAPIMsgStore as ManagedMAPI.IMsgStore;
                    }
                }
                #endregion

                //Pega o diretorio de recebimento dos emails
                uint sizeFolderInbox = 0;
                IntPtr pFInbox = MAPINative.NULL;
                var hr = mapiObj_.GetReceiveFolder("IPM.Note", 0, out sizeFolderInbox, out pFInbox, (StringBuilder)null);

                EntryID ENTRY_INBOX = null;

                if (hr == HRESULT.S_OK)
                {
                    byte[] b = new byte[sizeFolderInbox];
                    Marshal.Copy(pFInbox, b, 0, (int)sizeFolderInbox);

                    ENTRY_INBOX = new EntryID(b);
                    System.Diagnostics.Debug.WriteLine("entry id inbox: " + ENTRY_INBOX.ToString());

                    var fInbox = GetFolderFromEntry(ENTRY_INBOX);
                    if (fInbox != null)
                    {
                        System.Diagnostics.Debug.WriteLine("entry id inbox 2: " + ENTRY_INBOX.ToString());
                        Marshal.ReleaseComObject(fInbox);
                    }
                }

                // create a Guid that we pass to retreive the IMAPIProp Interface.
                Guid guidIMAPIProp = new Guid(MAPIInterface.IID_IMAPIProp);

                IMessage = Marshal.GetIUnknownForObject(this.mapiObj_);

                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                if (Marshal.QueryInterface(IMessage, ref guidIMAPIProp, out IMAPIProp) != (int)HRESULT.S_OK) return retdef;

                // double check, if we wave no pointer, exit...
                if (IMAPIProp == MAPINative.NULL) return retdef;

                // try to get the Property ( Property Tags can be found with Outlook Spy from Dmitry Streblechenko )
                // we pass the IMAPIProp Interface, the PropertyTag and the pointer to the SPropValue to the function.
                MAPINative.HrGetOneProp(IMAPIProp, (uint)Common.MAPI33Imports.Tags.PR_IPM_SUBTREE_ENTRYID, out ptrPropValue);

                // if that also fails we have no such property
                if (ptrPropValue == MAPINative.NULL) return retdef;

                // connect the pointer to our structure holding the value
                pPropValue = (ManagedMAPI.SPropValue)Marshal.PtrToStructure(ptrPropValue, typeof(ManagedMAPI.SPropValue));

                ManagedMAPI.MAPIProp mapi = new ManagedMAPI.MAPIProp(pPropValue);
                if (mapi.Type.FullName == typeof(byte[]).FullName)
                {
                    var entry = EntryID.EntryIdToString(mapi.AsBinary);
                    if (!string.IsNullOrEmpty(entry) && this.mapiObj_ != null)
                    {
                        EntryID eiSUBTREE_ENTRYID = new EntryID(mapi.AsBinary);
                        IntPtr lpEntryID = MAPINative.NULL;

                        uint tpEntry;
                        IntPtr iEntry;

                        //var eiSize = (uint)Marshal.SizeOf(ei);
                        //Marshal.StructureToPtr(ei, lpEntryID, false);
                        //IntPtr unmanagedAddr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(ManagedMAPI.ENTRYID)));

                        var intSize = Marshal.SizeOf(typeof(int));

                        ENTRYID eid = new ENTRYID();
                        eid.cbEntryID = (uint)mapi.AsBinary.Length;
                        eid.pEntryID = Marshal.AllocHGlobal(mapi.AsBinary.Length * IntPtr.Size);
                        Marshal.Copy(mapi.AsBinary, 0, eid.pEntryID, mapi.AsBinary.Length);

                        //Marshal.StructureToPtr(eid, unmanagedAddr, false);
                        //Marshal.FreeHGlobal(unmanagedAddr);

                        // OKAY
                        //var hresult = iStore.OpenEntry(pPropValue.Value.bin.cb, pPropValue.Value.bin.lpb, IntPtr.Zero,
                        //    (uint)ManagedMAPI.FLAGS.Default, out tpEntry, out iEntry);

                        var hresult = this.mapiObj_.OpenEntry(eid.cbEntryID, eid.pEntryID, IntPtr.Zero,
                                (uint)OpenEntryFlags.BestAccess, out tpEntry, out iEntry);

                        if (hresult != ManagedMAPI.HRESULT.S_OK) return retdef;

                        //var oo = Marshal.GetObjectForIUnknown(iEntry);
                        //if (oo != null)
                        //{
                        //    IList<PropertyInfo> props = new List<PropertyInfo>(oo.GetType().GetProperties());
                        //    Guid guidDerived = oo.GetType().GUID;

                        //    foreach (PropertyInfo prop in props)
                        //    {
                        //        object propValue = prop.GetValue(oo, null);
                        //        // Do something with propValue
                        //    }
                        //}

                        if ((ObjectType)tpEntry == ObjectType.MAPI_FOLDER)
                        {
                            // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                            if (Marshal.QueryInterface(iEntry, ref gFolder, out IMAPIFolder) != (int)HRESULT.S_OK) return retdef;

                            IMAPIFolder topFolder = Marshal.GetObjectForIUnknown(IMAPIFolder) as IMAPIFolder;

                            if (topFolder != null)
                            {
                                EntryID eiFolder = GetEntryIDFromFolder(IMAPIFolder);
                                var idEntryID = EntryID.EntryIdToString(eiFolder.AsByteArray);
                                var displayName = GetPropStringFrom(Common.MAPI33Imports.Tags.PR_DISPLAY_NAME, IMAPIFolder);

                                System.Diagnostics.Debug.WriteLine("Display nome do diretorio PR_IPM_SUBTREE_ENTRYID: " + displayName);
                                System.Diagnostics.Debug.WriteLine("Entry id do diretorio PR_IPM_SUBTREE_ENTRYID: " + eiFolder.ToString());

                                if (this.Session.CompareEntryIDs(eiSUBTREE_ENTRYID, eiFolder))
                                {
                                    Console.WriteLine("puts grilla, id do folder subtree eh igual ao entry do folder");
                                }

                                //Abre o folder inbox com o diretorio PR_IPM_SUBTREE_ENTRYID
                                var fInbox = GetFolderFromEntry(topFolder, ENTRY_INBOX);
                                if (fInbox != null)
                                {
                                    System.Diagnostics.Debug.WriteLine("Achou o diretorio inbox abaixo do PR_IPM_SUBTREE_ENTRYID: " + ENTRY_INBOX.ToString());
                                    Marshal.ReleaseComObject(fInbox);
                                }

                                //opa
                                var lstFolders = new List<MAPIFolder>();
                                RecFolders(this.mapiObj_, topFolder, 5, "", lstFolders);

                                if (lstFolders.Count > 0)
                                {
                                    #region para cada folder
                                    lstFolders.ForEach(
                                        oFolder =>
                                        {
                                            if (oFolder.EntryID.ToString() == ENTRY_INBOX.ToString())
                                            {
                                                oFolder.FolderType = MAPIFolder.TypeFolderFlag.Inbox;

                                                //DialogsHelper.ShowInfo("Inbox mesmo: " + oFolder.DisplayName);                                               
                                                var eio = oFolder.EntryID;

                                                if (this.Session.CompareEntryIDs(eiSUBTREE_ENTRYID, eio))
                                                {
                                                    Console.WriteLine("puts grilla, entryid do folder subtree eh igual ao entry do folder INBOX");
                                                }

                                                var s1 = eiSUBTREE_ENTRYID.ToString();
                                                var s2 = eio.ToString();

                                                if (string.Compare(s1, s2, true) == 0)
                                                {
                                                    Console.WriteLine("puts grilla, entryid do folder subtree eh igual ao entry do folder INBOX");
                                                }

                                                uint subType = 0;

                                                intSize = IntPtr.Size;
                                                var cb = (int)oFolder.EntryID.AsByteArray.Length;

                                                IntPtr subFolder = MAPINative.NULL;
                                                IntPtr pEntrySubF = Marshal.AllocHGlobal(cb * intSize);
                                                Marshal.Copy(oFolder.EntryID.AsByteArray, 0, pEntrySubF, cb);

                                                Guid g = Guid.Empty;

                                                try
                                                {
                                                    hr = oFolder.IMAPIFolder.OpenEntry(
                                                        (uint)cb,
                                                        pEntrySubF,
                                                        MAPINative.NULL,
                                                        (uint)OpenEntryFlags.Default,
                                                        out subType,
                                                        out subFolder);

                                                    if (hr == HRESULT.S_OK)
                                                    {
                                                        Marshal.Release(subFolder);
                                                    }
                                                    else
                                                    {
                                                        IntPtr lpError = MAPINative.NULL;
                                                        if (oFolder.IMAPIFolder.GetLastError((int)hr, 0, out lpError) == HRESULT.S_OK)
                                                        {
                                                            // if that also fails we have no such property
                                                            if (lpError == MAPINative.NULL) return;

                                                            try
                                                            {
                                                                // connect the pointer to our structure holding the value
                                                                var pError = (ManagedMAPI.MAPIError)Marshal.PtrToStructure(lpError, typeof(ManagedMAPI.MAPIError));

                                                                if (pError.ulVersion == 0)
                                                                {
                                                                }
                                                            }
                                                            finally
                                                            {
                                                                if (lpError != MAPINative.NULL) MAPINative.MAPIFreeBuffer(lpError);
                                                            }
                                                        }

                                                        hr = topFolder.OpenEntry(
                                                        (uint)cb,
                                                        pEntrySubF,
                                                        MAPINative.NULL,
                                                        (uint)OpenEntryFlags.BestAccess,
                                                        out subType,
                                                        out subFolder);
                                                        if (hr == HRESULT.S_OK)
                                                        {
                                                            Marshal.Release(subFolder);
                                                        }
                                                    }
                                                }
                                                catch (COMException ex)
                                                {
                                                    System.Diagnostics.Debug.WriteLine("Errrorrrr: " + ex.Message + " -> " + ex.StackTrace);
                                                    return;
                                                }
                                                catch (Exception ex)
                                                {
                                                    System.Diagnostics.Debug.WriteLine("Errrorrrr: " + ex.Message + " -> " + ex.StackTrace);
                                                    return;
                                                }

                                                Marshal.FreeHGlobal(pEntrySubF);
                                            }
                                            else
                                            {
                                                oFolder.FolderType = MAPIFolder.TypeFolderFlag.Nenhum;
                                            }
                                        });
                                    #endregion
                                }

                                Marshal.ReleaseComObject(topFolder);

                                return lstFolders.Select(
                                    s =>
                                    {
                                        return new FolderInfo(this.Session, s.DisplayName, s.EntryID, s.FolderType);
                                    }).ToList();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Errrorrrr: " + ex.Message + " -> " + ex.StackTrace);
            }
            finally
            {
                // Free used Memory structures
                if (ptrPropValue != MAPINative.NULL) MAPINative.MAPIFreeBuffer(ptrPropValue);

                // cleanup all references to COM Objects
                if (IMAPIFolder != MAPINative.NULL) Marshal.Release(IMAPIFolder);
                if (IMAPIProp != MAPINative.NULL) Marshal.Release(IMAPIProp);
                if (IMessage != MAPINative.NULL) Marshal.Release(IMessage);
                if (IUnknown != MAPINative.NULL) Marshal.Release(IUnknown);

                //MAPINative.MAPIUninitialize();
            }

            return retdef;
        }

        private static void RecFolders(ManagedMAPI.IMsgStore session, IMAPIFolder parent, int depth, string prefix, List<MAPIFolder> folders)
        {

            //// NB. We cannot call parent->GetHierarchyTable. That's because GetHierarchyTable
            //// tries to open it will full (read/write) access, but Outlook2000/IMO only supports
            //// readonly access, hence giving an MAPI_E_NO_ACCESS error. Therefore, we get
            //// the hierarchy in this roundabout way, in readonly mode.
            //IMAPITable *hierarchy; HRESULT hr;
            //const GUID local_IID_IMAPITable = {0x00020301,0,0, {0xC0,0,0,0,0,0,0,0x46}};
            //hr = parent->OpenProperty(PR_CONTAINER_HIERARCHY,&local_IID_IMAPITable,0,0,(IUnknown**)&hierarchy);
            //if (hr!=S_OK) return;
            //// and query for all the rows
            //SizedSPropTagArray(3, cols) = {3, {PR_ENTRYID,PR_DISPLAY_NAME,PR_SUBFOLDERS} };
            //SRowSet *rows;
            //hr = pHrQueryAllRows(hierarchy,(SPropTagArray*)&cols, NULL, NULL, 0, &rows);
            //hierarchy->Release();
            //if (hr!=S_OK) {pFreeProws(rows); return;}
            //// Note: the entry-ids returned by the list are just short-term list-specific
            //// entry-ids. But we want to put long-term entry-ids in our 'folder' list.
            //// That's why it's necessary to open the folder...

            IntPtr unk = MAPINative.NULL;
            IntPtr IMAPITable = MAPINative.NULL;
            IntPtr IMAPIContainer = MAPINative.NULL;

            Guid g = Guid.Empty;

            try
            {
                try
                {
                    var hr = parent.OpenProperty(
                        (uint)Common.MAPI33Imports.Tags.PR_CONTAINER_HIERARCHY,
                        ref gMAPITable, 0, 0, out unk);

                    if (hr != HRESULT.S_OK) return;

                    // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                    if (Marshal.QueryInterface(unk, ref gMAPITable, out IMAPITable) != (int)HRESULT.S_OK) return;

                    ManagedMAPI.IMAPITable objMAPITable = Marshal.GetObjectForIUnknown(IMAPITable) as ManagedMAPI.IMAPITable;
                    if (objMAPITable != null)
                    {
                        var mapiTab = new MAPITable(objMAPITable);

                        // and retrieve the corresponding UID 
                        uint[] itags = new uint[] { 
                            (uint)PropTags.PR_ENTRYID, 
                            (uint)PropTags.PR_DISPLAY_NAME, 
                            (uint)Common.MAPI33Imports.Tags.PR_SUBFOLDERS,
                            (uint)Common.MAPI33Imports.Tags.PR_FOLDER_CHILD_COUNT};

                        mapiTab.SetColumns(itags);

                        SRow[] rows;
                        if (mapiTab.QueryRows(50, out rows))
                        {
                            foreach (var f in rows)
                            {
                                var entry = f.propVals[0].AsBinary;
                                var displayName = f.propVals[1].AsString;

                                var temSubFolders = f.propVals[2].Type == typeof(Int32) && f.propVals[2].AsBool;
                                var qtosSubFolders = f.propVals[3].Type == typeof(Int32) ? f.propVals[3].AsInt32 : 0;

                                uint subType = 0;
                                IntPtr subFolder = MAPINative.NULL;

                                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                                //IntPtr pContainer = Marshal.GetIUnknownForObject(parent);

                                //if (Marshal.QueryInterface(pContainer, ref gMapiContainer, out IMAPIContainer) == S_OK)
                                //{
                                //var oContanier = Marshal.GetObjectForIUnknown(IMAPIContainer) as IMAPIContainer;

                                hr = parent.OpenEntry(
                                    (uint)f.propVals[0].AsSBinary.cb,
                                    f.propVals[0].AsSBinary.lpb,
                                    IntPtr.Zero,
                                    (uint)OpenEntryFlags.BestAccess,
                                    out subType,
                                    out subFolder);

                                if (hr != HRESULT.S_OK)
                                {
                                    hr = session.OpenEntry(
                                        (uint)f.propVals[0].AsSBinary.cb,
                                        f.propVals[0].AsSBinary.lpb,
                                        IntPtr.Zero,
                                        (uint)OpenEntryFlags.BestAccess,
                                        out subType,
                                        out subFolder);
                                }

                                if (hr != HRESULT.S_OK) continue;

                                if ((ObjectType)subType != ObjectType.MAPI_FOLDER) continue;

                                IntPtr psubFolder = MAPINative.NULL;

                                // try to retrieve the IMAPIProp interface from IMessage Interface, everything else is sensless.
                                if (Marshal.QueryInterface(subFolder, ref gMapiFolder, out psubFolder) != (int)HRESULT.S_OK) return;

                                var osubFolder = Marshal.GetObjectForIUnknown(psubFolder) as IMAPIFolder;

                                if (osubFolder != null)
                                {
                                    EntryID eiFolder = GetEntryIDFromFolder(psubFolder);
                                    var idEntryID = EntryID.EntryIdToString(eiFolder.AsByteArray);
                                    var displayName2 = GetPropStringFrom(Common.MAPI33Imports.Tags.PR_DISPLAY_NAME, psubFolder);

                                    folders.Add(new MAPIFolder(displayName, entry, osubFolder));
                                }

                                Marshal.Release(psubFolder);

                                if (temSubFolders)
                                {

                                }

                                //Marshal.ReleaseComObject(oContanier);
                                //}

                                //if (pContainer != NULL) Marshal.Release(pContainer);
                            }
                        }

                        Marshal.ReleaseComObject(objMAPITable);
                        objMAPITable = null;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            finally
            {
                if (IMAPITable != MAPINative.NULL)
                {
                    Marshal.Release(IMAPITable);
                }

                if (unk != MAPINative.NULL)
                {
                    Marshal.Release(unk);
                }
            }

        }


        private static EntryID GetEntryIDFromFolder(IntPtr folder)
        {
            IntPtr spEntry = MAPINative.NULL;

            try
            {
                MAPINative.HrGetOneProp(
                                           folder,
                                           (uint)Common.MAPI33Imports.Tags.PR_ENTRYID,
                                           out spEntry);

                // if that also sucess we have no such property
                if (spEntry != MAPINative.NULL)
                {
                    // connect the pointer to our structure holding the value
                    var pPropValue = (SPropValue)Marshal.PtrToStructure(spEntry, typeof(SPropValue));

                    var entryCompleto = new EntryID(pPropValue.Value.bin.AsBytes);
                    var idEntryID = EntryID.EntryIdToString(pPropValue.Value.bin.AsBytes);

                    //Marshal.FreeHGlobal(pPropValue.Value.lpszA);

                    return entryCompleto;
                }
            }
            finally
            {
                if (spEntry != MAPINative.NULL) MAPINative.MAPIFreeBuffer(spEntry);
            }

            return null;
        }

        private static string GetPropStringFrom(Common.MAPI33Imports.Tags tag, IntPtr item)
        {
            IntPtr spEntry = MAPINative.NULL;

            try
            {
                MAPINative.HrGetOneProp(
                    item,
                    (uint)tag,
                    out spEntry);

                // if that also sucess we have no such property
                if (spEntry != MAPINative.NULL)
                {
                    // connect the pointer to our structure holding the value
                    var pPropValue = (SPropValue)Marshal.PtrToStructure(spEntry, typeof(SPropValue));

                    MAPIProp mp = new MAPIProp(pPropValue);

                    try
                    {
                        if (mp.Type == typeof(byte[]))
                        {
                            var entryCompleto = new EntryID(pPropValue.Value.bin.AsBytes);
                            var idEntryID = EntryID.EntryIdToString(pPropValue.Value.bin.AsBytes);
                            return idEntryID;
                        }
                        else if (mp.Type == typeof(string))
                        {
                            return mp.AsString;
                        }
                        else if (mp.Type == typeof(Int32))
                        {
                            return mp.AsInt32.ToString();
                        }
                        else if (mp.Type == typeof(UInt64))
                        {
                            return mp.AsUInt64.ToString();
                        }
                    }
                    finally
                    {
                        //Marshal.FreeHGlobal(pPropValue.Value.lpszA);
                    }
                }
            }
            finally
            {
                if (spEntry != MAPINative.NULL) MAPINative.MAPIFreeBuffer(spEntry);
            }

            return null;
        }

        private IMAPIFolder GetFolderFromEntry(EntryID id)
        {
            return GetFolderFromEntry(this.mapiObj_, id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IMAPIFolder GetFolderFromEntry(object container, EntryID id)
        {
            uint tpEntry;
            IntPtr iEntry = MAPINative.NULL;
            IntPtr pEntryID = MAPINative.NULL;

            try
            {
                pEntryID = Marshal.AllocHGlobal(id.AsByteArray.Length * IntPtr.Size);

                Marshal.Copy(id.AsByteArray, 0, pEntryID, id.AsByteArray.Length);

                var hresult =
                    container is IMsgStore ? ((IMsgStore)container).OpenEntry((uint)id.AsByteArray.Length, pEntryID, IntPtr.Zero, (uint)OpenEntryFlags.BestAccess, out tpEntry, out iEntry) :
                        ((IMAPIContainer)container).OpenEntry((uint)id.AsByteArray.Length, pEntryID, IntPtr.Zero, (uint)OpenEntryFlags.BestAccess, out tpEntry, out iEntry);

                if (hresult != ManagedMAPI.HRESULT.S_OK) return null;

                if ((ObjectType)tpEntry == ObjectType.MAPI_FOLDER)
                {
                    return Marshal.GetObjectForIUnknown(iEntry) as IMAPIFolder;
                }
            }
            finally
            {
                if (pEntryID != MAPINative.NULL) Marshal.FreeHGlobal(pEntryID);
            }

            return null;
        }

        #region IDisposable Interface
        /// <summary>
        /// Dispose Msgstore object
        /// </summary>
        public void Dispose()
        {
            Session = null;
            if (mapiObj_ != null)
            {
                Marshal.ReleaseComObject(mapiObj_);
                mapiObj_ = null;
            }
        }

        #endregion

    }
}
