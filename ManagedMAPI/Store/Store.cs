﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace ManagedMAPI
{
    /// <summary>
    /// IMsgStore .Net Wrapper object
    /// </summary>
    public class MessageStore : IDisposable
    {
        /// <summary>
        /// New mail event
        /// </summary>
        public event EventHandler<MsgStoreNewMailEventArgs> OnNewMail;
  
        OnAdviseCallbackHandler callbackHandler_;
        uint ulConnection_ = 0;
        IMAPIAdviseSink pAdviseSink_ = null;
    
        IMsgStore mapiObj_;

        FolderTree storeFolders = null;

        /// <summary>
        /// EntryID of the object
        /// </summary>
        protected EntryID Id_;

        /// <summary>
        /// Initializes a new instance of the MsgStore class. 
        /// </summary>
        /// <param name="msgStore">IMsgStore object</param>
        /// <param name="entryID">Entry identification of IMsgStore object</param>
        public MessageStore(MAPISession session, IMsgStore msgStore, EntryID entryID, string name)
        {
            ulConnection_ = 0;
            Session = session;
            mapiObj_ = msgStore;
            Id_ = entryID;
            Name = name;

            storeFolders = new FolderTree(session, msgStore);
        }

        #region Public Properties
        /// <summary>
        /// Gets entry identificatio of Message Store
        /// </summary>
        public EntryID StoreID { get { return Id_; } }     

        public MAPISession Session { get; private set; }

        public string Name { get; private set; }

        public FolderTree Folders
        {
            get
            {
                return storeFolders;
            }
        }
        #endregion

        #region Public Methods
     
        /// <summary>
        /// Registers to receive notification of specified events that affect the message store.
        /// </summary>
        /// <param name="eventMask">A mask of values that indicate the types of notification events that the caller is interested in and should be included in the registration. </param>
        /// <returns></returns>
        public bool RegisterEvents(EEventMask eventMask)
        {
            callbackHandler_ = new OnAdviseCallbackHandler(OnNotifyCallback);
            HRESULT hresult = HRESULT.S_OK;
            try
            {
                pAdviseSink_ = new MAPIAdviseSink(IntPtr.Zero, callbackHandler_);
                hresult = MAPIStore.Advise(0, IntPtr.Zero, (uint)eventMask, pAdviseSink_, out ulConnection_);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
            return hresult == HRESULT.S_OK;
        }


        /// <summary>
        /// Cancels the sending of notifications.
        /// </summary>
        public void UnRegisteEvents()
        {
            if (ulConnection_ != 0)
                MAPIStore.Unadvise(ulConnection_);
            if (pAdviseSink_ != null)
            {
                pAdviseSink_ = null;
            }
        }

        /// <summary>
        /// return props of store
        /// </summary>
        /// <param name="store"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        public MAPIProp[] GetPropsStore(IMsgStore store, uint[] tags)
        {
            //           // 2. Other special folders. The message-store has properties for these.
            //SizedSPropTagArray(4, cols) = { 4, {PR_VALID_FOLDER_MASK, PR_IPM_OUTBOX_ENTRYID, PR_IPM_SENTMAIL_ENTRYID, PR_IPM_WASTEBASKET_ENTRYID} };
            //ULONG pcount; SPropValue *props;
            //hr = mapi_msgstore->GetProps((SPropTagArray*)&cols,0,&pcount,&props);
            //if (hr==S_OK || hr==MAPI_W_ERRORS_RETURNED)
            //{ LONG mask; if (props[0].ulPropTag==PR_VALID_FOLDER_MASK) mask=props[0].Value.ul; else mask=0;
            //  if ((mask&FOLDER_IPM_OUTBOX_VALID) && props[1].ulPropTag==PR_IPM_OUTBOX_ENTRYID) eid_outbox.set(props[1].Value.bin.cb, (ENTRYID*)props[1].Value.bin.lpb);
            //  if ((mask&FOLDER_IPM_SENTMAIL_VALID) && props[2].ulPropTag==PR_IPM_SENTMAIL_ENTRYID) eid_sent.set(props[2].Value.bin.cb, (ENTRYID*)props[2].Value.bin.lpb);
            //  if ((mask&FOLDER_IPM_WASTEBASKET_VALID) && props[3].ulPropTag==PR_IPM_WASTEBASKET_ENTRYID) eid_deleted.set(props[3].Value.bin.cb, (ENTRYID*)props[3].Value.bin.lpb);
            //  pMAPIFreeBuffer(props);
            //}
            var propsToGet = MAPIFolder.GetPropsTagsArray(tags);

            uint cItems = 0;
            IntPtr propArray = MAPINative.NULL;
            try
            {
                HRESULT hr = HRESULT.S_OK;
                if ((hr = store.GetProps(propsToGet, 0, out cItems, ref propArray)) == HRESULT.S_OK || hr == HRESULT.MAPI_W_ERRORS_RETURNED)
                {
                    if (cItems > 0)
                    {
                        MAPIProp[] lpProps = new MAPIProp[cItems];
                        for (int j = 0; j < cItems; j++) // each column
                        {
                            SPropValue lpProp = (SPropValue)Marshal.PtrToStructure(
                                propArray.AddMultiply(j, Marshal.SizeOf(typeof(SPropValue))), //+ j * Marshal.SizeOf(typeof(SPropValue)
                                typeof(SPropValue));
                            lpProps[j] = new MAPIProp(lpProp);
                        }

                        //uint mask = 0;
                        //if ((uint)lpProps[0].Tag == (uint)Common.MAPI33Imports.Tags.PR_VALID_FOLDER_MASK)
                        //    mask = lpProps[0].ul;
                        //else
                        //    mask = 0;

                        //if ((mask & FOLDER_IPM_OUTBOX_VALID) != 0 && (uint)lpProps[1].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_OUTBOX_ENTRYID)
                        //    dicFolderByType.Add(MAPIFolder.TypeFolderFlag.OutBox, new EntryID(lpProps[1].AsBinary));

                        //if ((mask & FOLDER_IPM_SENTMAIL_VALID) != 0 && (uint)lpProps[2].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_SENTMAIL_ENTRYID)
                        //    dicFolderByType.Add(MAPIFolder.TypeFolderFlag.SentMail, new EntryID(lpProps[2].AsBinary));

                        //if ((mask & FOLDER_IPM_WASTEBASKET_VALID) != 0 && (uint)lpProps[3].Tag == (uint)Common.MAPI33Imports.Tags.PR_IPM_WASTEBASKET_ENTRYID)
                        //    dicFolderByType.Add(MAPIFolder.TypeFolderFlag.DeleteBox, new EntryID(lpProps[3].AsBinary));

                        return lpProps;
                    }
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (propArray != MAPINative.NULL) MAPINative.MAPIFreeBuffer(propArray);
            }

            return null;
        }

        #endregion

        #region Private Properties/Methods/Events

        IMsgStore MAPIStore
        {
            get { return mapiObj_ as IMsgStore; }
        }

        void OnNotifyCallback(IntPtr pContext, uint cNotification, IntPtr lpNotifications)
        {
            EEventMask eventType = (EEventMask)Marshal.ReadInt32(lpNotifications);
            int intSize = Marshal.SizeOf(typeof(int));
            IntPtr sPtr = lpNotifications.AddMultiply(intSize, 2); // + intSize * 2; //ulEventType, ulAlignPad
            switch (eventType)
            {
                case EEventMask.fnevNewMail:
                    {
                        Console.WriteLine("New mail");
                        if (this.OnNewMail == null)
                            break;
                        NEWMAIL_NOTIFICATION notification = (NEWMAIL_NOTIFICATION)Marshal.PtrToStructure(sPtr, typeof(NEWMAIL_NOTIFICATION));
                        MsgStoreNewMailEventArgs n = new MsgStoreNewMailEventArgs(StoreID, notification);
                        this.OnNewMail(this, n);
                    }
                    break;
             }
        }

        #endregion

        #region IDisposable Interface
        /// <summary>
        /// Dispose Msgstore object
        /// </summary>
        public void Dispose()
        {
            UnRegisteEvents();
            Session = null;
            if (mapiObj_ != null)
            {
                Marshal.ReleaseComObject(mapiObj_);
                mapiObj_ = null;
            }
        }

        #endregion


    }


}
